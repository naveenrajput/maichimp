<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['homepage_welcome']=[
	'Dashboard' => 'Tableau de bord',
	'Greetlog' => 'Greetlog - Tableau de bord',
	'Search' => 'Rechercher ...',
	'Profile' => 'Profil',
	'Activity' => 'Journal des visiteurs',
	'Logout' => 'Se déconnecter',

	'Cancel' => 'Annuler',
	'Alerts' => "Centre d'alertes",
	'Message' => 'Centre de messagerie',
	'Ready' => 'Prêt à partir?',
	'Select' => 'Sélectionnez "Déconnexion" ci-dessous si vous êtes prêt à mettre fin à votre session en cours.',
	'Details' => 'Détails',

	'Manage' => 'Gérer les branches',
	'Visitor' => 'Détails du visiteur',
	'VisitorLog' => 'Journal des visiteurs',
	'Invites' => 'Invite',
	'Employee' => 'Annuaire des employés',
	'All' => 'Tous les employés',

	'Admin' => "Rôle d'administrateur",
	'Device' => "Connexion de l'appareil",
	'SetupGuide' => "Guide d'installation",
	'Setup' => 'Installer',
	'Help' => "Centre d'aide",
	'Settings' => 'Paramètres',

	'Greet' => 'Greetlog - Connexion',
	'Log' => 'Connectez-vous à Greetlog!',
	'Forgot' => 'Mot de passe oublié?',
	'Login' => "S'identifier",
	'Password' => 'Mot de passe'
];

