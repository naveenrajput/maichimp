<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['homepage_welcome']=[
	'Dashboard' => 'Dashboard',
	'Greetlog' => 'Greetlog - Dashboard',
	'Search' => 'Search for...',
	'Profile' => 'Profile',
	'Activity' => 'Activity Log',
	'Logout' => 'Logout',

	'Cancel' => 'Cancel',
	'Alerts' => 'Alerts Center',
	'Message' => 'Message Center',
	'Ready' => 'Ready to Leave?',
	'Select' => 'Select "Logout" below if you are ready to end your current session.',
	'Details' => 'Details',

	'Manage' => 'Manage Branches',
	'Visitor' => 'Visitor Details',
	'VisitorLog' => 'Visitor Log',
	'Invites' => 'Invites',
	'Employee' => 'Employee Directory',
	'All' => 'All Employee',

	'Admin' => 'Admin Role',
	'Device' => 'Device Connection',
	'SetupGuide' => 'Setup Guide',
	'Setup' => 'Setup',
	'Help' => 'Help Center',
	'Settings' => 'Settings',

	'Greet' => 'Greetlog - Login',
	'Log' => 'Log in to the Greetlog!',
	'Forgot' => 'Forgot Password?',
	'Log_In' => 'Log In',
	'Login' => 'Login',
	'Password' => 'Password'
];
