<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['homepage_welcome']=[
	'Dashboard' => 'لوحة القيادة ',
	'Greetlog' => 'Greetlog - لوحة القيادة ',
	'Search' => 'بحث عن...',

	'Profile' => 'الملف الشخصي  ',
	'Activity' => 'سجل النشاطات  ',
	'Logout' => 'تسجيل خروج ',

	'Cancel' => 'إلغاء ',
	'Alerts' => 'مركز التنبيهات  ',
	'Message' => 'مركز الرسائل ',
	'Ready' => 'على استعداد للمغادرة؟ ',
	'Select' => 'حدد "تسجيل الخروج" أدناه إذا كنت مستعدًا لإنهاء جلستك الحالية .',
	'Details' => 'تفاصيل ',

	'Manage' => 'إدارة الفروع ',
	'Visitor' => 'تفاصيل الزائر ',
	'VisitorLog' => 'سجل الزوار ',
	'Invites' => 'تدعو',
	'Employee' => 'دليل الموظف  ',
	'All' => 'كل موظف  ',

	'Admin' => 'دور المسؤول ',
	'Device' => 'اتصال الجهاز ',
	'SetupGuide' => 'دليل الإعداد ',
	'Setup' => 'اقامة ',
	'Help' => 'مركز المساعدة ',
	'Settings' => 'الإعدادات  ',

	'Greet' => 'Greetlog - تسجيل الدخول ',
	'Log' => 'تسجيل الدخول إلى Greetlog!',
	'Forgot' => 'هل نسيت كلمة المرور؟ ',
	'Login' => 'تسجيل الدخول ',
	'Password' => 'كلمه السر  '
];


