<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['homepage_welcome']=[
	'Dashboard' => 'Tablero',
	'Greetlog' => 'Greetlog - Panel de control',
	'Search' => 'Buscar...',
	'Profile' => 'Perfil',
	'Activity' => 'Registro de actividades',
	'Logout' => 'Cerrar sesión',

	'Cancel' => 'Cancelar',
	'Alerts' => 'Centro de alertas',
	'Message' => 'Centro de mensajes',
	'Ready' => '¿Preparado para irme?',
	'Select' => 'Seleccione "Cerrar sesión" a continuación si está listo para finalizar su sesión actual.',
	'Details' => 'Detalles',

	'Manage' => 'Administrar sucursales',
	'Visitor' => 'Detalles del visitante',
	'VisitorLog' => 'Registro de visitante',
	'Invites' => 'Invita',
	'Employee' => 'Directorio de empleados',
	'All' => 'Todos los empleados',

	'Admin' => 'Rol de administrador',
	'Device' => 'Conexión del dispositivo',
	'SetupGuide' => 'Guia de preparacion',
	'Setup' => 'Preparar',
	'Help' => 'Centro de ayuda',
	'Settings' => 'Configuraciones',

	'Greet' => 'Greetlog - Iniciar sesión',
	'Log' => '¡Inicia sesión en Greetlog!',
	'Forgot' => '¿Se te olvidó tu contraseña?',
	'Login' => 'Iniciar sesión',
	'Password' => 'Contraseña'
];
