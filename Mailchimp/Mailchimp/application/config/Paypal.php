<?php
/** set your paypal credential **/

$config['client_id'] = 'AUlQhBiy9afdck7rJaKnQx-1YRK99t7V_aUrOT-gmlVkODV2T13X-4iwI_Ih5ZF_ceqHbZA5cKgycC-m
';
$config['secret'] = 'EP8VSxLJkOJT3S9dx9kWZ3p2CGGlx8I9iO7nZAPEwceZTTXeBejwZwRMFdpZyT9HtcqUwB7ByaNpTJjd';

/**
 * SDK configuration
 */
/**
 * Available option 'sandbox' or 'live'
 */
$config['settings'] = array(

    'mode' => 'sandbox',
    /**
     * Specify the max request time in seconds
     */
    'http.ConnectionTimeOut' => 1000,
    /**
     * Whether want to log to a file
     */
    'log.LogEnabled' => true,
    /**
     * Specify the file that want to write on
     */
    'log.FileName' => 'application/logs/paypal.log',
    /**
     * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
     *
     * Logging is most verbose in the 'FINE' level and decreases as you
     * proceed towards ERROR
     */
    'log.LogLevel' => 'FINE'
);