<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class User extends REST_Controller {

 
    function __construct()
    {

    	parent::__construct();
        $this->db = $this->load->database('default', true);
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->library('form_validation');
        $this->load->database();
         $this->load->library('email');
        // $this->load->model('ApiModel');
         define('API_ACCESS_KEY','AAAA7lgHDAI:APA91bGp9sZ27PhNyMSElHqoxfzN_Sjrg9LZUIfDsl3ZvMXIXY1-9K1u5yc_dymBt1c36pnNQBQTRgPhHdBlqY8bFfs4KG8_FW9YTT6fdXOR99FAb7uKRQ7G7gIzsl2SZOms1WWTisCi');

    }
    /*code for signup From API*/


    // for sign-up user 
public function create_assign_contact_post(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $email = $data['email'];
        $name = $data['name'];
        
$data = [
    'email'     => $email,
     'name'     => $name,
    'status'    => 'subscribed'
   
];
    $apiKey = '7b9d7da7fa6b381413809c26664fbc68-us19';
    $listId = '0ea7bfc67e';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
       'merge_fields'  => [
            'FNAME'     => $data['name'],
            
        ]
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

    $result = curl_exec($ch);
     $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

     if($httpCode==200) {

            $this->response([
                'responseCode' => $httpCode,
                'message'=>'Contact Added'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        } 
        else {
            $this->response([
                    'responseCode' => $httpCode,
                    'message'=>'Error While Add Contact on list',
                    
            ], REST_Controller::HTTP_BAD_REQUEST);
        
        }

    

    
    }
    public function assign_contact_to_list_post(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $email = $data['email'];
        $name = $data['name'];
        
$data = [
    'email'     => $email,
     'name'     => $name,
    'status'    => 'subscribed'
   
];
    $apiKey = '7b9d7da7fa6b381413809c26664fbc68-us19';
    $listId = '0ea7bfc67e';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
       'merge_fields'  => [
            'FNAME'     => $data['name'],
            
        ]
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

    $result = curl_exec($ch);
     $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

     if($httpCode==200) {

            $this->response([
                'responseCode' => $httpCode,
                'message'=>'Contact Added'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        } 
        else {
            $this->response([
                    'responseCode' => $httpCode,
                    'message'=>'Error While Add Contact on list',
                    
            ], REST_Controller::HTTP_BAD_REQUEST);
        
        }

    

    
    }
     //Resend password
     
     public function create_list_post(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $apiKey = '7b9d7da7fa6b381413809c26664fbc68-us19';
        $listId = '0ea7bfc67e';

    //$memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/segments';
    $json = json_encode([
        'name' =>$data['list_name'],
        // "subscribed","unsubscribed","cleaned","pending"
       'static_segment'  => []
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                 

    $result = curl_exec($ch);
     $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

     if($httpCode==200) {

            $this->response([
                'responseCode' => $httpCode,
                'message'=>'category Added'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        } 
        else {
            $this->response([
                    'responseCode' => $httpCode,
                    'message'=>'Error While Add category on list',
                    
            ], REST_Controller::HTTP_BAD_REQUEST);
        
        }

    

    
     }

   
}