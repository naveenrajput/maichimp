<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Trial extends REST_Controller {

    function __construct()
    {
    	parent::__construct();

        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('ApiModel');
    }
        if($insert){
                    // Set the response and exit
                    $this->response([
             session_start();

$CLIENT_ID = "123";
$CLIENT_SECRET = "123";
$CALLBACK = "http://localhost:8000";
$SCOPES = array("sales_invoices", "documents");

function authorizeUrl($client_id, $callback, $scopes = array())
{
  $pattern = "https://moneybird.com/oauth/authorize?client_id=%s&redirect_uri=%s&scope=%s&response_type=code";
  return sprintf($pattern, $client_id,
                           urlencode($callback),
                           implode("+", $scopes));
}

function getAccessCode($client_id, $callback, $client_secret, $request_code) {
  $curl = curl_init("https://moneybird.com/oauth/token");
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($curl, CURLOPT_HEADER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, rawurldecode(http_build_query(array(
    'client_id' => $client_id,
    'redirect_uri' => $callback,
    'client_secret' => $client_secret,
    'code' => $request_code,
    'grant_type' => 'authorization_code'
  ))));

  $json = json_decode(curl_exec($curl));
  return $json->access_token;
}

function getAdministrations($access_token) {
  $headers = array(
    'Content-Type: application/json',
    sprintf('Authorization: Bearer %s', $access_token)
  );

  $curl = curl_init("https://moneybird.com/api/v2/administrations.json");

  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

  $result = json_decode(curl_exec($curl));

  return $result;
}

if (isset($_GET['reset'])) {
  session_destroy();
  header(sprintf("Location: %s", $CALLBACK));
  die();
} elseif(isset($_SESSION['access_token'])) {
  $administrations = getAdministrations($_SESSION['access_token']);

  foreach($administrations as $administration) {
    echo $administration->name . "<br />";
  }
} elseif (isset($_GET['code'])) {
  $access_token = getAccessCode($CLIENT_ID, $CALLBACK, $CLIENT_SECRET, $_GET['code']);
  $_SESSION['access_token'] = $access_token;

  header(sprintf("Location: %s", $CALLBACK));
  die();
}  else {
  echo "Starting point, click on the link <br />"
  ?>
  <a href="<?php echo authorizeUrl($CLIENT_ID, $CALLBACK, $SCOPES); ?>">Click</a>
  <?php
}


