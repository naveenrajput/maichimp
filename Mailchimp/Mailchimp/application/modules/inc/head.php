<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
	
  <title>Greetlog - Dashboard</title>
	
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">  
 <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
    <!---<link href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">---->
  
 <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
   <link href="<?php echo base_url() ?>assets/css/responsive.dataTables.min.css" rel="stylesheet">
  <!--<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">--->



  <link href="<?php echo base_url() ?>assets/css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-float-label.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/formValidation.min.css">

<!--fordatepicker--->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/daterangepicker.css" />

<!--for table responsive--->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  
 
</head>