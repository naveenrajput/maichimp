<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-basic sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('dashboard'); ?>">
        <img src="<?php echo base_url() ?>assets/img/logo.png" width="160">
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCompany" aria-expanded="true" aria-controls="collapseCompany">
          <i class="fas fa-building"></i>
          <span><?php echo $name=$this->session->userdata('company'); ?></span></a>
		  
		    <div id="collapseCompany" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <?php if($this->session->userdata('name')=='admin'){ ?>
            <a class="collapse-item" href="<?php echo base_url('add_company'); ?>">Settings</a>
          <?php } ?>
            <a class="collapse-item" href="<?php echo base_url('branches'); ?>">Manage Branches</a>
          </div>
        </div>
       
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
    
      <!-- dashboard -->
	   <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url('dashboard'); ?>" >
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard </span>
        </a>
        
      </li>
	  
	   <!-- Nav Item - Visitor Collapse Menu -->   
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVisitor" aria-expanded="true" aria-controls="collapseVisitor">
          <i class="fa fa-user"></i>
          <span>Visitor</span>
        </a>
        <div id="collapseVisitor" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo base_url('visitorlog'); ?>">Visitor Log</a>
            <a class="collapse-item" href="<?php echo base_url('invites'); ?>">Invites</a>
          </div>
        </div>
      </li>
	  <!-- Nav Item - employee directory Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEmployee" aria-expanded="true" aria-controls="collapseEmployee">
          <i class="fa fa-users"></i>
          <span>Employee Directory</span>
        </a>
        <div id="collapseEmployee" class="collapse" aria-labelledby="headingEmployee" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
        
            <a class="collapse-item" href="<?php echo base_url('employee'); ?>">All Employee</a>
          <?php if($this->session->userdata('name')=='admin'){ ?>
            <a class="collapse-item" href="<?php echo base_url('add-user'); ?>">Admin Role</a>
          <?php } ?>
         
          </div>
        </div>
      </li>
  
	  <!-- Nav Item - employee directory Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link " href="#" >
          <i class="fa fa-mobile-alt"></i>
          <span>Device Connection </span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
       Setup Guide
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
	   <li class="nav-item">
        <a class="nav-link " href="<?php echo base_url('setup'); ?>" >
          <i class="fas fa-fw fa-wrench"></i>
          <span>Setup </span>
        </a>
        
      </li>
	  
	  
      <!-- Nav Item - Help Center -->
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fa fa-info"></i>
          <span>Help Center</span></a>
      </li>

     
    

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->