<!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Greetlog 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url('logout'); ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url() ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>


    <!-- Page level plugins -->
  <script src="<?php echo base_url() ?>assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url() ?>assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url() ?>assets/js/demo/datatables-demo.js"></script>
  <!-- for file upload  -->
  <script src="<?php echo base_url() ?>assets/js/custom/bs-custom-file-input.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/custom.js"></script>

<!-- bootstrap front end validation -->
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>-->
<!--Close bootstrap front end validation -->

<!--datepicker -->
<script src="<?php echo base_url() ?>assets/js/moment.min.js"></script>
  <script src="<?php echo base_url() ?>assets//js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/daterangepicker.js"></script>
 
<!-- Bootstrap Form Helpers --> 
  <!--for contact no country code-->
<script src="<?php echo base_url() ?>assets/js/bootstrap-formhelpers.min.js"></script>

<!--Jquery Validation By Badal-->

<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
<script type="text/javascript">
$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
  $("#myForm").validate({
    
  rules: {
    empname: {
      required: true,
      lettersonly: true,
      //minlength: 4,
    },
    fname: {
      required: true,
     // minlength: 4,
      lettersonly: true
    },
    lname: {
      required: true,
      //minlength: 4,
      lettersonly: true
    },
    mname: {
      //minlength: 4,
      lettersonly: true
    },
    middlename: {
      //minlength: 4,
      lettersonly: true
    },
    lastname: {
      required: true,
     // minlength: 4,
      lettersonly: true
    },
    branchhead: {
      required: true,
     // minlength: 4,
      lettersonly: true
    },
    ownername: {
          required: true,
          //minlength: 4,
          lettersonly: true,
    },
    visitor_name: {
          required: true,
         // minlength: 4,
          lettersonly: true
    },
    contact_num: {
      required: true,
      minlength: 10,
      //digits: true
      
    },
    contactnum: {
      required: true,
      minlength: 10,
     //digits: true
    },
    contactno: {
      required: true,
      minlength: 10,
    },
    contactNo: {
      required: true,
      minlength: 10,
    },
    alternateno: {
      required: true,
      minlength: 10,
   
    },     
    email_id: {
      required: true,
      email:true
    },
    email: {
      required: true,
      email:true
    },
    password: {
      required: true,
      minlength: 8
    },
    bpassword: {
      required: true,
      minlength: 8
    },
    gender: {
      required: true,
    },
    designation: {
      required: true,
    },
    companytype: {
      required: true,
    },
    branchname: {
      required: true,
    },
    location: {
      required: true,
    },
   
    access: {
      required: true,
    },
    address: {
      required: true,
    },
    sendtype: {
      required: true,
    },
    plan: {
      required: true,
    },
    companyname: {
      required: true,
    },
   
    visitortype: {
      required: true,
    },
    logo: {
      required: true,
    },
    branch: {
      required: true,
    },
    host: {
      required: true,
    },
    
    title: {
      required: true,
    },
    msg: {
      required: true,
    },
    summary: {
      required: true,
    },
    invitedate: {
      required: true,
    },
    invitetime: {
      required: true,
    }
  },
  //For custom messages
  messages: {
    empname:{
      required: "This field is required.",
      lettersonly: "Only Alphabate Are allowed",
      minlength: "Enter at least 4 characters"
    },
    fname:{
      required: "This field is required.",
      lettersonly: "Only Alphabate Are allowed",
      minlength: "Enter at least 4 characters"
    },
    ownername:{
      required: "This field is required.",
      lettersonly: "Only Alphabate Are allowed",
      minlength: "Enter at least 4 characters"
    },
    visitor_name:{
      required: "This field is required.",
      lettersonly: "Only Alphabate Are allowed",
      minlength: "Enter at least 4 characters"
    },
    contact_num:{
      required: "This field is required.",
      minlength: "Enter at least 10 Digits"
    },
    contactNo:{
      required: "This field is required.",
      minlength: "Enter at least 10 Digits"
    },
    email_id:{
        required: "This field is required.",
        email: "Please enter vaild email"
    },
    password:{
 required: "This field is required.",
      minlength: "Enter at least 8 characters"
    },
    gender:{
 required: "This field is required.",
    },
    designation:{
 required: "This field is required.",
    }
  },
  errorElement : 'div',
  errorPlacement: function(error, element) {
    var placement = $(element).data('error');
    if (placement) {
      $(placement).append(error)
    } else {
      error.insertAfter(element);
    }
  }
});
</script>
<!--Jquery Validation-->
</body>

</html>