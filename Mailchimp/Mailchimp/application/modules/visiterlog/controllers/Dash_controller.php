<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Dash_controller extends MY_Controller {
		
		public function __construct(){
	        parent::__construct();
	        $this->load->model('Dash_model');
	        $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
	        $this->load->library('session');
    		$this->load->library('make_bread');
	        $this->load->library('email');
	        date_default_timezone_set("Asia/Kolkata");
	    }

	    public function Dashboard()
		{	
			$companyid=$this->session->userdata('id');
			if($this->session->userdata('name')=='admin'){

				$data = array();
				$end_date= '';
				$day = '';
				$branchid= '';
				$data['total_visit'] = $this->Dash_model->Check_in($companyid, $branchid, $day, $end_date); 
				$data['repeat_visit'] = $this->Dash_model->Repeat_visit($companyid, $branchid, $day, $end_date);
				$data['today_visit'] = $this->Dash_model->Today_visit($companyid, $branchid);
				$data['d_data'] = array('day7' =>  $this->Dash_model->Day7_data(),
								'day6' =>  $this->Dash_model->Day6_data(),
								'day5' =>  $this->Dash_model->Day5_data(),
								'day4' =>  $this->Dash_model->Day4_data(),
								'day3' =>  $this->Dash_model->Day3_data(),
								'day2' =>  $this->Dash_model->Day2_data(),
								'day1' =>  $this->Dash_model->Day1_data()
							);
				//$m_data = $this->Dash_model->M1_data() ;
				$data['month'] = array('m_5'  =>  $this->Dash_model->M5_data(),
							   'm_10' =>  $this->Dash_model->M10_data(),
							   'm_15' =>  $this->Dash_model->M15_data(),
							   'm_20' =>  $this->Dash_model->M20_data(),
							   'm_25' =>  $this->Dash_model->M25_data(),
							   'm_30' =>  $this->Dash_model->M30_data()
							   
							 );
				
				$data['yearly'] = array(
					//'jan'  =>  $this->Dash_model->Jan_data(),
							   'feb' =>  $this->Dash_model->Feb_data(),
							   'march' =>  $this->Dash_model->March_data(),
							   'april' =>  $this->Dash_model->Apr_data(),
							   'may' =>  $this->Dash_model->May_data(),
							   'june' =>  $this->Dash_model->June_data(),
							   'july' =>  $this->Dash_model->July_data(),
							   'aug' =>  $this->Dash_model->Aug_data(),
							   'sep' =>  $this->Dash_model->Sep_data(),
							   'oct' =>  $this->Dash_model->Oct_data(),
							   'nov' =>  $this->Dash_model->Nov_data(),
							   'dec' =>  $this->Dash_model->Dec_data()
							 );
				$data['v_list'] = $this->Dash_model->Visitor_list($companyid, $branchid);	
				
				//array_push($data, $repeat_visit, $today_visit, $d_data, $v_list);
				//print_r($data);die;
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				$this->load->view('dashboard',compact('data'));
				$this->load->view('../../inc/footer');

			}elseif($this->session->userdata('name')=='subadmin'){

				$branchid=$this->session->userdata('branch');
				$data = array();
				$end_date= '';
				$day = '';
				$branchid= '';
				$data['total_visit'] = $this->Dash_model->Check_in($companyid, $branchid, $day, $end_date); 
				$data['repeat_visit'] = $this->Dash_model->Repeat_visit($companyid, $branchid, $day, $end_date);
				$data['today_visit'] = $this->Dash_model->Today_visit($companyid, $branchid);
				$data['d_data'] = array('day7' =>  $this->Dash_model->Day7_data(),
								'day6' =>  $this->Dash_model->Day6_data(),
								'day5' =>  $this->Dash_model->Day5_data(),
								'day4' =>  $this->Dash_model->Day4_data(),
								'day3' =>  $this->Dash_model->Day3_data(),
								'day2' =>  $this->Dash_model->Day2_data(),
								'day1' =>  $this->Dash_model->Day1_data()
							);
				//$m_data = $this->Dash_model->M1_data() ;
				$data['month'] = array('m_5'  =>  $this->Dash_model->M5_data(),
							   'm_10' =>  $this->Dash_model->M10_data(),
							   'm_15' =>  $this->Dash_model->M15_data(),
							   'm_20' =>  $this->Dash_model->M20_data(),
							   'm_25' =>  $this->Dash_model->M25_data(),
							   'm_30' =>  $this->Dash_model->M30_data()
							   
							 );
				
				$data['yearly'] = array(
					//'jan'  =>  $this->Dash_model->Jan_data(),
							   'feb' =>  $this->Dash_model->Feb_data(),
							   'march' =>  $this->Dash_model->March_data(),
							   'april' =>  $this->Dash_model->Apr_data(),
							   'may' =>  $this->Dash_model->May_data(),
							   'june' =>  $this->Dash_model->June_data(),
							   'july' =>  $this->Dash_model->July_data(),
							   'aug' =>  $this->Dash_model->Aug_data(),
							   'sep' =>  $this->Dash_model->Sep_data(),
							   'oct' =>  $this->Dash_model->Oct_data(),
							   'nov' =>  $this->Dash_model->Nov_data(),
							   'dec' =>  $this->Dash_model->Dec_data()
							 );
				$data['v_list'] = $this->Dash_model->Visitor_list($companyid, $branchid);	
				
				//array_push($data, $repeat_visit, $today_visit, $d_data, $v_list);
				//print_r($data);die;
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				$this->load->view('dashboard',compact('data'));
				$this->load->view('../../inc/footer');
				// $data = array();
				// $end_date= '';
				// $day = '';
				// $total_visit = $this->Dash_model->Check_in( $companyid, $branchid, $day, $end_date); 
				// $repeat_visit = $this->Dash_model->Repeat_visit($companyid, $branchid, $day, $end_date);
				// $today_visit = $this->Dash_model->Today_visit($companyid, $branchid);
				// $d_data = array('day7' =>  $this->Dash_model->Day7_data(),
				// 				'day6' =>  $this->Dash_model->Day6_data(),
				// 				'day5' =>  $this->Dash_model->Day5_data(),
				// 				'day4' =>  $this->Dash_model->Day4_data(),
				// 				'day3' =>  $this->Dash_model->Day3_data(),
				// 				'day2' =>  $this->Dash_model->Day2_data(),
				// 				'day1' =>  $this->Dash_model->Day1_data()
				// 			);
				// //$m_data = $this->Dash_model->M1_data() ;
				// $month = array('m_5'  =>  $this->Dash_model->M5_data(),
				// 			   'm_10' =>  $this->Dash_model->M10_data(),
				// 			   'm_15' =>  $this->Dash_model->M15_data(),
				// 			   'm_20' =>  $this->Dash_model->M20_data(),
				// 			   'm_25' =>  $this->Dash_model->M25_data(),
				// 			   'm_30' =>  $this->Dash_model->M30_data()
							   
				// 			 );
				
				// $yearly = array(
				// 	//'jan'  =>  $this->Dash_model->Jan_data(),
				// 			   'feb' =>  $this->Dash_model->Feb_data(),
				// 			   'march' =>  $this->Dash_model->March_data(),
				// 			   'april' =>  $this->Dash_model->Apr_data(),
				// 			   'may' =>  $this->Dash_model->May_data(),
				// 			   'june' =>  $this->Dash_model->June_data(),
				// 			   'july' =>  $this->Dash_model->July_data(),
				// 			   'aug' =>  $this->Dash_model->Aug_data(),
				// 			   'sep' =>  $this->Dash_model->Sep_data(),
				// 			   'oct' =>  $this->Dash_model->Oct_data(),
				// 			   'nov' =>  $this->Dash_model->Nov_data(),
				// 			   'dec' =>  $this->Dash_model->Dec_data()
				// 			 );
				// 			 $data['v_list'] = $this->Dash_model->Visitor_list($companyid, $branchid);
				// //array_push($data, $total_visit, $repeat_visit, $today_visit, $d_data);
				// //print_r($data);die;
				// $this->load->view('../../inc/head');
				// $this->load->view('../../inc/sidebar');
				// $this->load->view('../../inc/header');
				// $this->load->view('dashboard',compact('total_visit', 'repeat_visit', 'today_visit','d_data'));
				// $this->load->view('../../inc/footer');
			}else{
				redirect('login');
			}
			
		}

		public function Days7_graph(){
			$d_data = array('day7' =>  $this->Dash_model->Day7_data(),
							'day6' =>  $this->Dash_model->Day6_data(),
							'day5' =>  $this->Dash_model->Day5_data(),
							'day4' =>  $this->Dash_model->Day4_data(),
							'day3' =>  $this->Dash_model->Day3_data(),
							'day2' =>  $this->Dash_model->Day2_data(),
							'day1' =>  $this->Dash_model->Day1_data()
						);
			redirect('dashboard', $d_data);
		}

		public function Month_graph(){
			$month = array('m_5'  =>  $this->Dash_model->M5_data(),
						   'm_10' =>  $this->Dash_model->M10_data(),
						   'm_15' =>  $this->Dash_model->M15_data(),
						   'm_20' =>  $this->Dash_model->M20_data(),
						   'm_25' =>  $this->Dash_model->M25_data(),
						   'm_30' =>  $this->Dash_model->M30_data()
						   
						 );
			redirect('dashboard', $month);
		}

		public function Yearly_graph(){
			$yearly = array(
				//'jan'  =>  $this->Dash_model->Jan_data(),
						   'feb' =>  $this->Dash_model->Feb_data(),
						   'march' =>  $this->Dash_model->March_data(),
						   'april' =>  $this->Dash_model->Apr_data(),
						   'may' =>  $this->Dash_model->May_data(),
						   'june' =>  $this->Dash_model->June_data(),
						   'july' =>  $this->Dash_model->July_data(),
						   'aug' =>  $this->Dash_model->Aug_data(),
						   'sep' =>  $this->Dash_model->Sep_data(),
						   'oct' =>  $this->Dash_model->Oct_data(),
						   'nov' =>  $this->Dash_model->Nov_data(),
						   'dec' =>  $this->Dash_model->Dec_data()
						 );
			redirect('dashboard', $yearly);
		}
		 
	}


