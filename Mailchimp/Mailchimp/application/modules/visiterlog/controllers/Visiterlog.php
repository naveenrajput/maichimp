<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Visiterlog extends MY_Controller {
		
		public function __construct(){
	        parent::__construct();
	        $this->load->model('Visiter_model');
	        $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
	        $this->load->library('session');
	        //$this->load->helper('url');
	        error_reporting(0);
    		$this->load->library('make_bread');
	        $this->load->library('email');
	        date_default_timezone_set("Asia/Kolkata");
	    }
	    //Controller for view login page
		public function index()
		{   $this->load->library('mongo_db', array('activate'=>'newdb'),'mongo_db2');

			print_r('expression');die;
		}
		//Controller for view registration
		public function Registration()
		{
			if($this->session->userdata('name')!='admin'){
				$this->load->view('registration');
			}else {
				redirect('dashboard');
			}

		}
		
		public function Check_Login()
		{
			$email =$this->input->post('email');
			$pass =base64_encode($this->input->post('password'));
			
			$check=$this->Visiter_model->checklogin($email,$pass);
			$check1=$this->Visiter_model->Bchecklogin($email,$pass);
			$check2=$this->Visiter_model->Uchecklogin($email,$pass);
            //print_r($check2);die; 
			if(!empty($check)/* && empty($check1) && empty($check2)*/){
					 $abc = array('name' =>'admin',
					'id' =>$check->id,
					'access'=>'full',
					'company' =>$check->companyname,
					'ownername' =>$check->ownername,
					'logo' =>$check->logo,
					'table' =>'companies',
					'where' =>array('email'=>$check->email,'password'=>$check->password),
				
				);
					$this->session->set_userdata($abc);
				
					redirect('dashboard');
				
			}elseif(/*empty($check) && */!empty($check1)){
					$abc = array('name' =>'subadmin',
								 'branch'=>$check1->branch_id,
							     'id'=>$check1->companyid,
							     'access'=>$check1->access,
							     'company' =>$check1->branchname,
							     'ownername' =>$check1->branchhead,
							     'logo' =>$check1->logo,
							     'table' =>'branches',
					             'where' =>array('email_id'=>$check1->email_id,'bpassword'=>$check1->bpassword),
						
							      );
					$this->session->set_userdata($abc);

					redirect('dashboard');

			}elseif(!empty($check2)){
			    $companyid=$check2->companyid;
                $cmp=$this->Visiter_model->get_companyname($companyid);
					$abc = array('name' =>'subadmin',
								 'branch'=>$check2->branchid,
							     'id'=>$check2->companyid,
							     'access'=>$check2->access,
							     'company' =>$cmp->companyname,
							     'ownername' =>$check2->empname,
							     'logo' =>$check2->profile_pic,
							     'table' =>'employees',
					             'where' =>array('email_id'=>$check2->email_id,'password'=>$check2->password),
							     //'email'=>$check2->email_id 
							 );
					$this->session->set_userdata($abc);

					redirect('dashboard');

			}
			elseif($this->session->userdata('name')!=null){
				redirect('dashboard');
			}elseif(empty($check) && empty($check1) && empty($check2) ){
					$this->session->set_flashdata('msg','Incorrect Username or Password');
					redirect('login');

			}
		}


		public function Logout()
	    {
			$this->session->sess_destroy();
			redirect('login');
	    }
		public function Import_emplist(){

        $data = array();
        $memData = array();
       //check Allowed types
            $allowed =  array('csv');
            $filename = $_FILES['file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                echo  $successMsg = '<span class="alert-danger form-control">Invalid file, please select only CSV file.</span>';
            }else{
            // Validate submitted form data
            if($this->form_validation->run() == FALSE){
                 
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
               
                // If file uploaded 

                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                   
                    $this->load->library('CSVReader');
                    $fields=array('S.No','Name', 'Email', 'Phone', 'Designation', 'DOB');
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                if (array_key_exists("Name",$csvData[1]) && array_key_exists("Email",$csvData[1]) && array_key_exists("Phone",$csvData[1]) && array_key_exists("Designation",$csvData[1]) && array_key_exists("DOB",$csvData[1])){
            
                    
                    $cart = array();
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ 
                            $rowCount++;
                            $email = $row['Email'];
                            $sno = $row['S.No'];
                            $memData = array(
                                'empname'    => $row['Name'],
                                'email_id'   => $row['Email'],
                                'contact_num'=> $row['Phone'],
                                'designation'=> $row['Designation'],
                                'companyid'  => $this->session->userdata('id'),
                                'dob'        => $row['DOB']
                            );
                         
                            // Check whether email already exists in the database
                            $prevQuery = "SELECT emp_id FROM employees WHERE email_id = '".$email."' ";
                            $prevResult=$this->db->query($prevQuery);

                            if($prevResult->result_id->num_rows > 0){
                        
                              
                               array_push($cart,$sno.'-'.$email);  
                        
                          
                            }
                            else{

                                // Insert member data in the database
                                 
                                 $query= $this->db->insert('employees',$memData);
                                
                            }
                           
                            
                        }
                        
                        // Status message with imported data count
                       if(!empty($query)){
                            if (!empty($cart)) {
                                foreach ($cart as $value){
                                 echo $successMsg = '<span class="alert-danger form-control">Email already exists on line no-'.$value.',</span>';
                                }
                            }else{

                              echo $successMsg = '<span class="alert-success form-control">File uploded Successfully</span>';
                            }
                       }else{
                        echo $successMsg = '<span class="alert-danger form-control">Email already exists</span>';
                       }  
                    }
                }else{
                    echo $successMsg = '<span class="alert-danger form-control">Please check Sample CSV File formate </span>';
                }
                }else{
                     echo  $successMsg = '<span class="alert-danger form-control">Error on file upload, please try again</span>';
                    
                }
            }
          } 
        }
        public function Export_emplist()
        {
            $cid=$this->session->userdata('id');
            if($this->session->userdata('name')=='admin'){
                    $result =$this->db->query("SELECT * FROM employees WHERE companyid='".$cid."'");
            }elseif($this->session->userdata('name')=='subadmin'){
        
                    $bid=$this->session->userdata('branch');
                        $result =$this->db->query("SELECT * FROM employees WHERE branchid='".$bid."' AND companyid='".$cid."' ");            
            }         
            $this->load->view('export',compact('result'));        
            redirect('employee'); 
        }
        public function Export_visitorlist()
        {
            $cid=$this->session->userdata('id');
            if($this->session->userdata('name')=='admin'){
        
                    $result =$this->db->query("SELECT * FROM visiters WHERE companyid='".$cid."' ");
        
            }elseif($this->session->userdata('name')=='subadmin'){
        
                    $bid=$this->session->userdata('branch');
                        $result =$this->db->query("SELECT * FROM visiters WHERE branchid='".$bid."' AND companyid='".$cid."' ");            
            }         
            $this->load->view('v_export',compact('result'));
            redirect('visitor'); 
        }
		//Controller for Listing Employee data
        public function Employee_List()
        {
	        $companyid=$this->session->userdata('id');
	        if($this->session->userdata('name')=='admin'){
	                $this->load->view('../../inc/head');
	                $this->load->view('../../inc/sidebar');
	                $this->load->view('../../inc/header');
	                $data=$this->Visiter_model->Admin_Employeelist($companyid);
	                $em = $this->Visiter_model->total_CEmployee($companyid);
	                //Bread Crumbs
				   		$this->make_bread->add('Employee Directory','employee' );
				   		$breadcrumb = $this->make_bread->output();

	                $this->load->view('emp-list',compact('data','em','breadcrumb'));
	                $this->load->view('../../inc/footer');
	       }elseif($this->session->userdata('name')=='subadmin'){
	                $this->load->view('../../inc/head');
	                $this->load->view('../../inc/sidebar');
	                $this->load->view('../../inc/header');
	                $branchid=$this->session->userdata('branch');
	                $data=$this->Visiter_model->Branch_Employeelist($companyid,$branchid);
	                $em = $this->Visiter_model->total_BEmployee($companyid,$branchid);
	                //Bread Crumbs
				   		$this->make_bread->add('Employee Directory','employee' );
				   		$breadcrumb = $this->make_bread->output();
	                $this->load->view('emp-list',compact('data','em','breadcrumb'));
	                $this->load->view('../../inc/footer');
	        }else{
	                redirect('login');
	        }
        }
        //Controller for show Add Employee form
        public function Add_Employee()
        {
                if($this->session->userdata('access')=='full'){
                        $this->load->view('../../inc/head');
                        $this->load->view('../../inc/sidebar');
                        $this->load->view('../../inc/header');
                        //Bread Crumbs
				   		$this->make_bread->add('Employee Directory','employee' );
				   		$this->make_bread->add('Add Employee','add-employee' );
				   		$data['breadcrumb'] = $this->make_bread->output();
                        $this->load->view('add-employee',$data);
                       // $this->load->view('add-employee');
                        $this->load->view('../../inc/footer');
                }else{
                        redirect('employee');
                }
        }
        public function Insert_Employee()
        {
        	$email = $this->input->post('email_id');
            $this->form_validation->set_rules('contact_num','Contact No.','required');
            $this->form_validation->set_rules('email_id', 'Email', 'required');
            $this->form_validation->set_rules('designation', 'Designation', 'required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            if ($this->form_validation->run() == FALSE)
            {
            	redirect('add-employee');
            }
            else
            {
            	$email_Emp=$this->Visiter_model->get_EmployeeEmail($email);
            	if (!empty($email_Emp)) {
					$email_err='Already exists please enter unique email';
					$this->load->view('../../inc/head');
	                $this->load->view('../../inc/sidebar');
	                $this->load->view('../../inc/header'); 
	                $this->load->view('add-employee',compact('email_err'));
	                $this->load->view('../../inc/footer'); 
				}else{
					$config['upload_path'] = 'uploads/';
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $this->load->library('upload',$config);

		                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&";
        				$password = substr( str_shuffle( $chars ), 0, 8 );
        				$sms= 'Your Username is '.$_POST['email_id'].' AND password is '.$password;
        		
                        if (!empty($_POST['email_id']) && isset($_POST['email_id']) )
                        {
                            /*print_r($_POST['email_id']);die;*/
                            $this->email->from('info@greetlog.com', 'greetlog.com');
                            $this->email->to($_POST['email_id']);
                            $this->email->subject('Your New Password...');
                            $this->email->message($sms);
                            $this->email->send();        
                        }
                        $middle = $this->input->post('mname');
    				    if(!empty($middle)){
    					    $empname= $this->input->post('fname')." ".$middle." ".$this->input->post('lname');
    					}else{
    					    $empname= $this->input->post('fname')." ".$this->input->post('lname');
    					}
                    $data = array('prefix' =>$this->input->post('prefix'),
                                'empname' =>$empname,
                                'email_id' =>$this->input->post('email_id'),
                                'contact_num' =>$this->input->post('contact_num'),
                                'gender' =>$this->input->post('gender'),
                                'dob' =>$this->input->post('dob'),
                                'designation' =>$this->input->post('designation'),
                                'password' =>base64_encode($password),
                                'companyid' =>$this->session->userdata('id'),
                                'createdate' =>date('Y-m-d')
                    );
                    if($this->upload->do_upload('profile_pic') )
                    {
                        $u_data =$this->upload->data();
                        $path=$u_data['file_name'];
                        $img = base_url().$config['upload_path'].$path;
                        $data['profile_pic']=$img;
                    }
                    if($this->session->userdata('name')=='subadmin'){
                        $data['branchid']=$this->session->userdata('branch');
                    }
		                $this->Visiter_model->AddEmployee($data);
		                redirect('employee');     
				}
            }        
    	}
      
        public function get_Employee()
        {

        	$id = $this->input->post('id');
        	$data=$this->Visiter_model->Edit_Employeelist($id)->result();
        	$response = $this->load->view('emp_form',compact('data'));
		}
        public function Edit_Employee($id)
        {
            if($this->session->userdata('name')=='admin' || $this->session->userdata('name')=='subadmin'){
                    $this->load->view('../../inc/head');
                    $this->load->view('../../inc/sidebar');
                    $this->load->view('../../inc/header');
                    $data=$this->Visiter_model->Edit_Employeelist($id);
                    $this->load->view('edit-employee',compact('data'));
                    $this->load->view('../../inc/footer');
            }else{
                    redirect('login');
            }
        }
        public function Update_Employee()
        {
            $id=$this->input->post('u_id');
            $data=$this->input->post();
            $email = $this->input->post('email_id');
            $emailid=$this->Visiter_model->Emp_id($id,$email);
            if (!empty($emailid)){
                $email_id=$emailid->email_id;
                $this->email->from('info@greetlog.com', 'greetlog.com');
                $this->email->to($_POST['email_id']);
                $this->email->subject('Email changed');
                $this->email->message('Your Email Id has been changed into '.$email_id);
                $this->email->send();

                $this->email->from('info@greetlog.com', 'greetlog.com');
                $this->email->to($email_id);
                $this->email->subject('Email changed');
                $this->email->message('Your Email Id has been changed into '.$_POST['email_id']);
                $this->email->send(); 
            }
            
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload',$config);
            if( ! $this->upload->do_upload('profile_pic') )
            {
                $error = array('error' => $this->upload->display_errors());        
            }
            else
            {
                $u_data =$this->upload->data();
                $path=$u_data['file_name'];
                $img = base_url().$config['upload_path'].$path;
                $data['profile_pic']=$img;
            }
                $data['modifieddate']=date('Y-m-d');
                unset($data['u_id']);
                $this->Visiter_model->Update_Employee($id,$data);
                redirect('employee');
        }
        //Controller for Add of status data on Enquiry Section
		public function Add_User()
		{
	
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$companyid=$this->session->userdata('id');
			$branchid=$this->session->userdata('branch');
			$data=$this->Visiter_model->Admin_Eployeelist($companyid);
			//Bread Crumbs
                           $this->make_bread->add('Employee Directory','employee' );
                           $this->make_bread->add('Admin Role','add-user' );
                           $breadcrumb = $this->make_bread->output();
			$this->load->view('add-user',compact('data','breadcrumb'));
			$this->load->view('../../inc/footer');
		} 
		//Controller Insert user data
        public function Insert_User()
        {
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload', $config);
            $data=$this->input->post();
            if( ! $this->upload->do_upload('profile_pic') )
            {   $this->load->view('../../inc/head');
                $this->load->view('../../inc/sidebar');
                $this->load->view('../../inc/header');
                $error = array('error' => $this->upload->display_errors()); 
                $this->load->view('add-user',$error);
                $this->load->view('../../inc/footer');            
            }
            else
            {
                $u_data =$this->upload->data();
                $path=$u_data['file_name'];
                $img = base_url().$config['upload_path'].$path;
                $data['profile_pic']=$img;
                $password=$this->input->post('Password');
                $data['password']=base64_encode($password);
                $data['companyid']=$this->session->userdata('id');
                $data['createdate']=date('Y-m-d');
                if($this->session->userdata('name')=='subadmin'){
                        $data['branchid']=$this->session->userdata('branch');
                }
                $this->Visiter_model->AddEmployee($data);
                redirect('user');
            }     
        } 
        public function User_list()
        {
            $companyid=$this->session->userdata('id');
            if($this->session->userdata('access')!=''){
                    $this->load->view('../../inc/head');
                    $this->load->view('../../inc/sidebar');
                    $this->load->view('../../inc/header');
                    $data=$this->Visiter_model->Admin_userlist($companyid);
                    $this->make_bread->add('Admin Role','add-user' );
                    $this->make_bread->add('User List','user' );
                    $breadcrumb = $this->make_bread->output();
                    $this->load->view('user-list',compact('data','breadcrumb'));
                    $this->load->view('../../inc/footer');
            }else{
                    redirect('login');
            }
        }
		//Controller for Show setup Section
        public function Setup_List()
        {
            if($this->session->userdata('access')=='full' || $this->session->userdata('access')=='view'){
                    $this->load->view('../../inc/head');
                    $this->load->view('../../inc/sidebar');
                    $this->load->view('../../inc/header');
                    $companyid=$this->session->userdata('id');
                    $data['company']=$this->Visiter_model->get_company($companyid);
                    $data['branch']=$this->Visiter_model->Branch_list($companyid);
                    	//Bread Crumbs
                        $companyname=$this->session->userdata('company');
                        //$this->make_bread->add($companyname);
				   		$this->make_bread->add('Setup','setup');
				   		$data['breadcrumb'] = $this->make_bread->output();
				   		$data['datahead'] = 'Setup List';
				   		$data['type'] = 'Admin';
                    $this->load->view('branch-list',$data);
                    $this->load->view('../../inc/footer');
            }else{
                    redirect('login');
            }
        }
     
		//Controller for view all type of status data on Enquiry Section
		public function Branch_List()
        {
                if($this->session->userdata('access')=='full' || $this->session->userdata('access')=='view' ){
                        $this->load->view('../../inc/head');
                        $this->load->view('../../inc/sidebar');
                        $this->load->view('../../inc/header');
                        $companyid=$this->session->userdata('id');
                        
                        $data['branch']=$this->Visiter_model->Branch_list($companyid);
                      //Bread Crumbs
				   		$this->make_bread->add('Manage Branches','branches' );
				   		$data['breadcrumb'] = $this->make_bread->output();
				   		$data['datahead'] = 'Branch List';
                        $data['add'] = 'Add Branch';
                        $this->load->view('branch-list',$data);
                        $this->load->view('../../inc/footer');
                }else{
                        redirect('login');
                }
        }
		//Controller for view all type of status data on Enquiry Section
		public function Add_Branch()
		{
		    if($this->session->userdata('access')=='full'){
    		    $this->make_bread->add('Manage Branches','branches' );
    			$this->make_bread->add('Add Branch','add-branch' );
    		    $data['breadcrumb'] = $this->make_bread->output();
    			$this->load->view('../../inc/head');
    			$this->load->view('../../inc/sidebar');
    			$this->load->view('../../inc/header');
    			$this->load->view('add-branch', $data);
    			$this->load->view('../../inc/footer');
		    }else{
                redirect('branches');
            }
		}
	
		
		//Controller for view all type of status data on Enquiry Section
		public function Insert_Branch()
		{
			if($this->session->userdata('access')=='full'){
				$email = $this->input->post('email_id');
				$contactno = $this->input->post('contactno');
				$email_b=$this->Visiter_model->get_BranchEmail($email);
				$cont_b=$this->Visiter_model->get_BranchContact($contactno);
				$data=$this->input->post();
				
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
				$this->form_validation->set_rules('companytype', 'Company Type', 'required');
                $this->form_validation->set_rules('branchhead', 'Admin Name', 'required');
                $this->form_validation->set_rules('contactno', 'Contact No.', 'required');
                $this->form_validation->set_rules('email_id', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('plan', 'Plan', 'required');
                $this->form_validation->set_rules('location', 'Location', 'required');
                if ($this->form_validation->run() == FALSE)
                {
                	redirect('add-branch');
                }
                else
                {
                	//echo"dfgd"; print_r($data);die;
                    if ((empty($email_b))&&(empty($cont_b))) {

						//$data=$this->input->post();
						$plan = $this->input->post('plan');
			        	$data['renewdate']=strtotime(date("Y-m-d"));
			        	$expireDate = strtotime(date('y-m-d', strtotime('+'.$plan)));
			        	$data['expirydate']=$expireDate;

						$config['upload_path'] = 'uploads/';
			            $config['allowed_types'] = 'jpg|jpeg|png|gif';
			            $this->load->library('upload',$config);
			            if( ! $this->upload->do_upload('logo') )
		                {
		                    $error = array('error' => $this->upload->display_errors());        
		                }
		                else
		                {
		                    $u_data =$this->upload->data();
		                    $path=$u_data['file_name'];
		                    $img = base_url().$config['upload_path'].$path;
		                    $data['logo']=$img;
		                    $data['companyid']=$this->session->userdata('id');
    						$data['createdate']=date('Y-m-d');
    						$id=$this->Visiter_model->AddBranch($data);
    						//print_r($id);die;
    						
		                    //$admin=$this->Visiter_model->get_branchIdByEmail($email);
		                    
		                    if (!empty($email) && isset($email)) {
        			            //print_r($email);die;
        			            //$msg = 'Please click here and generate your password '.base_url().'Generate_password/'.($id);
        			             $msg='<!DOCTYPE html><html lang=en><head>
                        <meta http-equiv=Content-Type content="text/html; charset=UTF-8" />
                        <meta name=viewport content="width=device-width initial-scale=1.0 maximum-scale=1.0" />
                        <meta name=viewport content="width=600,initial-scale = 2.3,user-scalable=no">
                        <!--[if !mso]>
                        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel=stylesheet>
                        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel=stylesheet>
                        <!-- <![endif]-->
                        <title>Greetlog </title>
                        <style type=text/css>body{width:100%;background-color:#f4f4f4;margin:0;padding:0;-webkit-font-smoothing:antialiased;mso-margin-top-alt:0;mso-margin-bottom-alt:0;mso-padding-alt:0}p,h1,h2,h3,h4{margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0}span.preheader{display:none;font-size:1px}html{width:100%}table{font-size:14px;border:0}@media only screen and (max-width:640px){.main-header{font-size:20px!important}.main-section-header{font-size:28px!important}.show{display:block!important}.hide{display:none!important}.align-center{text-align:center!important}.no-bg{background:none!important}.main-image img{width:440px!important;height:auto!important}.divider img{width:440px!important}.container590{width:440px!important}.container580{width:400px!important}.main-button{width:220px!important}.section-img img{width:320px!important;height:auto!important}.team-img img{width:100%!important;height:auto!important}}@media only screen and (max-width:479px){.main-header{font-size:18px!important}.main-section-header{font-size:26px!important}.divider img{width:280px!important}.container590{width:280px!important}.container590{width:280px!important}.container580{width:260px!important}.section-img img{width:280px!important;height:auto!important}}</style>
                        <!-- [if gte mso 9]><style type=”text/css”>body{font-family:arial,sans-serif!important}</style> <![endif]-->
                        </head>
                        <body class=respond leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 style>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td height=15 style=font-size:25px;line-height:15px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center height=70 style=height:70px> <a href style=display:block;border-style:none!important;border:0!important><img width=200 border=0 style=display:block;width:200px src=http://greetlogs.com/greetlog_admin/assets/img/logo-color.png alt /></a> </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=15 style=font-size:25px;line-height:15px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff class=bg_color>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center class=section-img> <a href style=border-style:none!important;display:block;border:0!important><img src=http://greetlogs.com/greetlog_admin/assets/img/mailer/mailer.jpg style=display:block;width:590px width=590 border=0 alt /></a> </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:20px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center style=color:#343434;font-size:24px;font-family:Quicksand,Calibri,sans-serif;font-weight:700;letter-spacing:3px;line-height:35px class=main-header>
                        <div style=line-height:35px> Hi <span style=color:#41ce4e>'.$this->input->post('branchhead').'</span> </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=40 align=center cellpadding=0 cellspacing=0 bgcolor=eeeeee>
                        <tr>
                        <td height=2 style=font-size:2px;line-height:2px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td align=center style=color:#343434;font-size:24px;font-family:Quicksand,Calibri,sans-serif;font-weight:700;letter-spacing:3px;line-height:35px class=main-header>
                        <div style=line-height:35px>
                        <p>Welcome to Greetlog!</p>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:16px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px><br>Tap the button below to creating your Greetlog account password. </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=200 cellpadding=0 cellspacing=0 bgcolor="41CE4E " style>
                        <tr>
                        <td height=10 style=font-size:10px;line-height:10px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center style="color:#fff;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 26px;">
                        <div style=line-height:26px> <a href="'.base_url().'Generate_password/'.($id).'" style=color:#fff;text-decoration:none>GENERATE PASSWORD</a> </div>
                        </td>
                        </tr>
                        <tr>
                        <td height=10 style=font-size:10px;line-height:10px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:18px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px><strong>Thank you,</strong> </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center style="color:#888;font-size:16px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px>
                        <p>Team Greetlog</p>
                        </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 14px;">
                        <div style=line-height:24px>If you did not make this request, you can safely ignore this mail. </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:20px;line-height:20px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=f4f4f4>
                        <tr>
                        <td height=25 style=font-size:25px;line-height:25px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td>
                        <table border=0 align=center cellpadding=0 cellspacing=0 style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0 class=container590>
                        <tr>
                        <td align=left style="color:#aaa;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px> <span style=color:#333>Copyright © Greetlog 2019</span> </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=25 style=font-size:25px;line-height:25px>&nbsp;</td>
                        </tr>
                        </table>
                        </body>
                        </html>';
			            
			        $config['charset'] = 'utf-8';

                    $config['newline'] = "\r\n";
        
                    $config['mailtype'] = 'html'; // or html
        
                    $config['validation'] = TRUE; // bool whether to validate email or not     
                    $this->email->initialize($config);
        			            $this->email->from('info@greetlog.com', 'greetlog.com');
        			            $this->email->to($email);
        			            $this->email->subject('Set password');
        			            $this->email->message($msg);
        			            $this->email->send();
        			            $this->session->set_flashdata('success_msg',"Your email is verify.");
        			            $this->load->view('login',compact('email'));
        			        } 
        			        redirect('branches');
		                   
		                }
						

					}
					else{
						
						$email_err='';
						$contact_err='';

						if (!empty($email_b)) {
							$email_err='Already exists please enter unique email';
						}
						if (!empty($cont_b)) {
							$contact_err='Already exists please enter unique number';
						}
						$this->load->view('../../inc/head');
						$this->load->view('../../inc/sidebar');
						$this->load->view('../../inc/header');
						$this->load->view('add-branch',compact('email_err', 'contact_err'));
						$this->load->view('../../inc/footer');
						//redirect('add-branch',$contact_err);
					}
                }

				

			}
		}
		//Create Passward for branch login
		public function Generate_BranchPass($id)
		{
				$data['password']=$this->Visiter_model->selectBranchdetails($id);
				$this->load->view('generate_password',$data);
			
		}
		//controller for Saved Password
		public function Update_BranchPassword()
		{
			
			$id=$this->input->post('u_id');
			$data=$this->input->post();
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[password]');
			$password=base64_encode($this->input->post('password'));
				//$c_password=base64_encode($this->input->post('c_password'));
				
				/*$data = array(
                         	  'password'     => $password,
                              'c_password'   => $c_password,
                            );*/
				// unset($data['u_id'], $data['c_password']);
				// $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
				/*$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
				$this->form_validation->set_rules('c_password', 'Password Confirmation', 'required|matches[password]');*/
				if (!$this->form_validation->run() == TRUE){
		                redirect('create_password/'.$id);
		        }
		        
		        
		        $pass['bpassword']=$password;
		        //$row = $q->result();
				$abc=$this->Visiter_model->Update_branchpass($pass,$id);
				//$email=$row[0]->email;
				//print_r($email);die;
				$this->session->set_flashdata('succ_msg','password has been created.');
				$this->load->view('login');

			
		}
		public function Edit_Branchlist($id)
		{
			if($this->session->userdata('access')=='full'){
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				$data=$this->Visiter_model->Edit_Branch($id);
			
				//bread crumbs
				$this->make_bread->add('Manage Branches','branches' );
				$this->make_bread->add('Edit Branch' );
		    	$breadcrumb = $this->make_bread->output();
				$this->load->view('edit-branch',compact('data','breadcrumb'));
			
				$this->load->view('../../inc/footer');
			}else{
				redirect('branches');
			}
		}
		public function Update_Branchlist()
		{
			if($this->session->userdata('name')=='admin'){
				$id=$this->input->post('u_id');
				$data=$this->input->post();
				
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload',$config);
            if( ! $this->upload->do_upload('logo') )
            {
                $error = array('error' => $this->upload->display_errors());        
            }
            else
            {
                $u_data =$this->upload->data();
                $path=$u_data['file_name'];
                    $img = base_url().$config['upload_path'].$path;
                    $data['logo']=$img;
            }
	            $data['modifieddate']=date('Y-m-d');
				unset($data['u_id']);
				$this->Visiter_model->Update_Branch($id,$data);
				redirect('branches');
			}
		}
		//Controller for Add Invites for Visitors
		public function Invite_visitor()
		{
			$companyid=$this->session->userdata('id');
			if($this->session->userdata('name')=='admin'){
				$data['invites']=$this->Visiter_model->Invites_list($companyid);
			}elseif($this->session->userdata('name')=='subadmin'){
				$branchid=$this->session->userdata('branch');
				$data['invites']=$this->Visiter_model->Invites_visiterlist($companyid,$branchid);
			}
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$this->make_bread->add('Visitor');
	   		$this->make_bread->add('Invite', 'invites');
	   		$data['breadcrumb'] = $this->make_bread->output();

			$data['company']=$this->Visiter_model->get_company($companyid);
			$data['branch']=$this->Visiter_model->get_branches($companyid);
			$data['emp']=$this->Visiter_model->Admin_Employeelist($companyid);
			
			//print_r($data['invites']);die;
			$this->load->view('invites',$data);
			$this->load->view('../../inc/footer');
		}
		//Insert Single visitor invites
		public function Insert_VisitorInvites()
		{
			$data=$this->input->post();
			$email=$this->input->post('email');
			$data['companyid']=$this->session->userdata('id');
			$data['createdate']=date('Y-m-d');
			$data['visitortype']="S";
			if($this->session->userdata('name')=='subadmin'){
				$data['branchid']=$this->session->userdata('branch');
			}
			for($i=0;$i<count($_POST['email']);$i++){

				$this->email->from('info@greetlog.com', 'Greetlog');
	            $this->email->to($_POST['email'][$i]);
	            $this->email->subject($_POST['title'][$i]);
	            $this->email->message($_POST['msg'][$i]);
	            $this->email->send();
			}
	        //print_r($_POST['email']);die;
			$this->Visiter_model->Add_Invites($data);
			redirect('invites');
		}
		//Insert Group visitor invites
		public function Insert_groupInvites()
		{
			$data=$this->input->post();
			$data['companyid']=$this->session->userdata('id');
			if($this->session->userdata('name')=='subadmin'){
				$data['branchid']=$this->session->userdata('branch');
			}
	        $data['visitor_name']=implode(",",$data['visitor_name']);
	        $data['contactnum']=implode(",",$data['contactnum']);
	        $data['email']=implode(",",$data['email']);
			$data['createdate']=date('Y-m-d');
			$data['visitortype']="G";
			for($i=0;$i<count($_POST['email']);$i++){
				$this->email->from('info@greetlog.com', 'Greetlog');
	            $this->email->to($_POST['email'][$i]);
	            $this->email->subject($_POST['title'][$i]);
	            $this->email->message($_POST['msg'][$i]);
	            $this->email->send();
	            //print_r($_POST['email'][$i]);
			}
			$this->Visiter_model->Add_Invites($data);
			redirect('invites');
		}
		//Insert Group visitor invites
		public function Insert_multiInvites()
		{
			for($i=0;$i<count($_POST['email']);$i++){
  			$data=[
  				'companyid'=>$this->session->userdata('id'),
                'visitortype'=>'M',
                /*'branch'      =>$_POST['branch'],*/
                'host'      =>$_POST['host'],
                'title'      =>$_POST['title'][$i],
                'visitor_name'      =>$_POST['visitor_name'][$i],
                'contactnum'      =>$_POST['contactnum'][$i],
                'email'      =>$_POST['email'][$i],
                'invitedate'      =>$_POST['invitedate'][$i],
                'invitetime'      =>$_POST['invitetime'][$i],
                'msg'      =>$_POST['msg'][$i],
                'summary'      =>$_POST['summary'][$i],
            ];
	            if($this->session->userdata('name')=='subadmin'){
					$data['branchid']=$this->session->userdata('branch');
				}
				$this->Visiter_model->Add_Invites($data);
				$this->email->from('info@greetlog.com', 'Greetlog');
	            $this->email->to($_POST['email'][$i]);
	            $this->email->subject($_POST['title'][$i]);
	            $this->email->message($_POST['msg'][$i]);
	            $this->email->send();
	            //print_r($_POST['email'][$i]);
			}
			redirect('invites');	
		}
		/*//Controller for Add Group Invites for Visitors
		public function Group_Invite()
		{
			$companyid=$this->session->userdata('id');
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$data['company']=$this->Visiter_model->get_company($companyid);
			$data['branch']=$this->Visiter_model->get_branches($companyid);
			$data['emp']=$this->Visiter_model->Admin_Employeelist($companyid);
			$this->load->view('groupinvites',$data);
			$this->load->view('../../inc/footer');
		}
		//Insert Single visitor invites
		public function Insert_VisitorInvites()
		{
			$data=$this->input->post();
			$data['companyid']=$this->session->userdata('id');
			if($this->session->userdata('name')=='subadmin'){
				$data['branchid']=$this->session->userdata('branch');
			}
			$data['createdate']=date('Y-m-d');
			$this->Visiter_model->Add_Invites($data);
			redirect('invites');
		}
		//Insert Group visitor invites
		public function Insert_groupInvites()
		{
			$data=$this->input->post();
			$data['companyid']=$this->session->userdata('id');
			if($this->session->userdata('name')=='subadmin'){
				$data['branchid']=$this->session->userdata('branch');
			}
	        $data['visitor_name']=implode(",",$data['visitor_name']);
	        $data['host']=implode(",",$data['host']);
	        $data['contactnum']=implode(",",$data['contactnum']);
	        $data['email']=implode(",",$data['email']);
	        $data['invitedate']=implode(",",$data['invitedate']);
	        $data['invitetime']=implode(",",$data['invitetime']);
	        $data['msg']=implode(",",$data['msg']);
    		
			$data['createdate']=date('Y-m-d');
			$this->Visiter_model->Add_Invites($data);
			redirect('invites');
		}*/
		public function Visitor_List()
		{
			$companyid=$this->session->userdata('id');
			if($this->session->userdata('name')=='admin'){
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				//bread crumbs
				$this->make_bread->add('Visitor List','visitorlog' );
			    $breadcrumb = $this->make_bread->output();

				$data=$this->Visiter_model->Admin_Visitorlist($companyid);
				$this->load->view('visitor-list',compact('data', 'breadcrumb'));
				$this->load->view('../../inc/footer');
			}elseif($this->session->userdata('name')=='subadmin'){
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
					$this->make_bread->add('Visitor List','visitorlog' );
			    $breadcrumb = $this->make_bread->output();

				$branchid=$this->session->userdata('branch');
				$data=$this->Visiter_model->Branch_Visitorlist($companyid,$branchid);
				//$this->load->view('visitor-list',compact('data'));
				
            	$this->load->view('visitor-list',compact('data', 'breadcrumb'));
				$this->load->view('../../inc/footer');
			}else{
				redirect('login');
			}
		}
		public function Add_Visitor()
		{
			if($this->session->userdata('name')=='admin' || $this->session->userdata('name')=='subadmin'){
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				$this->load->view('add-visitor');
				$this->load->view('../../inc/footer');
			}else{
				redirect('login');
			}
		}
		//Controller for view all type of status data on Enquiry Section
		public function Checkin_Visiter()
		{
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$data=$this->Visiter_model->checkinvisiter();
			$this->load->view('visiter');
			$this->load->view('../../inc/footer');
		}
		public function Insert_Visitor()
		{
			$data=$this->input->post();
			$data['companyid']=$this->session->userdata('id');
			if($this->session->userdata('name')=='subadmin'){
				$data['branchid']=$this->session->userdata('branch');
			}
			$abc=$this->Visiter_model->InsertVisiterdetails($data);
			redirect('visitor');
		}

	/*Created By divya*/
// 		public function Dashboard()
// 		{
// 			if($this->session->userdata('name')=='admin' || $this->session->userdata('name')=='subadmin'){
// 				$this->load->view('../../inc/head');
// 				$this->load->view('../../inc/sidebar');
// 				$this->load->view('../../inc/header');
// 				$this->load->view('dashboard');
// 				$this->load->view('../../inc/footer');
// 			}else{
// 				redirect('login');
// 			}
// 		}
		//Controller for view forgot_password
		public function Forgot_Password($id)
		{
			if($this->session->userdata('name')!='admin' && $this->session->userdata('name')!='subadmin'){
				$data['password']=$this->Visiter_model->selectCompanydetails($id);
				$this->load->view('forgot_password',$data);
			}else {
				redirect('dashboard');
			}
			
		}

		//To show Add company Details
		public function Add_companydetails()
		{
			if($this->session->userdata('access')=='full'){
				$id=$this->session->userdata('id');
				$companyname=$this->session->userdata('companyname');
				$data['c_detail']=$this->Visiter_model->selectCompanydetails($id);

				// add the first crumb, the segment being added to the previous crumb's URL
			    $this->make_bread->add($companyname);
			    $this->make_bread->add('Setting');
			    // now, let's store the output of the breadcrumb in a variable and show it (preferably inside a view)
			    $data['breadcrumb'] = $this->make_bread->output();
			    
				$this->load->view('../../inc/head');
				$this->load->view('../../inc/sidebar');
				$this->load->view('../../inc/header');
				$this->load->view('add_company',$data);
				$this->load->view('../../inc/footer');

			}else{
				redirect('login');
			}
		}
		// Registration Form 
		public function Company_registration()
		{
				
				$email = $this->input->post('email');
				$admin=$this->Visiter_model->get_companyIdByEmail($email);
				//print_r($admin);die;
				if (!empty($admin)) {

					$this->session->set_flashdata('email_err','Email already registered. Please enter unique email');
					
					$this->load->view('registration');
					
				}else{
				    $middle = $this->input->post('middlename');
				    if(!empty($middle)){
					    $ownername= $this->input->post('ownername')." ".$middle." ".$this->input->post('lastname');
					}else{
					    $ownername= $this->input->post('ownername')." ".$this->input->post('lastname');
					}
					
					$data = array('companytype' => $this->input->post('companytype'), 
                                'companyname' => $this->input->post('companyname'), 
                                'ownername' =>$ownername, 
                                'email' => $this->input->post('email'),
                                'country' => $this->input->post('country'),
                                'contactno' => $this->input->post('contactno'),
                                'createdate' =>date('Y-m-d')
                    );
                    
					
					$cid=$this->Visiter_model->C_registration($data);
                    
                    $data= array('companyid' => $cid, 
                                'empname' =>$ownername, 
                                'email_id' => $this->input->post('email'), 
                                'contact_num' => $this->input->post('contactno'),
                                'createdate' =>date('Y-m-d')
                    );
                    $this->Visiter_model->AddEmployee($data);
                    
					$admin=$this->Visiter_model->get_companyIdByEmail($email);
					
					if (!empty($email) && isset($email)) {
			            //print_r($email);die;
			            //$msg = 'Please click here and generate your password '.base_url().'create_password/'.($admin->id);
			            $msg='<!DOCTYPE html><html lang=en><head>
                        <meta http-equiv=Content-Type content="text/html; charset=UTF-8" />
                        <meta name=viewport content="width=device-width initial-scale=1.0 maximum-scale=1.0" />
                        <meta name=viewport content="width=600,initial-scale = 2.3,user-scalable=no">
                        <!--[if !mso]>
                        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel=stylesheet>
                        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel=stylesheet>
                        <!-- <![endif]-->
                        <title>Greetlog </title>
                        <style type=text/css>body{width:100%;background-color:#f4f4f4;margin:0;padding:0;-webkit-font-smoothing:antialiased;mso-margin-top-alt:0;mso-margin-bottom-alt:0;mso-padding-alt:0}p,h1,h2,h3,h4{margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0}span.preheader{display:none;font-size:1px}html{width:100%}table{font-size:14px;border:0}@media only screen and (max-width:640px){.main-header{font-size:20px!important}.main-section-header{font-size:28px!important}.show{display:block!important}.hide{display:none!important}.align-center{text-align:center!important}.no-bg{background:none!important}.main-image img{width:440px!important;height:auto!important}.divider img{width:440px!important}.container590{width:440px!important}.container580{width:400px!important}.main-button{width:220px!important}.section-img img{width:320px!important;height:auto!important}.team-img img{width:100%!important;height:auto!important}}@media only screen and (max-width:479px){.main-header{font-size:18px!important}.main-section-header{font-size:26px!important}.divider img{width:280px!important}.container590{width:280px!important}.container590{width:280px!important}.container580{width:260px!important}.section-img img{width:280px!important;height:auto!important}}</style>
                        <!-- [if gte mso 9]><style type=”text/css”>body{font-family:arial,sans-serif!important}</style> <![endif]-->
                        </head>
                        <body class=respond leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 style>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td height=15 style=font-size:25px;line-height:15px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center height=70 style=height:70px> <a href style=display:block;border-style:none!important;border:0!important><img width=200 border=0 style=display:block;width:200px src=http://greetlogs.com/greetlog_admin/assets/img/logo-color.png alt /></a> </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=15 style=font-size:25px;line-height:15px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=ffffff class=bg_color>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center class=section-img> <a href style=border-style:none!important;display:block;border:0!important><img src=http://greetlogs.com/greetlog_admin/assets/img/mailer/mailer.jpg style=display:block;width:590px width=590 border=0 alt /></a> </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:20px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center style=color:#343434;font-size:24px;font-family:Quicksand,Calibri,sans-serif;font-weight:700;letter-spacing:3px;line-height:35px class=main-header>
                        <div style=line-height:35px> Hi <span style=color:#41ce4e>'.$this->input->post('ownername').$this->input->post('lastname').'</span> </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=40 align=center cellpadding=0 cellspacing=0 bgcolor=eeeeee>
                        <tr>
                        <td height=2 style=font-size:2px;line-height:2px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td align=center style=color:#343434;font-size:24px;font-family:Quicksand,Calibri,sans-serif;font-weight:700;letter-spacing:3px;line-height:35px class=main-header>
                        <div style=line-height:35px>
                        <p>Welcome to Greetlog!</p>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:16px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px><br>Tap the button below to creating your Greetlog account password. </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=200 cellpadding=0 cellspacing=0 bgcolor="41CE4E " style>
                        <tr>
                        <td height=10 style=font-size:10px;line-height:10px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center style="color:#fff;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 26px;">
                        <div style=line-height:26px> <a href="'.base_url().'create_password/'.($admin->id).'" style=color:#fff;text-decoration:none>GENERATE PASSWORD</a> </div>
                        </td>
                        </tr>
                        <tr>
                        <td height=10 style=font-size:10px;line-height:10px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:18px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px><strong>Thank you,</strong> </div>
                        </td>
                        </tr>
                        <tr>
                        <td align=center style="color:#888;font-size:16px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px>
                        <p>Team Greetlog</p>
                        </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:25px;line-height:20px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 width=400 align=center cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td align=center style="color:#888;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 14px;">
                        <div style=line-height:24px>If you did not make this request, you can safely ignore this mail. </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=20 style=font-size:20px;line-height:20px>&nbsp;</td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        <table border=0 width=100% cellpadding=0 cellspacing=0 bgcolor=f4f4f4>
                        <tr>
                        <td height=25 style=font-size:25px;line-height:25px>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align=center>
                        <table border=0 align=center width=590 cellpadding=0 cellspacing=0 class=container590>
                        <tr>
                        <td>
                        <table border=0 align=center cellpadding=0 cellspacing=0 style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0 class=container590>
                        <tr>
                        <td align=left style="color:#aaa;font-size:14px;font-family:"Work Sans", Calibri, sans-serif; line-height: 24px;">
                        <div style=line-height:24px> <span style=color:#333>Copyright © Greetlog 2019</span> </div>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td height=25 style=font-size:25px;line-height:25px>&nbsp;</td>
                        </tr>
                        </table>
                        </body>
                        </html>';
			            
			        $config['charset'] = 'utf-8';

                    $config['newline'] = "\r\n";
        
                    $config['mailtype'] = 'html'; // or html
        
                    $config['validation'] = TRUE; // bool whether to validate email or not     
                    $this->email->initialize($config);
			            $this->email->from('info@greetlog.com', 'Greetlog');
			            $this->email->to($email);
			            $this->email->subject('Create Your Password');
			            $this->email->message($msg);
			            $this->email->send();
			        } 
			        $id = $admin->id;
			        $this->load->view('login',compact('email'));	
			        /*$this->login_1($id);*/
				}
		        
		

		}

		//controller for Generate/Forgot Password
		public function Update_Password()
		{
			
				$id=$this->input->post('u_id');
			
				$password=base64_encode($this->input->post('password'));
				$c_password=base64_encode($this->input->post('c_password'));
				
				$data = array(
                         	  'password'     => $password,
                              'c_password'   => $c_password,
                            );
				// unset($data['u_id'], $data['c_password']);
				// $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
				$this->form_validation->set_rules('c_password', 'Password Confirmation', 'required|matches[password]');
				
				// $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|alpha_numeric|callback_password_check');
				// $this->form_validation->set_rules('c_password', 'Confirm Password', 'required|matches[passconf]|min_length[8]|alpha_numeric|callback_password_check');
				
				if (!$this->form_validation->run() == TRUE){

		                redirect('create_password/'.$id);
		        }
		        $q = $this->Visiter_model->companyList_model($id);
		        unset($data['u_id'], $data['c_password']);
		        $row = $q->result();
		      	//  print_r($row[0]->email);die;
				$abc=$this->Visiter_model->Update_passModel($data,$id);
				$email=$row[0]->email;
				//print_r($email);die;
				$this->session->set_flashdata('succ_msg','Your password has been created.');
				$this->load->view('login',compact('email'));

			
		}

		//view verify email
		public function verify_emailpage(){

			if(($this->session->userdata('name')!='admin') && ($this->session->userdata('name')!='subadmin')){
				
				$this->load->view('verify_email');
			}else {
				redirect('dashboard');
			}
			
		}

		//
		public function verification_email(){

			$email=$this->input->post('email');
			$admin=$this->Visiter_model->get_companyIdByEmail($email);
			//print_r($admin);die;
			if (!empty($admin)) {
				if (!empty($email) && isset($email)) {
			            //print_r($email);die;
			            $msg = 'Please click here and generate your password '.base_url().'create_password/'.($admin->id);
			            $this->email->from('info@greetlog.com', 'greetlog.com');
			            $this->email->to($email);
			            $this->email->subject('Set password');
			            $this->email->message($msg);
			            $this->email->send();
			            $this->session->set_flashdata('success_msg',"Your email is verify.");
			            $this->load->view('login',compact('email'));
			        } 
			}else{
				$this->session->set_flashdata('msg','Email doesnt registered. Please enter registered email');
				$error = "Email doesn't registered. Please enter registered email";
			 	$this->load->view('verify_email',compact('error'));
			}
		}
		
		//insert company details
		public function insertCompany()
		{
			if($this->session->userdata('name')=='admin'){

					$config['upload_path'] = 'uploads/';

		            $config['allowed_types'] = 'jpg|jpeg|png|gif';
		            $this->load->library('upload',$config);
		            $this->upload->do_upload('logo');
		            
		            /*if($this->upload->do_upload('logo')){
		            	

		            }else{
		            	$error = array('error' => $this->upload->display_errors());
		                $this->load->view('add_company',compact('error'));
		            }*/
		            $img = base_url().$config['upload_path'].$_FILES['logo']['name'];
		            
		            $id=$this->input->post('u_id');
					$data=$this->input->post();
		        	$plan = $this->input->post('plan');
		        	//print_r($img);die;
		        	$data['logo']=$img;
		        	// date_default_timezone_set("Asia/Kolkata");
		        	$data['renewdate']=strtotime(date("Y-m-d"));
		        	$expireDate = strtotime(date('y-m-d', strtotime('+'.$plan)));
		        	$data['expirydate']=$expireDate;
		        	unset($data['u_id']);

					//print_r($expireDate);die;
		        	$id=$this->session->userdata('id');
		        	$companyname=$this->session->userdata('companyname');
					$this->Visiter_model->insertCompanydetails($data,$id);
					//breadcrumbs
					$this->make_bread->add($companyname);
			   		$this->make_bread->add('Setting','add_company');
			   		$this->make_bread->add('CompanyList');
			   		$data['breadcrumb'] = $this->make_bread->output();

		   			// $this->load->view('../../inc/head');
					// $this->load->view('../../inc/sidebar');
					// $this->load->view('../../inc/header');
					redirect('companyList',$data);
					//$this->load->view('../../inc/footer');
				            
				
			}else{
				redirect('login');
			}
		}

		//Controller for view comapny list
		public function companyList()
		{
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$id=$this->session->userdata('id');
			$companyname=$this->session->userdata('companyname');
			$data['info'] = $this->Visiter_model->companyList_model($id);

			//breadcrumbs
			$this->make_bread->add($companyname);
	   		$this->make_bread->add('Setting','add_company');
	   		$this->make_bread->add('CompanyList');
	   		$data['breadcrumb'] = $this->make_bread->output();
			$this->load->view('companyList',$data);
			$this->load->view('../../inc/footer');
		}
		//Controller for view comapny Details
		public function viewComapny_details($id)
		{
			$this->load->view('../../inc/head');
			$this->load->view('../../inc/sidebar');
			$this->load->view('../../inc/header');
			$data['c_detail']=$this->Visiter_model->selectCompanydetails($id);
			//breadcrumbs
			// $this->make_bread->add($companyname);
	   		$this->make_bread->add('Setting','add_company');
	   		$this->make_bread->add('companyList');
	   		$data['breadcrumb'] = $this->make_bread->output();

			$this->load->view('showComapny_details',$data);
			$this->load->view('../../inc/footer');
		}

		//Controller for Update comapny Details
		public function Update_data()
		{
			$id=$this->input->post('u_id');
			$data=$this->input->post();
			$data['modifieddate']=date('Y-m-d');
			unset($data['u_id']);
			// if (!empty($_FILES['logo']['name'])) {
			$config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload',$config);
            $this->upload->do_upload('logo');
            
            $u_data =$this->upload->data();
            $path=$u_data['file_name'];
            $img = base_url().$config['upload_path']. $path;
            
            $logo=$_FILES['logo']['name'];
            $img = base_url().$config['upload_path']. $_FILES['logo']['name'];
            //print_r($logo);die;
            if($logo!=""){
            	$data['logo']=$img;
            	$this->Visiter_model->editCompanydetails($data, $id);
				redirect(base_url('companyList'));
            }else{
            	$this->Visiter_model->editCompanydetails($data, $id);
				redirect(base_url('companyList'));
            }     

			
		}
		/*Closed By divya*/
		/* stripe pyment controllare start here*/
	public function index()
	{
		$this->load->view('stripe/product_form');		
	}

	public function check()
	{
		
		//check whether stripe token is not empty
		if(!empty($_POST['stripeToken']))
		{
			//get token, card and user info from the form
			$token  = $_POST['stripeToken'];
			$name = $_POST['name'];
			$email = $_POST['email'];
			$card_num = $_POST['card_num'];
			$card_cvc = $_POST['cvc'];
			$card_exp_month = $_POST['exp_month'];
			$card_exp_year = $_POST['exp_year'];
			
			//include Stripe PHP library
			require_once APPPATH."third_party/stripe/init.php";
			
			//set api keypk_test_nWA7XE3W258YGy761Mdy4Dxi00xrc2qYSF
			$stripe = array(
			  "secret_key"      => "sk_test_0mo1br6RgU9LeSY20JRrqwRV00XnUVnGiX",
			  "publishable_key" => "pk_test_nWA7XE3W258YGy761Mdy4Dxi00xrc2qYSF
"
			);
			
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			
			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $email,
				'source'  => $token
			));
			
			//item information
			$itemName = "Stripe Donation";
			$itemNumber = "PS123456";
			$itemPrice = 100;
			$currency = "usd";
			$orderID = "SKA92712382139";
			
			//charge a credit or a debit card
			$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemNumber,
				'metadata' => array(
					'item_id' => $itemNumber
				)
			));
			
			//retrieve charge details
			$chargeJson = $charge->jsonSerialize();

			//check whether the charge is successful
			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
			{
				//order details 
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");
			
				
				//insert tansaction data into the database
				$dataDB = array(
					'name' => $name,
					'email' => $email, 
					'card_num' => $card_num, 
					'card_cvc' => $card_cvc, 
					'card_exp_month' => $card_exp_month, 
					'card_exp_year' => $card_exp_year, 
					'item_name' => $itemName, 
					'item_number' => $itemNumber, 
					'item_price' => $itemPrice, 
					'item_price_currency' => $currency, 
					'paid_amount' => $amount, 
					'paid_amount_currency' => $currency, 
					'txn_id' => $balance_transaction, 
					'payment_status' => $status,
					'created' => $date,
					'modified' => $date
				);
				if ($this->db->insert('orders', $dataDB)) {
					if($this->db->insert_id() && $status == 'succeeded'){
						$data['insertID'] = $this->db->insert_id();
						$this->load->view('stripe/payment_success', $data);
						// redirect('Welcome/payment_success','refresh');
					}else{
						echo "Transaction has been failed";
					}
				}
				else
				{
					echo "not inserted. Transaction has been failed";
				}

			}
			else
			{
				echo "Invalid Token";
				$statusMsg = "";
			}
		}
	}

	public function payment_success()
	{
		$this->load->view('stripe/payment_success');
	}

	public function payment_error()
	{
		$this->load->view('stripe/payment_error');
	}

	public function help()
	{
		$this->load->view('help');
	}
	}