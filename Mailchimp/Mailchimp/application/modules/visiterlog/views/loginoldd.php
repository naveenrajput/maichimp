<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  
  <title>Greetlog - Login</title>
  
  <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
 <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<style>



</style>
</head>

<body class="bg-gradient-primary">
   <div class="wrapper fadeInDown">
      <div class="fadeIn first p-t-30  p-b-30">
      <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="180" class="" alt="Greetlog" />
    </div>
    <?php if ( $this->session->flashdata('msg')): ?>
                  <div class="alert alert-success" id="flash">
                    <h4 style="color: red;"><?php echo $this->session->flashdata('msg');?></h4>
                  </div>
              <?php endif; ?>
  <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <div class="p-2 ">
  <div class="text-center p-t-20  p-b-20">
               <h1 class="h5 text-gray-900 ">Log in to the Greetlog!</h1>
         
     </div>
  </div>
              
    <!-- Login Form -->
    <form action="<?php echo base_url('chklogin');?>" method="post">
      <input type="email" id="login" class="fadeIn second " name="email" value="<?php if(isset($email)){echo $email;} ?>" placeholder="Login">
      <input type="password" id="password" class="fadeIn third " name="password" placeholder="Password">
      <button type="submit" class="fadeIn btn  btn-green shadow-sm m-t-20 m-b-20" >Log In</button>
    </form>

    <!-- Remind Passowrd -->
     <div id="formFooter">
      <a class="underlineHover" href="<?php echo base_url('forgot_link') ?>">Forgot Password?</a>
    </div>

  </div>
</div>

 
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>

  <script src="<?php echo base_url() ?>assets/js/custom.js"></script>

</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript">
  $(function() {
    $('#flash').delay(500).fadeIn('normal', function() {
      $(this).delay(500).fadeOut();
    });
  });
</script>