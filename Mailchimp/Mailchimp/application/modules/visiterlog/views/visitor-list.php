<div class="container-fluid">

   <!-- Page Heading -->
   <div class="row">
      <div class="col-md-10">
         <h1 class="h3 mb-2 text-gray-800">Visitor List</h1>
            <?php
      if(isset($breadcrumb)&&  !is_null($breadcrumb)){
      ?> 
   <div class="span10" style="margin-left:5px;">
      <ul class="breadcrumb">
         <?php
            echo $breadcrumb ;             
            ?>     
      </ul>
   </div>
   <?php 
      }
      ?>
      </div>
      <?php if($access=='full'){ ?>
      <div class="col-md-2">
         <a href="<?php echo base_url('vexport'); ?>" class="btn btn-green"><i class="exp"></i>Export</a>
      </div>
      <?php } ?>
   </div>
   <!-- DataTales Example -->
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">Visitor Datalist </h6>
      </div>
      <div class="card-body">
         <div class="table-responsive">
            <table id="example" class="table table-hover responsive nowrap" style="width:100%">
               <thead>
                  <tr>
                     <th>Visitor Name</th>
                     <th>Contact No.</th>
                     <th>Contact Person</th>
                     <th>Purpose</th>
                     <th>Status</th>
                    <!--  <th>Create Date</th> -->
                     <!-- <th>Actions</th> -->
                  </tr>
               </thead>
               <tbody>
                  <?php if (isset($data)) {
                     $i=1; foreach ($data as $emp) {
                     ?>
                  <tr>
                     <td>
                        <a href="#">
                           <div class="d-flex align-items-center">
                              <div class="avatar avatar-blue mr-3"><img src="<?php base_url('uploads/men.png'); ?>" class="avatar-img"></div>
                              <div class="">
                                 <p class="font-weight-bold mb-0"><?php echo $emp->name; ?></p>
                                 <p class="text-muted mb-0"><?php echo $emp->email; ?></p>
                              </div>
                           </div>
                        </a>
                     </td>
                     <td><?php echo $emp->contactno; ?></td>
                     <td><?php echo $emp->contactperson; ?></td>
                     <td><?php echo $emp->purpose; ?></td>
                     <td><?php echo $emp->status; ?></td>
                     <!-- <td>
                        <div class="badge badge-success badge-success-alt"><?php //echo $detail->plan; ?></div>
                        </td> -->
                     <!-- <td>
                        <div class="dropdown">
                          <button class="btn btn-sm btn-icon" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-h" data-toggle="tooltip" data-placement="top"
                                  title="Actions"></i>
                              </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                             <a class="dropdown-item" href="#"><i class="far fa-eye mr-2"></i> Btn</a>
                            <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt mr-2"></i> Edit Profile</a>
                            <a class="dropdown-item text-danger" href="#"><i class="far fa-trash-alt mr-2"></i> Remove</a>
                          </div>
                        </div>
                        </td> -->
                  </tr>
                  <?php $i++; } } ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script>
   $(document).ready(function() {
     $("#example").DataTable({
       aaSorting: [],
       responsive: true,
   
       columnDefs: [
         {
           responsivePriority: 1,
           targets: 0
         },
         {
           responsivePriority: 2,
           targets: -1
         }
       ]
     });
   
     $(".dataTables_filter input")
       .attr("placeholder", "Search here...")
       .css({
         width: "300px",
         display: "inline-block"
       });
   
    $('[data-toggle="tooltip"]').tooltip();
   });
   
</script>