<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
	
  <title>Greetlog - Dashboard</title>
	
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
 <!-- Custom styles for this page 
  -->
 <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css" rel="stylesheet">

  <link href="<?php echo base_url() ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<style>



</style>
</head>

<body class="bg-gradient-primary">

  <div class="wrapper fadeInDown">
      <div class="fadeIn first p-t-15  p-b-20">
      <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="180" class="" alt="Greetlog" />
    </div>
  <div id="formContent">
    <!-- Tabs Titles -->
	
		
    <!-- Icon -->
    <!---<div class="fadeIn first p-t-30 ">
      <img src="<?php //echo base_url() ?>assets/img/logo-color.png" width="180" class="" alt="Greetlog" />
    </div>--->
	
	<div class="p-2 ">
	<div class="text-center p-t-20  p-b-10">
               <h1 class="h5 text-gray-900 ">Create an Account!</h1>
               
     </div>
	</div>
    <!-- Login Form -->
    <form method="post" action="<?php echo base_url('registration');?>" class="needs-validation" novalidate>
          <select id="inputState" class="form-control  fadeIn first custom-select01" name="companytype" required="">
              <option value="" selected>Company Profile</option>
              <option value="Company">Company</option>
              <option value="Corporation">Corporation</option>
              <option value="School/Institute">School/Institute</option>
              <option value="College/University">College/University</option>

          </select>

          <!--  <div class="invalid-feedback">
        Looks good!
      </div> --><label  id="validationCustom01">company name</label>
          <input type="text" class="form-control fadeIn first " name="companyname" placeholder="Company Name"  required="">
          <!-- <div class="invalid-feedback">
        Looks good!
      </div> -->
          <input type="text" class="form-control fadeIn first " name="ownername" placeholder="Name" id="validationCustom02" required="">
         <!--  <div class="invalid-feedback">
        Looks good!
      </div> -->
          <input type="text" class="form-control fadeIn second  "required="" name="email" placeholder="Email" id="validationCustom03">
         <!--  <div class="invalid-feedback" >
        Looks good!
      </div> -->
          <input type="tel" class="form-control fadeIn third dis_code" name="contactno" id="validationCustom04" required="">
         <!--  <div class="invalid-feedback" >
        Looks good!
      </div> -->
      <button class="fadeIn third btn  btn-green shadow-sm m-t-10 m-b-10"  type="submit" >Submit</button>
    
    </form>
  <!-- Remind Passowrd -->
     <div id="formFooter">
      <a class="underlineHover fadeIn third" href="<?php echo base_url('login') ?>">Already have an account? Login!</a>
    </div>

  </div>
</div>
   
 
<!-- Modal -->
<div class="modal fade" id="demo3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog opacity-animate3">
    <div class="modal-content">
     
      <div class="modal-body">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	  
        <p>
                            “ War is an ugly thing, but not the ugliest of things. The decayed and degraded state of moral and patriotic feeling which thinks that nothing is worth war is much worse. The person who has nothing for which he is willing to fight, nothing which is more important than his own personal safety, is a miserable creature and has no chance of being free unless made and kept so by the exertions of better men than himself. ” — John Stuart Mill
                        </p>
                    </div>
          <div class="modal-footer text-center">
                       
                        <button class="btn btn-green shadow-sm" data-dismiss="modal" aria-hidden="true">
                            Okay
                        </button>
                    </div>          
        
      </div>
      
    </div>
  </div>
</div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 
  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/intlTelInput.js"></script>

  <!----<script src="<?php// echo base_url() ?>assets/js/custom.js"></script>----->
  
 


<script>

  // $(document).ready(function() {
      
  //      var form=$("#myForm");
  //      $("#smt").click(function(){

  //        $.ajax({
  //                type:"POST",
  //                url:'registration',
  //                data:form.serialize(),
  //                success: function(response){
                  
  //                    $('#demo3').modal('show'); 
                
  //                }
  //            });

  //      });

  //  });


$(".dis_code").intlTelInput({
  utilsScript: "<?php echo base_url() ?>assets/js/utils.js"
});

var select_designatfirst = $('#inputState'),
  validationCustom01 = $('#validationCustom01');

select_designatfirst.on('change', function () {
  validationCustom01.text(select_designatfirst.find(':selected').text() +  ' Name' );
});



// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

</script>

</body>

</html>