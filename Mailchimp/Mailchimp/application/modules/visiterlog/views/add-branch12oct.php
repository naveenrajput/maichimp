<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Branch Form</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div>
    <div class="card-body">
      <div class="form">
        <form  method="post" id="myForm" enctype="multipart/form-data" action="<?php echo base_url('insert-branch'); ?>" data-toggle="validator" role="form">
          <div class="form-row">
            <div class="form-group col-md-4">
              <select id="inputState" class="form-control  fadeIn first custom-select01" name="companytype" required="" placeholder="Company Type">
                <option value="">Select Type</option>
                <option value="Company">Company</option>
                <option value="Corporation">Corporation</option>
                <option value="School/Institute">School/Institute</option>
                <option value="College/University">College/University</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="branchname" placeholder="Company Name" value="<?php echo $name=$this->session->userdata('company'); ?>" required>
            </div>
            <!--pic start-->
              <div class="form-group col-md-4">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="logo">
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                        </div>
                    </div>
                </div><span id="err_img"></span></div>
            <!--pic close-->
            <div class="form-group col-md-6">
              <input type="text" class="form-control" name="branchhead" placeholder="Admin Name" required>
            </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control" name="contactno" placeholder="Admin Contact" required>
            </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control" name="email_id" placeholder="Admin Email" ><?php echo form_error('email_id'); ?>
            </div>
                        <div class="form-group col-md-6">
                           <select name="plan" class="form-control">
                              <option value="">Select Plan</option>
                              <option value="15 days">15 Days Trial</option>
                              <option value="3 months">3 Months</option>
                              <option value="6 months">6 Months</option>
                              <option value="12 months">12 Months</option>     
                           </select>
                        </div>
            
                        <div class="form-group col-md-6">
                            <label>Send Type : </label>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email">
                              <label class="custom-control-label" for="customRadioInline1">By Email</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message">
                              <label class="custom-control-label" for="customRadioInline2">By Message</label>
                            </div> 
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline3" name="sendtype" class="custom-control-input" value="whatsapp">
                              <label class="custom-control-label" for="customRadioInline3">By Whats App</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Access Provide : </label>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full">
                              <label class="custom-control-label" for="customRadioInline4">Full Access</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view">
                              <label class="custom-control-label" for="customRadioInline5">View Only</label>
                            </div>
                        </div>
            <div class="form-group col-md-6">
              <input type="text" class="form-control" name="slogan" placeholder="Company Tag">
            </div>
                        
            <div class="form-group col-md-6">
              <input type="text" id="pac-input" class="form-control" name="location" placeholder="Location" required>
            </div>
          </div>

          <div id="map" class="clearfix"></div>
          <button type="submit" class="btn btn-green shadow-sm">Submit</button>
        </form>

      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<style type="text/css">
  /* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
  width:300px;
  height:250px;
}

</style>


<!-- <input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map"></div> -->
<!-- Replace the value of the key parameter with your own API key. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initAutocomplete"
         async defer></script>



<script type="text/javascript">
          // This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}




 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

//image validation
$("#myForm").submit(function(e) {
  var inp = document.getElementById('imageUpload');

  if (inp.files.length == 0) {

      $('#err_img').css('color','red');
      $("#err_img").append("Please uplolad logo");
      e.preventDefault();
  }
    
});

         </script>