<style>
    
#map {
  width:100%;
  height:350px;
}
@media (max-width: 767px) {
  #map {
  width:100%;
  height:200px;

}  
}
    
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
   <!-- Page Heading -->
   <h1 class="h3 mb-2 text-gray-800">Add Branch </h1>
    <?php

       if(isset($breadcrumb)&&  !is_null($breadcrumb)){
       ?> 
       <div class="span10" style="margin-left:5px;">
          
             <ul class="breadcrumb">
                <?php
                   echo $breadcrumb ;             
                ?>     
             </ul>
          
       </div>
       <?php 
        }
    ?>
   <div class="card shadow mb-4">
      <!-- <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
      </div> -->
      <div class="card-body ">
         <form  method="post" id="myForm"  class="m-t-10" enctype="multipart/form-data" action="<?php echo base_url('insert-branch'); ?>"  autocomplete="off" novalidate>
            <div class="form-row">
               <div class="col-lg-6 col-md-12 col-sm-12 left-shift">
                  <div class="form-row">
                     <label class="form-group has-float-label col-md-12">
                        <select id="inputState" class="form-control  fadeIn first custom-select01" name="companytype" required="" placeholder="Company Type">
                           <option value="<?php if (!empty(set_value('companytype'))) { echo set_value('companytype');}else{echo '';} ?>"><?php if (!empty(set_value('companytype'))) { echo set_value('companytype');}else{echo 'Select Type';} ?></option>
                           <option value="Company">Company</option>
                           <option value="Corporation">Corporation</option>
                           <option value="School/Institute">School/Institute</option>
                           <option value="College/University">College/University</option>
                           <option value="title">Others</option>
                        </select>
                        <span>Whats You are ?</span>
                        <div class="valid-feedback"> </div>
                        <div class="invalid-feedback">Please select option. This field is required.</div>
                     </label>
                     <div class="form-group col-md-12">
                        <label class="has-float-label">
                           <input type="text" class="form-control" name="branchname" value="<?php echo set_value('branchname'); ?>" placeholder="-" value="" required>
                           <span class="" >Branch Name</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter branch name. This field is required.</div>
                        </label>
                     </div>
                     <div class="form-group col-md-12">
                        <label class="has-float-label">
                           <input type="text" class="form-control" name="slogan" value="<?php echo set_value('slogan') ; ?>" placeholder="-">
                           <span class="" >Branch Tagline</span>
                           
                           
                        </label>
                     </div>
                     <div class="form-group col-md-6">
                        <label class="has-float-label">
                           <input type="text" class="form-control" name="branchhead" value="<?php echo set_value('branchhead'); ?>" placeholder="-" required>
                           <span class="">Admin Name</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter admin name. This field is required.</div>
                        </label>
                     </div>
                     <div class="form-group col-md-6">
                        <label class="has-float-label">
                           <input type="email" class="form-control" name="email_id" value="<?php echo set_value('email_id'); ?>" placeholder="-" required>
                           <span class="">Admin Email</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                           <?php echo form_error('email_id'); ?>
                           <div style="color: red;"><?php if (isset($email_err)) { echo $email_err; } ?></div>
                           
                        </label>
                     </div>
                     <div class="form-group  col-md-6 m-t-10 ">
                        <label class="form-group has-float-label">
                           <select id="countries_phone1" class="form-control bfh-countries" name="country" data-country="IN">
                           </select>
                           <span>Country</span>
                           <div class="valid-feedback"> </div>
                           <div class="invalid-feedback">Please select option. This field is required.</div>
                        </label>
                     </div>
                     <div class="form-group col-md-6 m-t-10">
                        <label class="has-float-label">
                           <input type="text" class="form-control bfh-phone" data-country="countries_phone1" value="<?php echo set_value('contactno'); ?>" placeholder="Contact Number" name="contactno" required>
                           <span class="" for="">Contact Number</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter a contact number. This field is required.</div>
                           <div style="color: red;"><?php if (isset($contact_err)) { echo $contact_err; } ?></div>
                        </label>
                     </div>
                                <div class="form-group col-md-12">
                        <label class="has-float-label">
                          <input id="address"class="form-control" type="textbox" value="" name="location" required >
                   
                           <span class="" >Branch Location</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter a location. This field is required.</div>
                        </label>
                  <!--  <input id="submit" type="button" class="btn btn-outline-secondary" value="Get Location">-->
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-12 col-sm-12 right-shift">
                   <div class="col-md-12">
                         
                           <div id="map" ></div>
                      
                     </div>
                   
                  
               </div>
            </div>
             <div class="m-b-20">
              <div class="card ">
               
                <div class="card-body">
                    
                  <div class=" mb-1"><strong>Logo</strong></div>
                    <p class="small ">Choose a image to appear on your welcome screen in place of your logo.</p>
                 <!--pic start-->
                 <div class="form-row ">
                 <div class="col-md-3">
             <!--pic start-->
                  <div class="form-group">
                     <div class="avatar-upload">
                        <div class="avatar-edit">
                           <input type='file' id="imageUpload" name="logo">
                           <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                           <div id="imagePreview" style="background-image: url(<?php echo base_url('uploads/logo.jpg'); ?>);">
                           </div>
                        </div>
                     </div>
                     <span id="err_img"></span>
                  </div>
                  <!--pic close--> 
                     </div>
                     <div class="col-md-9 m-t-30 m-b-20">
                         <p>We recommend uploading a transparent PNG cropped to the edges of your logo and up to 600px width or 600px heigh.</p>

                          <strong>  Tip:</strong> To further customize the look of your device, add a welcome image.
                         </div>
                    
                     <!--pic close-->
                
                </div>
                </div>
              </div>

            </div>
            
            <div class="form-row">
          
                       <label class="form-group has-float-label col-md-6 clearfix">
                 <select name="plan" class="form-control" required="">
                    <option value="<?php if (!empty(set_value('plan'))) { echo set_value('plan');}else{echo '';} ?>"><?php if (!empty(set_value('plan'))) { echo set_value('plan');}else{echo 'Select Plan';} ?></option>
                    <option value="15 days">15 Days Trial</option>
                    <option value="3 months">3 Months</option>
                    <option value="6 months">6 Months</option>
                    <option value="12 months">12 Months</option>
                 </select>
                 <span>Select Plan</span>
                        <div class="valid-feedback"> </div>
                        <div class="invalid-feedback">Please select option. This field is required.</div>
              
            </div>
            <div class="form-row m-t-10">
              <div class="form-group col-md-6">
                 <label>Send Type : </label>
                 <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email"  <?php if(set_value('sendtype') =="Email"){echo "checked";}else{ echo "checked";}?> >
                    <label class="custom-control-label" for="customRadioInline1">By Email</label>
                 </div>
                 <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message" <?php if(set_value('sendtype') =="Message"){echo "checked";}?> >
                    <label class="custom-control-label" for="customRadioInline2">By Message</label>
                 </div>
                 <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline3" name="sendtype" class="custom-control-input" value="whatsapp"  <?php if(set_value('sendtype') =="whatsapp"){echo "checked";}?>  >
                    <label class="custom-control-label" for="customRadioInline3">By Whats App</label>
                 </div>
              </div>
      
              <div class="form-group col-md-6">
                 <label>Access Provide : </label>
                 <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full"  <?php if(set_value('access') =="full"){echo "checked";}?> >
                    <label class="custom-control-label" for="customRadioInline4">Full Access</label>
                 </div>
                 <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view" <?php if(set_value('access') =="view"){echo "checked";}else{ echo "checked"; }?> >
                    <label class="custom-control-label" for="customRadioInline5">View Only</label>
                 </div>
              </div>
            </div>
   
               <button type="submit" class="btn btn-green shadow-sm">Submit</button>
            </div>
            </div>
         </form>
      </div>
   <!--</div>
</div>
</div>-->

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Replace the value of the key parameter with your own API key. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap"
         async defer></script>
<script type="text/javascript">
  /*map*/
  function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 28.6139, lng: 77.2090}
  });
  var geocoder = new google.maps.Geocoder();

  document.getElementById('address').addEventListener('change', function() {
    geocodeAddress(geocoder, map);
  });
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === 'OK') {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
  
  /*map*/
   function readURL(input) {
   if (input.files && input.files[0]) {
   var reader = new FileReader();
   reader.onload = function(e) {
     $('#imagePreview').css('background-image', 'url('+e.target.result +')');
     $('#imagePreview').hide();
     $('#imagePreview').fadeIn(650);
   }
   reader.readAsDataURL(input.files[0]);
   }
   }
   $("#imageUpload").change(function() {
   readURL(this);
   });
   
   //image validation
   $("#myForm").submit(function(e) {
   var inp = document.getElementById('imageUpload');
   
   if (inp.files.length == 0) {
   
   $('#err_img').css('color','red');
   $("#err_img").html("Please uplolad logo");
   e.preventDefault();
   }
   
   });
   
   //form validation kavita
   // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
  /* $("#myForm").submit(function(event) {
   
      // make selected form variable
      var vForm = $(this);
      
      if (vForm[0].checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
      } else {
       
        // Replace alert with ajax submit here...
        //alert("your form is valid and ready to send");
        
      }
      
      // Add bootstrap 4 was-validated classes to trigger validation messages
      vForm.addClass('was-validated');
      
   
   });*/
   
   
</script>