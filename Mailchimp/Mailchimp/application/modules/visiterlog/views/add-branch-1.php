<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Branch Form</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div>
    <div class="card-body">
       <form  method="post" id="myForm" enctype="multipart/form-data" action="<?php echo base_url('insert-branch'); ?>"  autocomplete="off" novalidate>
              <div class="form-row">
                <div class="col-lg-9 col-md-12 col-sm-12 left-shift">
                  <div class="form-row">
				     <label class="form-group has-float-label col-md-6">
                          <select id="inputState" class="form-control  fadeIn first custom-select01" name="companytype" required="" placeholder="Company Type">
									<option value="<?php if (!empty(set_value('companytype'))) { echo set_value('companytype');}else{echo '';} ?>"><?php if (!empty(set_value('companytype'))) { echo set_value('companytype');}else{echo 'Select Type';} ?></option>
									<option value="Company Name">Company</option>
									<option value="Corporation Name">Corporation</option>
									<option value="School/Institute Name">School/Institute</option>
									<option value="College/University Name">College/University</option>
									<option value="title">Others</option>
								  </select>
                                    <span>Whats You are ?</span>
                                    <div class="valid-feedback"> </div>
                                    <div class="invalid-feedback">Please select option. This field is required.</div>
                       </label>           
				  
                    <div class="form-group col-md-6"> 
					<span class="has-float-label">
                      <input type="text" class="form-control" name="branchname" value="<?php echo set_value('branchname'); ?>" placeholder="-" value="" required>
                      <label class="" >Branch Name</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                      </span>
					  </div>
					  <div class="form-group col-md-6"> 
					<span class="has-float-label">
                         <input type="text" class="form-control" name="slogan" value="<?php echo set_value('slogan') ; ?>" placeholder="-">
                      <label class="" >Branch Tagline</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                      </span>
					  </div>
                    <div class="form-group col-md-6"> <span class="has-float-label">
                        <input type="text" class="form-control" name="branchhead" value="<?php echo set_value('branchhead'); ?>" placeholder="-" required>
                      <label class="">Admin Name</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                      </span> 
					  </div>
					 
					  <div class="form-group col-md-6"> <span class="has-float-label">
                     <input type="text" class="form-control" name="email_id" value="<?php echo set_value('email_id'); ?>" placeholder="-" >
                      <label class="">Admin Email</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                      <span style="color: red;"><?php if (isset($email_err)) { echo $email_err; } ?></span>
                      </span> 
					  </div>
                    <div class="form-group  col-md-6 m-t-10 ">
                      <label class="form-group has-float-label">
                      <select id="countries_phone1" class="form-control bfh-countries" data-country="IN">
                      </select>
                      <span>Country</span>
                      <div class="valid-feedback"> </div>
                      <div class="invalid-feedback">Please select option. This field is required.</div>
                      </label>
                    </div>
                    <div class="form-group col-md-6 m-t-10"> 
					<span class="has-float-label">
                      <input type="text" class="form-control bfh-phone" data-country="countries_phone1" value="<?php echo set_value('contactno'); ?>" placeholder="Contact Number" name="contactno" required>
                      <label class="" for="">Contact Number</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a contact number. This field is required.</div>
                      <span style="color: red;"><?php if (isset($contact_err)) { echo $contact_err; } ?></span>
                      </span> </div>
                
                   
                  </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 right-shift"> 
                  <!--pic start-->
				    <div class="form-group">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="logo">
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(<?php echo base_url('uploads/logo.jpg'); ?>);">
                        </div>
                    </div>
                </div><span id="err_img"></span></div>
				  
                  <!--pic close--> 
                </div>
              </div>
			  
			  <div class="form-group col-md-6">
                           <select name="plan" class="form-control">
                              <option value="<?php if (!empty(set_value('plan'))) { echo set_value('plan');}else{echo '';} ?>"><?php if (!empty(set_value('plan'))) { echo set_value('plan');}else{echo 'Select Plan';} ?></option>
                              <option value="15 days">15 Days Trial</option>
                              <option value="3 months">3 Months</option>
                              <option value="6 months">6 Months</option>
                              <option value="12 months">12 Months</option>     
                           </select>
                        </div>
            
                        <div class="form-group col-md-6">
                            <label>Send Type : </label>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email"  <?php if(set_value('sendtype') =="Email"){echo "checked";}else{ echo "checked";}?> >
                              <label class="custom-control-label" for="customRadioInline1">By Email</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message" <?php if(set_value('sendtype') =="Message"){echo "checked";}?> >
                              <label class="custom-control-label" for="customRadioInline2">By Message</label>
                            </div> 
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline3" name="sendtype" class="custom-control-input" value="whatsapp"  <?php if(set_value('sendtype') =="whatsapp"){echo "checked";}?>  >
                              <label class="custom-control-label" for="customRadioInline3">By Whats App</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Access Provide : </label>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full"  <?php if(set_value('access') =="full"){echo "checked";}?> >
                              <label class="custom-control-label" for="customRadioInline4">Full Access</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view" <?php if(set_value('access') =="view"){echo "checked";}else{ echo "checked"; }?> >
                              <label class="custom-control-label" for="customRadioInline5">View Only</label>
                            </div>
                        </div>
			  
              <div class="row">
                    <div class="form-group col-md-6"> <span class="has-float-label">
                 
                    <input type="text" id="pac-input" class="form-control" name="location" value="<?php echo set_value('location') ; ?>"  placeholder="Location" required >
            
                      <label class="">Branch Location</label>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                      </span> 
					  </div>
                <div id="map" ></div>
          <button type="submit" class="btn btn-green shadow-sm">Submit</button>
              </div>
            </form>
            </div>
      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<style type="text/css">
  /* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
  width:100%;
  height:350px;
}

</style>


<!-- <input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map"></div> -->
<!-- Replace the value of the key parameter with your own API key. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initAutocomplete"
         async defer></script>



<script type="text/javascript">
          // This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

//image validation
$("#myForm").submit(function(e) {
  var inp = document.getElementById('imageUpload');

  if (inp.files.length == 0) {

      $('#err_img').css('color','red');
      $("#err_img").html("Please uplolad logo");
      e.preventDefault();
  }
    
});

//form validation kavita
         // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
         $("#myForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               alert("You have registered successfully. Please check your email");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });

</script>