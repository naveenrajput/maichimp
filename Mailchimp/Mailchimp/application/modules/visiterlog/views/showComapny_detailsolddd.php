<!--Begin Page Content -->
       <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Company Form</h1>
       <!--    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="#">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">General Information</h6>
            </div>
            <div class="card-body">
               <div class="form1">
                  <div id="successMsg"></div>
                  <form action="<?php echo base_url('updatecompany');?>" method="post" enctype="multipart/form-data" id="myForm">
                    <?php 

                      foreach ($c_detail as $data){
                    ?>
                    <input type="hidden" name="u_id" value="<?php echo $data->id; ?>">
                     <div class="form-row">
                        <div class="form-group col-md-4">
                          <!-- <label for="inputEmail4">Email</label> -->
                           <input type="text" class="form-control" name="companyname" value="<?php echo $data->companyname; ?>" disabled>
                        </div>
                        <div class="form-group col-md-4">
                           <select name="companytype" class="form-control"  >
                              <option value="<?php echo $data->companytype; ?>"><?php echo $data->companytype; ?></option>
                              <option value="Company">Company</option>
                              <option value="Corporation">Corporation</option>
                              <option value="School/Institute">School/Institute</option>
                              <option value="College/University">College/University</option>
                           </select>
                        </div>

                        <!--pic start-->
                        <div class="form-group col-md-4">
                          <div class="avatar-upload">
                              <div class="avatar-edit">
                                  <input type='file' id="imageUpload" name="logo" >
                                  <label for="imageUpload"></label>
                              </div>
                              <div class="avatar-preview">
                                  <div id="imagePreview" style="background-image: url(<?php echo $data->logo; ?>);">
                                  </div>
                              </div>
                          </div>
                        </div>
                        <!--pic close-->
                        
                     </div>
                      <div class="form-group ">
                        <input type="text" class="form-control" name="slogan" value="<?php echo $data->slogan; ?>" placeholder="Slogan" >
                     </div>

                     <div class="form-row">
                       <div class="form-group col-md-6">
                           <!---<label for="inputEmail4">Email</label>-->
                           <input type="text" class="form-control" name="ownername" value="<?php echo $data->ownername; ?>" required="" placeholder="Owner Name">
                        </div>
                     </div>
                     <div class="form-row">
                       <div class="form-group col-md-6">
                           <input type="text" class="form-control" name="contactno" value="<?php echo $data->contactno; ?>" required="" placeholder="Contact Number">
                        </div>
                        <div class="form-group col-md-6">
                           <input type="text" class="form-control" name="alternateno" value="<?php echo $data->alternateno; ?>" placeholder="Alternate Number(Optional)">
                        </div>
                     </div>

                     <div class="form-row">
                       <div class="form-group col-md-6">
                           <!---<label for="inputEmail4">Email</label>-->
                           <input type="email" class="form-control" name="email" value="<?php echo $data->email; ?>" disabled>
                        </div>
                       
                        <div class="form-group col-md-6">
                           <input type="password" class="form-control" name="password" value="<?php echo $data->password; ?>" required="" placeholder="Password">
                        </div>
                     </div>
                     
                     <div class="form-group">
                        <input type="text" class="form-control" name="address" value="<?php echo $data->address; ?>" required="" placeholder="Address">
                     </div>
 
                    
                      <label for="sendType">Send Type : </label>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email" <?php if($data->sendtype =="Email"){echo "checked";}?>>
                        <label class="custom-control-label" for="customRadioInline1">By Email</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message" <?php if($data->sendtype =="Message"){echo "checked";}?> >
                        <label class="custom-control-label" for="customRadioInline2">By Message</label>
                      </div> <br>
                      

                     <button type="submit" id="smt" class="btn btn-green shadow-sm">Update</button>
                   <?php }?>
                  </form>
               </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content-->

<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
</script>