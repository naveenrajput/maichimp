<!-- Begin Page Content -->
<div class="container-fluid">
   <?php
   
   if(isset($breadcrumb)&&  !is_null($breadcrumb)){
   ?> 
   <div class="span10" style="margin-left:5px;">
      
         <ul class="breadcrumb">
            <?php
               echo $breadcrumb ;             
            ?>     
         </ul>
      
   </div>
   <?php 
      }
      ?>
   <!-- Page Heading -->
   <h1 class="h3 mb-2 text-gray-800">Add Details</h1>
   <!--    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="#">official DataTables documentation</a>.</p> -->
   <!-- DataTales Example -->
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">General Information</h6>
      </div>
      <div class="card-body">
         <div class="form1">
            <form action="<?php echo base_url('insertCompany');?>" method="post" enctype="multipart/form-data" id="myForm" >
               <?php 
               if(isset($c_detail)&&  !is_null($c_detail)){
                  foreach ($c_detail as $data){
                    
                  ?>
               <input type="hidden" name="u_id" value="<?php echo $data->id; ?>">
               <div class="form-row">
                  <div class="col-md-8">
                     <div class="form-row">
                        <!-- <div class="form-group col-md-6">
                           <select name="companytype" class="form-control"  >
                              <option value="<?php //echo $data->companytype; ?>"><?php //echo $data->companytype; ?></option>
                              <option value="Company">Company</option>
                              <option value="Corporation">Corporation</option>
                              <option value="School/Institute">School/Institute</option>
                              <option value="College/University">College/University</option>
                           </select>
                        </div> -->
                     
                        
                        <div class="form-group col-md-6">
                                <label class="has-float-label">
                                   
                           <input type="text" class="form-control" name="companyname" placeholder="Company Name" value="<?php echo $data->companyname; ?>" disabled>
                                <span class="" for="">Company </span>
                                 <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                                      </label>
                        </div>
                        <div class="form-group col-md-12">
                             <label class="has-float-label">
                           <input type="text" class="form-control" name="slogan" placeholder="Company Tag" value="<?php if(!empty($data->slogan)){ echo $data->slogan; }else{ echo set_value('slogan'); }  ?>" >
                             <span class="" for="">Tagline </span>
                                 <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                        </label>
                        </div>
                        <div class="form-group col-md-6">
                             <label class="has-float-label">
                           <!---<label for="inputEmail4">Email</label>-->
                           <input type="text" class="form-control" name="ownername" placeholder="Owner Name" value="<?php echo $data->ownername; ?>" required>
                             <span class="" for="">Owner Name </span>
                                 <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                                      </label>
                        </div>
                        <div class="form-group col-md-6">
                           <!---<label for="inputEmail4">Email</label>-->
                           <input type="email" class="form-control" name="email" placeholder="Eamil ID" value="<?php echo $data->email; ?>" disabled >
                        </div>
                        <div class="form-group col-md-6">
                           <input type="text" class="form-control" name="contactNo" placeholder="Contact No." value="<?php echo $data->contactno; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                           <input type="text" class="form-control" name="alternateNo" placeholder="Alternate Number(Optional)" value="<?php echo $data->alternateno; ?>" >
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <!--pic start-->
                     <div class="form-group col-md-12">
                        <div class="avatar-upload">
                           <div class="avatar-edit">
                              <input type='file' id="imageUpload" name="logo"  />
                              <label for="imageUpload"></label>
                           </div>
                           <div class="avatar-preview">
                              <div id="imagePreview" style="background-image: url(<?php if (!empty($data->logo)) {
                                 echo $data->logo;
                                 }else{ ?><?php echo base_url('uploads/logo.jpg'); ?><?php } ?>  );">
                              </div>
                           </div>
                        </div>
                        <span id="err_img"></span>
                     </div>
                     <!--pic close-->
                  </div>
               </div>
               <!--
                  <div class="form-row">
                  
                     <div class="form-group col-md-6">
                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>" >
                        <?php echo form_error('password');?>
                     </div>
                  </div>-->
               <div class="form-group ">
                  <textarea class="form-control" rows="2" name="address" placeholder="Address"  required><?php if(!empty($data->address)){ echo $data->address; }else{ echo set_value('address'); }  ?></textarea>
                  <!--  <input type="text" class="form-control" name="address" placeholder="Address" value="<?php //echo set_value('address'); ?>" >-->
               </div>
               <div class="form-row">
                  <div class="form-group col-md-4">
                     <label for="sendType">Plan  </label>
                     <select name="plan" class="form-control" value="<?php echo set_value('plan'); ?>" >
                        <option value="15 days">15 Days Trial</option>
                        <option value="3 months">3 Months</option>
                        <option value="6 months">6 Months</option>
                        <option value="12 months">12 Months</option>
                     </select>
                  </div>
               </div>
               <div class="form-row clearfix">
                  <div class="form-group col-md-12">
                     <label for="sendType">Send Type : </label>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email" <?php if($data->sendtype =="Email"){echo "checked";}?> >
                        <label class="custom-control-label" for="customRadioInline1">By Email</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message" <?php if($data->sendtype =="Message"){echo "checked";}?> >
                        <label class="custom-control-label" for="customRadioInline2">By Message</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="whatsapp" <?php if($data->sendtype =="whatsapp"){echo "checked";}?> >
                        <label class="custom-control-label" for="customRadioInline1">By Whats App</label>
                     </div>
                  </div>
               </div>
               <!-- <br>* PLAN :  ₹ 199.00/mo when you renew<br> --> 
               <button type="submit" id="smt" class="btn btn-green shadow-sm" >submit</button>
               <?php }  }?>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script type="text/javascript">
 // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
         $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               //alert("your form is valid and ready to send");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });



   // $(document).ready(function() {
        
   //      var form=$("#myForm");
   //      $("#smt").click(function(){
   //        var imageUpload = 
   
   //        $.ajax({
   //                type:"POST",
   //                url:'insertCompany',
   //                data:form.serialize(),
   //                success: function(response){
                    
   //                    location.reload("companyList"); 
                  
   //                }
   //            });
   
   //      });
   
   //  });
   
   // profile-pic
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
              $('#imagePreview').css('background-image', 'url('+e.target.result +')');
              $('#imagePreview').hide();
              $('#imagePreview').fadeIn(650);
          }
          reader.readAsDataURL(input.files[0]);
      }
   }
   $("#imageUpload").change(function() {
      readURL(this);
   });
   
   $("#myForm").submit(function(e) {
    var inp = document.getElementById('imageUpload');
   
   
    if (inp.files.length == 0) {
        $('#err_img').css('color','red');
        $("#err_img").html("Please uplolad logo");
        // setTimeout(function() {
        //   $("#err_img").hide();
        // }, 1000);
        
        // $('#err_img').css('color','red');
        // $("#err_img").html("Please uplolad logo");
        e.preventDefault();
    }
      
   });
   
   //image validation
   // function validate(){
   //     var inp = document.getElementById('imageUpload').val();
   
   // alert(inp);
   //     if(inp.files.length == 0){
   //         // alert("Attachment Required");
   //         $('#err_img').css('color','red');
   //         $("#err_img").append("Please uplolad logo");
   //    inp.focus();
   //     return false;
   //     }
   // }
   
   // $(document).ready(function (e){
   // $("#smt").on('submit',(function(e){
   // e.preventDefault();
   // // $('#targetLayer').html('Please Wait <i class="fa fa-circle-o-notch fa-spin" style="font-size: 14px;"></i>');
   // $.ajax({
   // url: "insertCompany",
   // type: "POST",
   // data:  new FormData(this),
   // contentType: false,
   // cache: false,
   // processData:false,
   // success: function(data){
   // //$("#view").html(data);
   //  location.reload("companyList"); 
   // },
   // error: function(){}
   // });
   
   // }));
   // });
   
      // $("#smt").click('button',(function(e) {
      //     e.preventDefault();
      //     $.ajax({
      //         url: "<?php //echo base_url('insertCompany'); ?>",
      //         type: "POST",
      //         data:  new FormData(this),
      //         mimeType:"multipart/form-data",
      //         contentType: false,
      //         cache: false,
      //         processData:false,
      //         success: function(data)
      //         {
      //           alert(data);
      //         //$("#targetLayer").html(data);
      //         },
      //         error: function() 
      //         {
      //         }           
      //    });
      // }));
   
   
</script>