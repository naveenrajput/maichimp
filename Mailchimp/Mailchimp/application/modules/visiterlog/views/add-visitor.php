<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Visitor Form</h1>
  <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="#">official DataTables documentation</a>.</p> -->

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div>
    <div class="card-body">
      <div class="form">
        <form  method="post" action="<?php echo base_url('insert-visitor'); ?>" data-toggle="validator" role="form">
          <div class="form-row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="name" placeholder="Name" required>
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="contactno" placeholder="Mobile" required>
            </div>
            <!--pic start-->
              <div class="form-group col-md-4">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="profile_pic" />
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                        </div>
                    </div>
                </div>
              </div>
            <!--pic close-->
            <div class="form-group col-md-6">
              <!---<label for="inputEmail4">Email</label>-->
              <input type="text" class="form-control" name="alternateno" placeholder="Alternate Mobile">
            </div>
            <div class="form-group col-md-6">
              <!---<label for="inputEmail4">Email</label>-->
              <input type="email" class="form-control" name="email" placeholder="Email" required>
            </div>
            <div class="form-group col-md-6">
              <textarea class="form-control" name="purpose" placeholder="Purpose of meeting" required></textarea>
            </div>
            <div class="form-group col-md-6">
              <textarea class="form-control" name="remark" placeholder="Remark" required></textarea>
            </div>
            </div>
          <button type="submit" class="btn btn-green shadow-sm">Submit</button>
         <!--  <a href="#" class=" btn btn-green shadow-sm"> Submit</a> -->
        </form>
      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->

<script type="text/javascript">
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
</script>