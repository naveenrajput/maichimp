
<!----------------->
<div class="container-fluid invite">

   <!-- Page Heading -->
 <div class="row">
    <div class="col-md-8">
	<h1 class="h3 mb-2 text-gray-800">Invite</h1>
   <?php
   
   if(isset($breadcrumb)&&  !is_null($breadcrumb)){
   ?> 
   <div class="span10" >
      
         <ul class="breadcrumb">
            <?php
               echo $breadcrumb ;             
            ?>     
         </ul>
      
   </div>
   <?php 
      }
      ?>
    </div>

  </div>
   <ul id="tabs" class="nav nav-tabs" role="tablist">
      <li class="nav-item">
         <a id="tab-A" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">New Invite</a>
      </li>
      <li class="nav-item">
         <a id="tab-B" href="#pane-B" class="nav-link" data-toggle="tab" role="tab">Group Invite</a>
      </li>
      <li class="nav-item">
         <a id="tab-C" href="#pane-C" class="nav-link" data-toggle="tab" role="tab">Multiple Invite</a>
      </li>
   </ul>
   <div id="content" class="tab-content" role="tablist">
      <div id="pane-A" class="card tab-pane fade show active" role="tabpanel" aria-labelledby="tab-A">
         <div class="card shadow mb-4 ">
            <!-- qq---->
            <div class="card-body m-t-20">
                    <h5>Personal Detail</h5>
               <div class="form">
                  <form  method="post" class="bootstrapForm" enctype="multipart/form-data" action="<?php echo base_url('insert-invites'); ?>" data-toggle="validator" role="form" novalidate>
                     <div class="form-row">
                  
                        
                     <!--   <div class="form-group col-md-4">
                          <label >Organization Name</label>
                           <select name="branch" class="form-control" placeholder="Branch" required>
                              <?php foreach ($company as $com) { ?>
                              <option value="<?php //echo $com->companyname; ?>"><?php //echo $com->companyname; ?></option>
                              <?php } ?>
                              <?php foreach ($branch as $bran) { ?>
                              <option value="<?php//echo $bran->branchname; ?>"><?php //echo $bran->branchname; ?></option>
                              <?php } ?>
                           </select>
                        </div>-->
                       
                        
                        <div class="form-group col-md-4">
                             <span class="has-float-label">
                           
                           <input type="text" name="visitor_name" class="form-control" placeholder="-" required>
                           <label >Full Name</label>
                         <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter name. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-4">
                               <span class="has-float-label">
                         
                           <input type="email" name="email" class="form-control" placeholder="-" required>
                             <label >Email</label>
                            <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-4">
                              <span class="has-float-label">
                         
                           <input type="text" name="contactnum" class="form-control" placeholder="-" required>
                             <label >Contact No.</label>
                                  <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter contact no. This field is required.</div>
                        </div>
                        
                        <div class="form-group col-md-4">
                             <span class="has-float-label">
                                  <input type="date" name="invitedate" class="form-control" placeholder="DD-MM-YYYY" required>
                                   <label >Arrival Date</label>
                             <div class="valid-feedback"></div>
                             <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-4">
                             <span class="has-float-label">
                           <input type="time" name="invitetime" class="form-control" placeholder="Arrival Time" required>
                            <label >Arrival Time</label>
                              <div class="valid-feedback"></div>
                             <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>
                  
                        
                        <div class="form-group col-md-6">
                               <span class="has-float-label">
                           <input type="text" name="title" class="form-control" placeholder="Title of meeting" required>
                               <label >Title</label>
                               <div class="valid-feedback"></div>
                             <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-6">
                            
                            <span class="has-float-label">
                               <select name="host" class="form-control" placeholder="Host" required>
                                  <option value="" selected>Select Host</option>
                                  <?php foreach ($emp as $employee) { ?>
                                  <option value="<?php echo $employee->empname; ?>"><?php echo $employee->empname; ?></option>
                                  <?php } ?>
                               </select>
                               <label >Host Name</label>
                               <div class="valid-feedback"></div>
                               <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-6">
                            <span class="has-float-label">
                               <textarea type="text" name="msg" class="form-control" placeholder="Hello Sir" required></textarea>
                               <label >Message</label>
                               <div class="valid-feedback"></div>
                               <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>
                        <div class="form-group col-md-6">
                            <span class="has-float-label">
                               <textarea type="text" name="summary" class="form-control" placeholder="Set meeting agenda" required></textarea>
                               <label >Summary</label>
                               <div class="valid-feedback"></div>
                               <div class="invalid-feedback">Please enter date. This field is required.</div>
                           </span>
                        </div>  
                     </div>
                     
                     <button type="submit" id="smt" class="btn btn-green shadow-sm m-t-10 m-b-10">Create Invite</button>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div id="pane-B" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-B">
         <div class="card shadow mb-4 ">
            <!-- qq---->
            <div class="card-body m-t-20">
               <form  method="post" enctype="multipart/form-data" action="<?php echo base_url('insert-grpinvites'); ?>" data-toggle="validator" role="form">
                  <div class="form-row">
                    
                     <div class="form-group col-md-4">
                        <span class="has-float-label">
                            <select name="host" class="form-control" placeholder="Host" required>
                              <option value="" selected>Select Host</option>
                              <?php foreach ($emp as $employee) { ?>
                              <option value="<?php echo $employee->empname; ?>"><?php echo $employee->empname; ?></option>
                              <?php } ?>
                            </select>
                            <label >Host Name</label>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                     </div>
                     <div class="form-group col-md-4">
                        <span class="has-float-label">
                            <input type="date" name="invitedate" class="form-control" placeholder="Arrival Date" required>
                            <label >Arrival Date</label>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                     </div>
                     
                     <div class="form-group col-md-4">
                        <span class="has-float-label">
                            <input type="time" name="invitetime" class="form-control" placeholder="Arrival Time" required>
                            <label >Arrival Time</label>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                     </div>
                      
                     <div class="form-group col-md-6">
                        <span class="has-float-label">
                            <input type="text" name="title" class="form-control" placeholder="Title of meeting" required>
                            <label >Title</label>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                     </div>
                         
                  </div>
                     <div class="form-row">
                        <div class="form-group col-md-6">
                         <span class="has-float-label">
                             <textarea type="text" name="msg" class="form-control" placeholder="Hello Sir" required></textarea>
                             <label >Message</label>
                             
                      </div>
                      </div>
                         <div class="form-row">
                      <div class="form-group col-md-6">
                            <span class="has-float-label">
                       
                         <textarea type="text" name="summary" class="form-control" placeholder="Set meeting agenda" required></textarea>
                           <label >Summary</label>
                           <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                      </div>
                         
                     </div>
                    
                      
                 
                  <div id="test-body" class="form-row m-t-10 m-b-10" >
                  
                    
                              <div class="form-group col-md-3">
                            <span class="has-float-label">
                        <input type="text" name="visitor_name[]" class="form-control" placeholder="-" required>
                         <label >Full Name</label>
                           <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span>
                        </div>
                   
                     
                              <div class="form-group col-md-3 ">
                             <span class="has-float-label">
                           <input type="text" name="contactnum[]" class="form-control" placeholder="-" required>
                           <label >Contact</label>
                           <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span></div>
                    
                    
                              <div class="form-group col-md-3">
                             <span class="has-float-label">
                           <input type="email" name="email[]" class="form-control" placeholder="-" required>
                           <label >Email</label>
                           <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter date. This field is required.</div>
                        </span></div>
                 
                        <div class="col-md-3">
                         
                           <button  class='delete-row btn btn-green' type='button' >Delete</button> 
                        
                        </div>
                        
                  </div>
                  <button type="submit" class="btn btn-green shadow-sm">Create Invite</button>
                  <input id='add-row' class='btn btn-green' type='button' value='Add' />   
           
  
                  
                  
               </form>
               
               
               
            </div>
         </div>
      </div>
      <div id="pane-C" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
         <div class="card shadow mb-4 ">
            <!-- qq---->
            <div class="card-body m-t-20">
               <form  method="post" enctype="multipart/form-data" action="<?php echo base_url('multiples'); ?>" data-toggle="validator" role="form">
                  <div class="form-row">
                    <!-- <div class="form-group col-md-6">
                      <label >Organization Name</label>
                        <select name="branch" class="form-control" placeholder="Branch" required>
                           <?php foreach ($company as $com) { ?>
                           <option value="<?php //echo $com->companyname; ?>"><?php //echo $com->companyname; ?></option>
                           <?php } ?>
                           <?php foreach ($branch as $bran) { ?>
                           <option value="<?php //echo $bran->branchname; ?>"><?php //echo $bran->branchname; ?></option>
                           <?php } ?>
                        </select>
                     </div>-->
                    
                     <div class="col-md-6">
                           <label class="form-group has-float-label">
                       
                        <select name="host" class="form-control" placeholder="Host" required>
                          <option value="" selected>Select Host</option>
                          <?php foreach ($emp as $employee) { ?>
                          <option value="<?php echo $employee->empname; ?>"><?php echo $employee->empname; ?></option>
                          <?php } ?>
                        </select>
                           <span>Host Name</span>
                                    <div class="valid-feedback"> </div>
                                    <div class="invalid-feedback">Please select option. This field is required.</div>
                          </label>
                     </div>
                     
                  </div>
                  <div class="table-responsive">
                     <table class="table table-bordered" id="" width="100%" cellspacing="0">
                        <thead>
                           <tr>
                              <!--  <th>S.No</th> -->
                              <th>Title</th>
                              <th>Name</th>
                              <th>Contact</th>
                              <th>Email</th>
                              <th>Arrival Date</th>
                              <th>Arrival Time</th>
                              <th>Message</th>
                              <th>Summary</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody id="test-bodyC">
                           <!-- <?php //$i=1; { ?> -->
                           <tr>
                              <td><input type="text" name="title[]" class="form-control" placeholder="Title of meeting" required></td>
                              <td><input type="text" name="visitor_name[]" class="form-control" placeholder="Full Name" required></td>
                              <td><input type="text" name="contactnum[]" class="form-control" placeholder="Mobile" required></td>
                              <td><input type="email" name="email[]" class="form-control" placeholder="Email" required></td>
                              <td><input type="date" name="invitedate[]" class="form-control" placeholder="Arrival Date" required></td>
                              <td><input type="time" name="invitetime[]" class="form-control" placeholder="Arrival Time" required></td> 
                              <td><input type="text" name="msg[]" class="form-control" placeholder="Hello sir" required></td>
                              <td><input type="text" name="summary[]" class="form-control" placeholder="Set meeting agenda" required></td>
                              <td>
                                 <button  class='delete-row btn btn-green' type='button' >Delete</button>      
                              </td>
                           </tr>
                           <!-- <?php //$i++; } ?> -->
                        </tbody>
                     </table>
                  </div>
                  <button type="submit" class="btn btn-green shadow-sm">Create Invite</button>
                  <input id='add-rowC' class='btn btn-green' type='button' value='Add' />           
               </form>
            </div>
         </div>
      </div>
      <!-- DataTales Example -->
   </div>
   <!-- End of Main Content -->
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">All Invites</h6>
      </div>
      <div class="card-body">
         <div class="table-responsive">
            <table id="example" class="table table-hover responsive nowrap" style="width:100%">
               <thead>
                  <tr>
                     <th>Title</th>
                     <th>Host Name</th>
                     <th>Visitor Name</th>
                     <th>Email</th>
                     <th>Arrival Date</th>
                     <th>Arrival Time</th>
                     <th>Message</th>
                     <th>Summary</th>
                     <th>Contact No.</th>
                     <!-- <th>Action</th> -->
                  </tr>
               </thead>
               <tbody>
                  <?php if (isset($invites)) {
                     $i=1; foreach ($invites as $invite) {
                     ?>
                  <tr>
                     <td>
                        <a href="#">
                           <div class="d-flex align-items-center">
                              <div class="avatar avatar-blue mr-3"><span><?php echo $invite->visitortype; ?></span></div> 
                              <div class="">
                                 <p class="font-weight-bold mb-0"><?php echo $invite->title; ?></p>
                                 <!-- <p class="text-muted mb-0"><?php //echo $invite->email; ?></p> -->
                              </div>
                           </div>
                        </a>
                     </td>
                     <td><?php echo $invite->host; ?></td>
                     <td><?php echo $invite->visitor_name; ?></td>
                     <td><?php echo $invite->email; ?></td>
                     <td><?php echo $invite->invitedate; ?></td>
                     <td><?php echo $invite->invitetime; ?></td>
                     <td><?php echo $invite->msg; ?></td>
                     <td><?php echo $invite->summary; ?></td>
                     <td><?php echo $invite->contactnum; ?></td>
                    
                  </tr>
                  <?php }}?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<!-- nav nav-tabs -->
</div><!-- /.container-fluid -->
<script type="text/javascript" herf="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" herf="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" herf="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" herf="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>


<!--datepicker-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>


<script type="text/javascript">
   // Add row
   
   //end row
    // // Add row in group invite
   var row=1;
   $(document).on("click", "#add-row", function () {
   var new_row = '<tr id="row' + row + '"><td><input name="visitor_name[]" type="text" class="form-control" placeholder="Full name" required/><td><input name="contactnum[]" type="text" class="form-control" placeholder="Mobile" required/></td><td><input name="email[]" type="email" class="form-control" placeholder="Email" required/></td><td><input class="delete-row btn btn-green" type="button" value="Delete" /></td></tr>';
  
   $('#test-body').append(new_row);
   row++;
   return false;
   });
   
   
   $(document).on("click", ".delete-row", function () {
     if(row>1) {
       $(this).closest('tr').remove();
       row--;
     }
   return false;
   });
   //end row


    // // Add row multiple invite
   var rowC=1;
   $(document).on("click", "#add-rowC", function () {
   var new_rowC = '<tr id="rowC' + rowC + '"><td><input type="text" name="title[]" class="form-control" placeholder="Title of meeting" required></td><td><input name="visitor_name[]" type="text" class="form-control" placeholder="Full name" required/><td><input name="contactnum[]" type="text" class="form-control" placeholder="Mobile" required/></td><td><input name="email[]" type="email" class="form-control" placeholder="Email" required/></td><td><input name="invitedate[]" type="date" class="form-control" required/></td><td><input name="invitetime[]" type="time" class="form-control" required/></td><td><input name="msg[]" type="text" class="form-control" placeholder="Hello sir" required/></td><td><input type="text" name="summary[]" class="form-control" placeholder="Set meeting agenda" required></td><td><input class="delete-row btn btn-green" type="button" value="Delete" /></td></tr>';
   
   $('#test-bodyC').append(new_rowC);
   rowC++;
   return false;
   });
   
   
   $(document).on("click", ".delete-row", function () {
     if(rowC>1) {
       $(this).closest('tr').remove();
       rowC--;
     }
   return false;
   });
   //end row
  


   
   $(document).ready(function() {
   $("#example").DataTable({
     aaSorting: [],
     responsive: true,
   
     columnDefs: [
       {
         responsivePriority: 1,
         targets: 0
       },
       {
         responsivePriority: 2,
         targets: -1
       }
     ]
   });
   
   $(".dataTables_filter input")
     .attr("placeholder", "Search here...")
     .css({
       width: "300px",
       display: "inline-block"
     });
   
   $('[data-toggle="tooltip"]').tooltip();
   });
   
   //form validation kavita
         // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
         $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               alert("You have registered successfully. Please check your email");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });
</script>


