<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
	
  <title>Greetlog - Dashboard</title>
	
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
 <!-- Custom styles for this page -->
  <link href="<?php echo base_url() ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<style>



</style>
</head>

<body class="bg-gradient-primary">

  <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first p-t-30 p-b-30">
      <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="180" class="" alt="Greetlog" />
    </div>

    <!-- Login Form -->
    <form>
      <input type="text" class="fadeIn second " name="passowrd" placeholder="Password">
      <input type="text" class="fadeIn third " name="confirm_password" placeholder="Confirm Password">
      <button  class="fadeIn btn  btn-green shadow-sm m-t-20 m-b-20" >Create</button>
    </form>


  </div>
</div>

 
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>

  <script src="<?php echo base_url() ?>assets/js/custom.js"></script>

</body>

</html>