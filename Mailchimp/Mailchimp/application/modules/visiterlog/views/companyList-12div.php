<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
  
        <!-- Begin Page Content -->
       <div class="container-fluid">

        <?php
   
         if(isset($breadcrumb)&&  !is_null($breadcrumb)){
         ?> 
         <div class="span10" style="margin-left:5px;">
            
               <ul class="breadcrumb">
                  <?php
                     echo $breadcrumb ;             
                  ?>     
               </ul>
            
         </div>
         <?php 
            }
            ?>

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Company List</h1>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="#">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Company List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                   <table id="example" class="table table-hover responsive nowrap" style="width:100%">
        <thead>
          <tr>
            <th>Company Name</th>
            <th>Company Type</th>
            <th>Company Tag</th>
            <th>Admin Name</th>
            <th>Contact No.</th>
            <th>Alternate No.</th>
            <th>Address</th>
            <th>Plan</th>
            <th>Expiry date</th>
            <th>Actions</th>
            
          </tr>
        </thead>
        <tbody>
          <?php
              $s_no = 0;
              if (isset($info)) {
              foreach ($info->result() as $detail){
                $s_no++;
                $id = $detail->id;
            ?>
          <tr>
            <td>
              <a href="#">
                <div class="d-flex align-items-center">
                  <div class="avatar avatar-blue mr-3"><img src="<?php echo $detail->logo; ?>" class="avatar-img"></div>

                  <div class="">
                    <p class="font-weight-bold mb-0"><?php echo $detail->companyname; ?></p>
                    <p class="text-muted mb-0"><?php echo $detail->email; ?></p>
                  </div>
                </div>
              </a>
            </td>
            <td><?php echo $detail->companytype; ?></td>
            <td><?php echo $detail->slogan; ?></td>
            <td><?php echo $detail->ownername; ?></td>
            <td><?php echo $detail->contactno; ?></td>
            <td><?php echo $detail->alternateno; ?></td>
            <td><?php echo $detail->address; ?></td>
           
            <td>
              <div class="badge badge-success badge-success-alt"><?php echo $detail->plan; ?></div>
            </td>
            <td><?php echo date("Y-m-d",$detail->expirydate); ?></td>
            <td>
              <div class="dropdown">
                <button class="btn btn-sm btn-icon" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-h" data-toggle="tooltip" data-placement="top"
                        title="Actions"></i>
                    </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                   <a class="dropdown-item" href="#"><i class="far fa-eye mr-2"></i> Btn</a>
                  <a class="dropdown-item" href="<?php echo base_url() ?>showComapny_details/<?php echo $id ?>"><i class="fas fa-pencil-alt mr-2"></i> Edit Profile</a>
                  <a class="dropdown-item text-danger" href="#"><i class="far fa-trash-alt mr-2"></i> Remove</a>
                </div>
              </div>
            </td>
          </tr>

         <?php }}?>
        </tbody>
      </table>



               
              </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

<script>
$(document).ready(function() {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,

    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });

  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});

</script>

