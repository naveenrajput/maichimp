<!--Begin Page Content -->
<div class="container-fluid">
   
   <!-- Page Heading -->
   <h1 class="h3 mb-2 text-gray-800">Edit Company Details</h1>
   <?php
      if(isset($breadcrumb)&&  !is_null($breadcrumb)){
      ?> 
   <div class="span10" style="margin-left:5px;">
      <ul class="breadcrumb">
         <?php
            echo $breadcrumb ;             
            ?>     
      </ul>
   </div>
   <?php 
      }
      ?>
   <div class="card shadow mb-4">
      <div class="card-header py-3">
         <h6 class="m-0 font-weight-bold text-primary">General Information</h6>
      </div>
      <div class="card-body">
         <div class="form1">
            <div id="successMsg"></div>
            <form action="<?php echo base_url('updatecompany');?>" method="post" enctype="multipart/form-data" id="bootstrapForm" novalidate="">
               <?php 
                  foreach ($c_detail as $data){
                  ?>
               <input type="hidden" name="u_id" value="<?php echo $data->id; ?>">
               <div class="form-row">
                  <div class="col-md-8">
                     <div class="form-row">
                        <!-- <div class="form-group col-md-4">
                         <select name="companytype" class="form-control"  >
                            <option value="<?php //echo $data->companytype; ?>"><?php //echo $data->companytype; ?></option>
                            <option value="Company">Company</option>
                            <option value="Corporation">Corporation</option>
                            <option value="School/Institute">School/Institute</option>
                            <option value="College/University">College/University</option>
                         </select>
                         </div> -->
                        <div class="form-group col-md-6">
                          <label class="has-float-label">
                            <input type="text" class="form-control" name="companyname" value="<?php echo $data->companyname; ?>" disabled>
                            <span class="" for="companyname">Organization Name</span>
                          </label>
                        </div>
                        <div class="form-group col-md-6">
                          <label class="has-float-label">
                            <input type="text" class="form-control" name="ownername" value="<?php echo $data->ownername; ?>" required="" placeholder="-">
                            <span class="" for="adminname">Admin Name</span>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter admin name. This field is required.</div>
                          </label>
                        </div>
                        <div class="form-group col-md-12">
                          <label class="has-float-label">
                            <input type="text" class="form-control" name="slogan" value="<?php echo $data->slogan; ?>" placeholder="-" >
                            <span class="" for="companytag">Company Tag</span>
                          </label>
                        </div>
                        
                        <div class="form-group col-md-6">
                          <label class="has-float-label">
                            <input type="email" class="form-control" name="email" value="<?php echo $data->email; ?>" disabled>
                            <span for="inputEmail4">Admin Email</span>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter admin email. This field is required.</div>
                          </label>
                        </div>
                        <div class="form-group col-md-6 contact_fld">
                           <label class="form-group has-float-label">
                              <select  id="countries_phone1" class="form-control bfh-countries" data-country="IN"></select>
                              <span>Country</span>
                              <div class="valid-feedback"> </div>
                              <div class="invalid-feedback">Please select option. This field is required.</div>
                            </label>
                        </div>
                        <div class="form-group col-md-6">
                          <label class="has-float-label">
                            <input type="text" class="form-control" name="contactno" value="<?php echo $data->contactno; ?>" required="" placeholder="-">
                            <span class="" for="contactno">Contact Number</span>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter contact number. This field is required.</div>
                          </label>
                        </div>
                        <div class="form-group col-md-12">
                          <label class="has-float-label">
                            <textarea class="form-control" rows="2" name="address" placeholder="-"  required><?php echo $data->address; ?></textarea>
                            <span class="" for="address">Address</span>
                            <div class="valid-feedback"></div>
                            <div class="invalid-feedback">Please enter address. This field is required.</div>
                          </label>
                        </div>
                        <!-- <div class="form-group col-md-6">
                           <input type="text" class="form-control" name="alternateNo" placeholder="Alternate Number(Optional)" value="<?php //echo $data->alternateno; ?>" >
                        </div> -->
                     </div>
                  </div>
                  <div class="col-md-4">
                     <!--pic start-->
                     <div class="form-group col-md-12">
                        <div class="avatar-upload">
                          <div class="avatar-edit">
                             <input type='file' id="imageUpload" name="logo" >
                             <span for="imageUpload"></span>
                          </div>
                          <div class="avatar-preview">
                             <div id="imagePreview"style="background-image: url(<?php if (!empty($data->logo)) {
                                 echo $data->logo;
                                 }else{ ?><?php echo base_url('uploads/logo.jpg'); ?><?php } ?>  );">
                             </div>
                          </div>
                       </div>
                        <span id="err_img"></span>
                     </div>
                     <!--pic close-->
                  </div>
               </div>
               <!--
                  <div class="form-row">
                  
                     <div class="form-group col-md-6">
                        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php //echo set_value('password'); ?>" >
                        <?php //echo form_error('password');?>
                     </div>
                  </div>-->
               <!-- <div class="form-group ">
                  <textarea class="form-control" rows="2" name="address" placeholder="Address"  required><?php //echo $data->address; ?></textarea>
                  
               </div> -->
               
               <br>
               <button type="submit" id="smt" class="btn btn-green shadow-sm">Update</button>
               <?php }?>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content-->
<script type="text/javascript">
   function readURL(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function(e) {
               $('#imagePreview').css('background-image', 'url('+e.target.result +')');
               $('#imagePreview').hide();
               $('#imagePreview').fadeIn(650);
           }
           reader.readAsDataURL(input.files[0]);
       }
   }
   $("#imageUpload").change(function() {
       readURL(this);
   });

   $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               //alert("your form is valid and ready to send");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });
</script>