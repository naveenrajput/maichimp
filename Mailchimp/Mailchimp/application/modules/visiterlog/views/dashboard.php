     <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet">-->
    
    
        <!-- Begin Page Content -->
        <div class="container-fluid">
          
        
          

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-green shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
            <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; ">
                <i class="fa fa-calendar"></i>&nbsp;
                <span></span> <i class="fa fa-caret-down"></i>
    		    </div>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Check-in (Total Visits)</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php if(isset($data['total_visit'])){echo $data['total_visit']; } ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Repeat Visits</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php if(isset($data['repeat_visit'])){echo $data['repeat_visit']; }?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Today Visits</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php if(isset($data['today_visit'])){echo $data['today_visit']; }?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

         
          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-8">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Visitors Overview</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-4">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Today's Visit</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                    
                      <table id="example" class="table table-hover responsive" style="width:100%">
                         <thead>
                            <tr>
                              <th>Visitor</th>
                              <th>Email</th>
                              <th>Contact No</th>
                              <th>Meeting With</th>
                              <th>Purpose</th>
                            </tr>
                         </thead>
                         <tbody>
                          <?php //print_r($v_list);die;
                            foreach ($data['v_list'] as $value) {  
                           ?>
                            <tr>
                               <td>
                                  <a href="#">
                                     <div class="d-flex align-items-center">
                                        <div class="avatar avatar-blue mr-3">
                                           <img src="<?php echo $value->profile_pic; ?>" class="avatar-img">
                                        </div>
                                        <div class="">
                                           <p class="font-weight-bold mb-0"><?php echo $value->name; ?></p>
                                        </div>
                                     </div>
                                  </a>
                               </td>
                               <td><?php echo $value->email; ?></td>
                               <td><?php echo $value->contactno; ?></td>
                               <td><?php echo $value->contactperson; ?></td>
                               <td><?php echo $value->purpose; ?> </td>
                               
                            </tr>
                          <?php } ?>
                         </tbody>
                      </table>
                  </div>
                </div><!-- Card Body -->

              </div>
            </div> <!-- col-xl-4 col-lg-4 -->
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Upcoming Visitors</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                    
                      <table id="example" class="table table-hover responsive" style="width:100%">
                         <thead>
                            <tr>
                              <th>Visitor</th>
                              <th>Email</th>
                              <th>Contact No</th>
                              <th>Meeting With</th>
                              <th>Purpose</th>
                            </tr>
                         </thead>
                         <tbody>
                          <?php //print_r($data['v_list']);
                            foreach ($data['v_list'] as $value) {  
                           ?>
                            <tr>
                               <td>
                                  <a href="#">
                                     <div class="d-flex align-items-center">
                                        <div class="avatar avatar-blue mr-3">
                                           <img src="<?php echo $value->profile_pic; ?>" class="avatar-img">
                                        </div>
                                        <div class="">
                                           <p class="font-weight-bold mb-0"><?php echo $value->name; ?></p>
                                        </div>
                                     </div>
                                  </a>
                               </td>
                               <td><?php echo $value->email; ?></td>
                               <td><?php echo $value->contactno; ?></td>
                               <td><?php echo $value->contactperson; ?></td>
                               <td><?php echo $value->purpose; ?> </td>
                               
                            </tr>
                          <?php } ?>
                         </tbody>
                      </table>
                  </div>
                </div><!-- Card Body -->

              </div>
            </div> <!-- col-xl-12 col-lg-12 -->
            
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url() ?>assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url() ?>assets/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo base_url() ?>assets/js/demo/chart-pie-demo.js"></script>
  
  
<script type="text/javascript">
  $(function() {
    $('#flash').delay(500).fadeIn('normal', function() {
      $(this).delay(500).fadeOut();
    });
  });
  
  //daterange
  
  $(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
       // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#reportrange span').html(start.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});
</script>
<script type="text/javascript">
  $(document).ready(function() {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,
     paging:   false,
    ordering: false,
     info:     false,
     searching: false,
    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });

  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});
</script>