<script src="http://greetlogs.com/greetlog_admin/assets/js/bootstrap-formhelpers.min.js"></script> 
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Edit Branch </h1>
  <?php

       if(isset($breadcrumb)&&  !is_null($breadcrumb)){
       ?> 
       <div class="span10" style="margin-left:5px;">
          
             <ul class="breadcrumb">
                <?php
                   echo $breadcrumb ;             
                ?>     
             </ul>
          
       </div>
       <?php 
        }
    ?>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <!-- <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div> -->
    <div class="card-body">
      <div class="form">
        <form  method="post" action="<?php echo base_url('update-branch'); ?>" enctype="multipart/form-data" data-toggle="validator" role="form" id="myForm" novalidate="" >
          <?php if (isset($data)) {

               foreach ($data as $emp) {
                //print_r($emp);
          ?>
          <div class="form-row">
            <div class="col-md-8">
              <div class="form-row">
                <input type="hidden" name="u_id" value="<?php echo $emp->branch_id; ?>">
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="text" class="form-control" name="branchname" value="<?php echo $emp->branchname; ?>" placeholder="-" required>
                    <span class="" for="branchname"> Branch Name</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter branch name. This field is required.</div>
                  </label>
                </div>
                
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="text" class="form-control" name="branchhead" value="<?php echo $emp->branchhead; ?>" placeholder="-" required>
                    <span class="" for="adminname"> Admin Name</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter admin name. This field is required.</div>
                  </label>
                </div>
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="email" class="form-control" name="email_id" value="<?php echo $emp->email_id; ?>" placeholder="-" required>
                    <span class="" for="adminemail">Admin Email</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter admin email. This field is required.</div>
                  </label>
                </div>
    
              <div class="form-group col-md-6 ">
                   <label class="form-group has-float-label">
                
                        <select  id="countries_phone1" class="form-control bfh-countries"  name="country"  data-country="<?php echo $emp->country; ?>"></select>
                      <span>Country</span>
                      
                    </label>
                </div>
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="text" class="form-control bfh-phone" data-country="countries_phone1" name="contactno" value="<?php echo $emp->contactno; ?>" placeholder="-" required>
                    <span class="" for="contactno">Contact No.</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter contact no. This field is required.</div>
                  </label>
                </div>
               
                <div class="form-group col-md-12">
                  <label class="has-float-label">
                    <input type="text" class="form-control" name="slogan" value="<?php echo $emp->slogan; ?>" placeholder="-">
                    <span class="" for="contactno">Branch Tag</span>
                  </label>
                </div>
                <div class="form-group col-md-12">
                    <label>Access Provide : </label>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full" <?php if ($emp->access=='full'){ echo "checked"; }?>>
                      <label class="custom-control-label" for="customRadioInline4">Full Access</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view" <?php if ($emp->access=='view'){ echo "checked"; }?>>
                      <label class="custom-control-label" for="customRadioInline5">View Only</label>
                    </div>
                </div>
                
                <div class="form-group col-md-12">
                  <label class="has-float-label">
                    <textarea class="form-control" name="location" placeholder="-" required><?php echo $emp->location; ?></textarea>
                    <span class="" for="location">Location</span>
                  </label>
                </div>
              </div>
            </div>
            <!--pic start-->
            <div class="col-md-4">
               <div class="form-group col-md-12">
                  <div class="avatar-upload">
                      <div class="avatar-edit">
                          <input type='file' id="imageUpload" name="logo"/>
                          <label for="imageUpload"></label>
                      </div>
                      <div class="avatar-preview">
                          <div id="imagePreview" style="background-image: url(<?php echo $emp->logo; ?>);">
                          </div>
                      </div>
                    </div>
                  </div>
              </div>
                  
                <!--pic close-->
            
          </div>
         <?php } } ?>
          <button type="submit" class="btn btn-green shadow-sm">Submit</button>
         <!--  <a href="#" class=" btn btn-green shadow-sm"> Submit</a> -->

        </form>
      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->
 
<!-- Bootstrap Form Helpers --> 
  <!--for contact no country code-->

<script type="text/javascript">

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});


/*$("#myForm").submit(function(event) {
         
     // make selected form variable
     var vForm = $(this);
     
   
     if (vForm[0].checkValidity() === false) {
       event.preventDefault()
       event.stopPropagation()
     } else {
      
       // Replace alert with ajax submit here...
       //alert("your form is valid and ready to send");
       
     }
     
     // Add bootstrap 4 was-validated classes to trigger validation messages
     vForm.addClass('was-validated');
     
  
 });*/

</script>