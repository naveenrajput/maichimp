      <div class="container-fluid">

        <?php
   
         if(isset($breadcrumb)&&  !is_null($breadcrumb)){
         ?> 
         <div class="span10" style="margin-left:5px;">
            
               <ul class="breadcrumb">
                  <?php
                     echo $breadcrumb ;             
                  ?>     
               </ul>
            
         </div>
         <?php } ?>
            
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php if (isset($datahead)) { echo $datahead; } ?></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">

                <table id="example" class="table table-hover responsive nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th>Branch Name</th>
                      <th>Admin Name</th>
                      <th>Contact No.</th>
                      <th>Plan</th>
                      <th>Expiry Date</th>
                      <th>Company Tag</th>
                      <th>Location</th>
                      <!-- <th>Paid/Unpaid</th> -->
                      <!-- <th>Create Date</th> -->
                      <th>Access</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (isset($company)) {
                         foreach ($company as $cmp) {
                    ?>
                      <tr>
                        <td>
                          <a href="#">
                            <div class="d-flex align-items-center">
                              <div class="avatar avatar-blue mr-3"><img src="<?php echo $cmp->logo; ?>" class="avatar-img"></div>

                              <div class="">
                                <p class="font-weight-bold mb-0"><a href="#" data-title="View Account" style="text-decoration: underline;"><?php echo $cmp->companyname; ?></a></p>
                                <p class="text-muted mb-0"><?php echo $cmp->email; ?></p>
                              </div>
                            </div>
                          </a>
                        </td>
                       
                        <td><?php echo $cmp->ownername; ?></td>
                        <td><?php echo $cmp->contactno; ?></td>
                        <td><?php echo $cmp->plan; ?></td>
                        <td><?php echo date("Y-m-d",$cmp->expirydate); ?></td>
                        <td><?php echo $cmp->slogan; ?></td>
                        <td><?php echo $cmp->address; ?></td>
                        <td>Full </td>
                        <td>
                        <div class="dropdown">
                          <button class="btn btn-sm btn-icon" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-h" data-toggle="tooltip" data-placement="top"
                                  title="Actions"></i>
                              </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                             <a class="dropdown-item" href="#"><i class="far fa-eye mr-2"></i> Btn</a>
                            <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt mr-2"></i> Edit Profile</a>
                            <a class="dropdown-item text-danger" href="#"><i class="far fa-trash-alt mr-2"></i> Remove</a>
                          </div>
                        </div>
                      </td>
                      </tr>
                    <?php } } ?>
                    <?php if (isset($branch)) {
                          foreach ($branch as $emp) {
                    ?>
                    <tr>
                      <td>
                        <a href="#">
                          <div class="d-flex align-items-center">
                            <div class="avatar avatar-blue mr-3"><img src="<?php echo $emp->logo; ?>" class="avatar-img"></div>

                            <div class="">
                              <p class="font-weight-bold mb-0"><a href="#" data-title="View Account" style="text-decoration: underline;"><?php echo $emp->branchname; ?></a></p>
                              <p class="text-muted mb-0"><?php echo $emp->email_id; ?></p>
                            </div>
                          </div>
                        </a>
                      </td>
                      <td><?php echo $emp->branchhead; ?></td>
                      <td><?php echo $emp->contactno; ?></td>
                      <td><?php echo $emp->plan; ?></td>
                      <td><?php echo date("Y-m-d",$emp->expirydate); ?></td>
                      <td><?php echo $emp->slogan; ?></td>
                      <td><?php echo $emp->location; ?></td>
                      
                      <!-- <td><?php //echo $emp->createdate; ?></td> -->
                      <td><?php echo $emp->access; ?></td>
                      <td>
                        <div class="dropdown">
                          <button class="btn btn-sm btn-icon" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-h" data-toggle="tooltip" data-placement="top"
                                  title="Actions"></i>
                              </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                             <a class="dropdown-item" href="#"><i class="far fa-eye mr-2"></i> Btn</a>
                            <a class="dropdown-item" href="<?php echo base_url('edit-branch/').$emp->branch_id; ?>"><i class="fas fa-pencil-alt mr-2"></i> Edit Profile</a>
                            <a class="dropdown-item text-danger" href="#"><i class="far fa-trash-alt mr-2"></i> Remove</a>
                          </div>
                        </div>
                      </td>
                    </tr>

                   <?php  } } ?>
                  </tbody>
                </table>


                
              </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
<!-- End of Main Content -->

<script>
$(document).ready(function() {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,

    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });

  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});

</script>