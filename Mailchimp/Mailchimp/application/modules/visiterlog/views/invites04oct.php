<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Single Invites</h1>

  <div class="col-md-4">
      <a href="<?php echo base_url('group-invites'); ?>" class="btn btn-green">Group Invites</a>
  </div>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div>
    <div class="card-body">
      <div class="form">
        <form  method="post" enctype="multipart/form-data" action="<?php echo base_url('insert-invites'); ?>" data-toggle="validator" role="form">
          <div class="form-row">
            <div class="form-group col-md-4">
               <select name="branch" class="form-control" placeholder="Branch" required>
                <?php foreach ($company as $com) { ?>
                  <option value="<?php echo $com->companyname; ?>"><?php echo $com->companyname; ?></option>
                <?php } ?>
                <?php foreach ($branch as $bran) { ?>
                  <option value="<?php echo $bran->branchname; ?>"><?php echo $bran->branchname; ?></option>
                <?php } ?>
               </select>
            </div>
            <div class="form-group col-md-4">
               <select name="visitortype" class="form-control" placeholder="Visitor Type" required>
                  <option value="" selected>Visitor Type</option>
                  <option value="Visitor">Visitor</option>
               </select>
            </div>
            <div class="form-group col-md-4">
              <input type="text" name="visitor_name" class="form-control" placeholder="Full Name" required>
            </div>
            <div class="form-group col-md-4">
              <input type="date" name="invitedate" class="form-control" placeholder="Arrival Date" required>
            </div>
            <div class="form-group col-md-4">
              <input type="time" name="invitetime" class="form-control" placeholder="Arrival Time" required>
            </div>
            <div class="form-group col-md-4">
              <input type="text" name="contactnum" class="form-control" placeholder="Mobile" required>
            </div>
            <div class="form-group col-md-4">
              <input type="email" name="email" class="form-control" placeholder="Email" required>
            </div>
            <div class="form-group col-md-4">
                <label for="sendType">Send Type : </label>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email">
                  <label class="custom-control-label" for="customRadioInline1">Email</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message">
                  <label class="custom-control-label" for="customRadioInline2">Message</label>
                </div>
            </div>
            <div class="form-group col-md-4">
              <select name="host" class="form-control" placeholder="Host" required>
                <option value="" selected>Select Host</option>
                <?php foreach ($emp as $employee) { ?>
                  <option value="<?php echo $employee->empname; ?>"><?php echo $employee->empname; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-md-12">
              <textarea type="text" name="msg" class="form-control" placeholder="Remark" required></textarea>
            </div>
          </div>

          <button type="submit" class="btn btn-green shadow-sm">Submit</button>

        </form>
      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <th>Email</th>
                      <th>Arrival Date</th>
                      <th>Arrival Time</th>
                      <th>Host Name</th>
                      <th>Remarks</th>
                    
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <th>Email</th>
                      <th>Arrival Date</th>
                      <th>Arrival Time</th>
                      <th>Host Name</th>
                      <th>Remarks</th>
                     
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php if (isset($invites)) {
                         $i=1; foreach ($invites as $invite) {
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $invite->visitor_name; ?></td>
                      <td><?php echo $invite->contactnum; ?></td>
                      <td><?php echo $invite->email; ?></td>
                      <td><?php echo $invite->invitedate; ?></td>
                      <td><?php echo $invite->invitetime; ?></td>
                      <td><?php echo $invite->host; ?></td>
                      <td><?php echo $invite->msg; ?></td>
                    </tr>
                  <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
