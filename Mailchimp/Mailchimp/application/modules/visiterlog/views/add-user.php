  <!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Add User</h1>
 <?php

       if(isset($breadcrumb)&&  !is_null($breadcrumb)){
       ?> 
       <div class="span10" style="margin-left:5px;">
          
             <ul class="breadcrumb">
                <?php
                   echo $breadcrumb ;             
                ?>     
             </ul>
          
       </div>
       <?php 
        }
    ?>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <!-- <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div> -->
    <?php if (isset($data)) {
            if($data!="3"){    ?>
    
    <div class="card-body">
      <div class="form">
        <form  method="post" id="myForm" action="<?php echo base_url('insert-user'); ?>" data-toggle="validator" enctype="multipart/form-data" role="form" novalidate="">
          <div class="form-row">
            <div class="col-md-8">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="text" class="form-control" name="empname" placeholder="-" required>
                    <span class="" for="username">User Name</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter user name. This field is required.</div>
                  </label>
                </div>
                <div class="form-group col-md-6 ">
                   <label class="form-group has-float-label">
                      <select  id="countries_phone1" class="form-control bfh-countries" data-country="IN"></select>
                      <span>Country</span>
                      <div class="valid-feedback"></div>
                      <div class="invalid-feedback">Please enter country. This field is required.</div>
                    </label>
                </div>
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="text" class="form-control bfh-phone" data-country="countries_phone1" name="contact_num" placeholder="-" required>
                    <span class="" for="contact_num">Contact Number</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter contact number. This field is required.</div>
                  </label>
                </div>
                
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="email" class="form-control" name="email_id" placeholder="Email" required>
                    <span class="" for="email_id">Email</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter email. This field is required.</div>
                  </label>
                </div>
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                    <span class="" for="Password">Password</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter password. This field is required.</div>
                  </label>
                </div>
                <div class="form-group col-md-6">
                    <select name="gender" class="form-control" required="">
                      <option value="">Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      <option value="TransGender">TransGender</option>
                    </select>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please select gender. This field is required.</div>
                </div>
                <div class="form-group col-md-6">
                  <label class="has-float-label">
                    <input type="date" class="form-control" name="dob" placeholder="-" required>
                    <span class="" for="dob">Date Of Birth</span>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Please enter date of birth. This field is required.</div>
                  </label>
                </div>
              <div class="form-group col-md-6">
                <label class="has-float-label">
                  <input type="text" class="form-control" name="designation" placeholder="-" required>
                  <span class="" for="designation">Designation</span>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter designation. This field is required.</div>
                </label>
              </div>
              
              <div class="form-group col-md-12">
                  <label>Access Provide : </label>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full" class="sr-only" required>
                    <label class="custom-control-label" for="customRadioInline4">Full Access</label>
                  </div>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view" class="sr-only" required checked="">
                    <label class="custom-control-label" for="customRadioInline5">View Only</label>
                  </div>
              </div>
              </div>
            </div>
            <div class="col-md-4">
              <!--pic start-->
                  <div class="form-group col-md-12">
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" name="profile_pic">
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url(<?php echo base_url('uploads/empdef.jpg'); ?>);">
                            </div>
                        </div>
                    </div><span id="err_img"></span><span><?php if(isset($error)){ echo $error;} ?></span>
                  </div>
                <!--pic close-->
            </div>
          <button type="submit" class="btn btn-green shadow-sm" value="upload">Submit</button>
         <!--  <a href="#" class=" btn btn-green shadow-sm"> Submit</a> -->

        </form>
        </div>
      </div>
    </div>
  <?php }else{ ?>
<div>You Create Only Three User Account</div>
 <?php } } ?>
  </div>
</div>

<!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->

<script type="text/javascript">

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});


//image validation
/*$("#myForm").submit(function(e) {
  var inp = document.getElementById('imageUpload');

  if (inp.files.length == 0) {

      $('#err_img').css('color','red');
      $("#err_img").html("Please uplolad logo");
      e.preventDefault();
  }
    
});*/

/*$("#myForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               //alert("your form is valid and ready to send");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });*/
</script>