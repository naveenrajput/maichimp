<?php 

$filename = "Employees_" . date('Y-m-d') . ".csv"; 
$delimiter = ","; 
 
// Create a file pointer 
$f = fopen('php://memory', 'w'); 
 
// Set column headers 
$fields = array('S.No','Name', 'Email', 'Phone', 'Designation', 'DOB'); 
fputcsv($f, $fields, $delimiter); 
 
// Get records from the database 
/*$result =$this->db->query("SELECT * FROM employees WHERE branchid='".$bid."' AND companyid='".$cid."' ");*/
/*if($result->num_rows > 0){ */
    // Output each row of the data, format line as csv and write to file pointer 
    $i=1; foreach($result->result_array() as $row){
        $lineData = array( $i, $row['empname'], $row['email_id'], $row['contact_num'], $row['designation'], $row['dob']); 
        fputcsv($f, $lineData, $delimiter); 
    $i++;} 
/*}*/
// Move back to beginning of file 
fseek($f, 0); 
 
// Set headers to download file rather than displayed 
header('Content-Type: text/csv'); 
header('Content-Disposition: attachment; filename="' . $filename . '";'); 
 
// Output all remaining data on a file pointer 
fpassthru($f); 
 
// Exit from file 
exit();