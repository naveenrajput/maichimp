<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Greetlog - Log In</title>
      <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">
      <!-- Custom fonts for this template-->
      <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/formValidation.min.css">
      <link rel="stylesheet" href="https://cdn.rawgit.com/tonystar/bootstrap-float-label/v4.0.1/dist/bootstrap-float-label.min.css">
      
      <style>
     
         <!--new--->
      </style>
   </head>
   <body class="bg-gradient-primary">
      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-6">
               <div class="fadeIn first m-t-50 m-b-30 text-center">
                  <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="200" class="" alt="Greetlog">
               </div>
               <div class="card o-hidden border-0 shadow-lg m-t-10 m-b-10">
                  <div class="card-body p-0">
                     <!-- Nested Row within Card Body -->
                     <div class="row">
                        <!----<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>-->
                        <div class="col-lg-12">
                           <div class="p-4">
                              <div class="text-center">
                                 <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                              </div>

                              <?php if ( $this->session->flashdata('msg')){ ?>
                                 <div class="alert alert-danger alert-dismissible fade show" id="flash">
                                   <strong>Error! </strong><?php echo $this->session->flashdata('msg');?>
                                 </div>
                              <?php }elseif ($this->session->flashdata('success_msg')) { ?>
                                 <div class="alert alert-success alert-dismissible fade show" id="flash">
                                   <strong>Success! </strong><?php echo $this->session->flashdata('success_msg');?>
                                 </div>
                             <?php }elseif ($this->session->flashdata('succ_msg')) { ?>
                                 <div class="alert alert-success alert-dismissible fade show" id="flash">
                                   <strong>Success! </strong><?php echo $this->session->flashdata('succ_msg');?>
                                 </div>
                              <?php } ?>

                              <form class="card-body" novalidate="" action="<?php echo base_url('chklogin');?>" method="post" id="myForm">
                                <!-- <div class="form-group ">
                                    <span class="has-float-label">
                                  <input class="form-control" id="first" type="text" placeholder="Name"/>
                                  <label for="first">First</label>
                                </span>
                                    
                                 </div>-->
                                 <div class="form-group">
                                      <label class="has-float-label">
                                   
                                    <input id="email" class="form-control" name="email" type="email" placeholder="-" value="<?php if(isset($email)){echo $email;} ?>" required>
                                     <span class="" for="email">Email</span>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                                      </label>
                                 </div>
                                 <div class="form-group">
                                      <label class="has-float-label">
                                  
                                    <input id="password" class="form-control" name="password" type="password" placeholder="********" required>
                                      <span class="" for="password">Password</span>
                                     
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a password. This field is required.</div>
                                    </label>
                                     <p toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></p>
                                 </div>
                                 <div class="col-sm-12   text-right">
                                    <a class="small text-basic" href="<?php echo base_url('verify_email') ?>">Forgot Password?</a>
                                 </div>
                                 <button  type="submit" class="btn btn-green btn-user  btn-block m-t-25">
                                 Log in
                                 </button>
                              </form>
                              <hr>
                              <div class="text-center">
                                 <a class=" text-basic" href="<?php echo base_url('register') ?>">Create an Account!</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>
 <!--     <script src="<?php //echo base_url() ?>assets/js/custom.js"></script>-->
      <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.slim.min.js"></script>
      <script src="<?php echo base_url() ?>assets/js/bootstrap-4-latest.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <script>
         // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
        /* $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               //alert("your form is valid and ready to send");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });*/

        $(function() {
          $('#flash').delay(300).fadeIn('normal', function() {
            $(this).delay(4000).fadeOut();
          });
        });
        
        //password show and hide
        $(".toggle-password").click(function() {
         
           $(this).toggleClass("fa-eye fa-eye-slash");
           var input = $($(this).attr("toggle"));
           if (input.attr("type") == "password") {
             input.attr("type", "text");
           } else {
             input.attr("type", "password");
           }
         });
      </script>
      
<!--Jquery Validation By Badal-->

<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
<!---<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>---->
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
<script type="text/javascript">
$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
  $("#myForm").validate({
    
  rules: {
    password: {
      required: true,
      minlength: 8,
      maxlength: 20,
    },
    email: {
      required: true,
      email:true
    }
  },
  //For custom messages
  messages: {
    password:{
      required: "This field is required.",
      minlength: "Enter at least 8 characters"
    },
    email:{
      required: "This field is required.",
    }
  },
  errorElement : 'div',
  errorPlacement: function(error, element) {
    var placement = $(element).data('error');
    if (placement) {
      $(placement).append(error)
    } else {
      error.insertAfter(element);
    }
  }
});
</script>
<!--Jquery Validation-->
    
   </body>
</html>
