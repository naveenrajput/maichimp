      <div class="container-fluid">
          <!-- Page Heading -->
          <div class="row">
            <div class="col-md-10">
              <h1 class="h3 mb-2 text-gray-800">Visitor List</h1>
            </div>
            <div class="col-md-2">
              <a href="<?php echo base_url('vexport'); ?>" class="btn btn-green"><i class="exp"></i>Export</a>
            </div>
          </div>     
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Profile</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <!-- <th>Alternate Contact</th> -->
                      <th>Email</th>
                      <th>Status</th>
                      <th>Create Date</th>
                    
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Profile</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <!-- <th>Alternate Contact</th> -->
                      <th>Email</th>
                      <th>Status</th>
                      <th>Create Date</th>
                     
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php if (isset($data)) {
                         $i=1; foreach ($data as $emp) {
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><img src="http://i.pravatar.cc/500?img=7" style="width:50px;"></td>
                      <td><?php echo $emp->name; ?></td>
                      <td><?php echo $emp->contactno; ?></td>
                      <!-- <td><?php // echo $emp->alternateno; ?></td> -->
                      <td><?php echo $emp->email; ?></td>
                      <td><?php echo $emp->status; ?></td>                     
                      <td><?php echo $emp->createdate; ?></td>
          
                    </tr>
                  <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
<!-- End of Main Content -->