<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Employee Form</h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
    </div>
    <div class="card-body">
      <div class="form">
        <form  method="post" id="myForm" action="<?php echo base_url('update-emp'); ?>" enctype="multipart/form-data" data-toggle="validator" role="form">
          <?php if (isset($data)) {
                foreach ($data as $emp) {
          ?>
          <input type="hidden" name="u_id" value="<?php echo $emp->emp_id; ?>">
          <div class="form-row">
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="empname" placeholder="Name" value="<?php echo $emp->empname; ?>" required>
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="contact_num" placeholder="Contact Number" value="<?php echo $emp->contact_num; ?>" required>
            </div>    
            <!--pic start-->
              <div class="form-group col-md-4">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="profile_pic"  />
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(<?php echo $emp->profile_pic; ?>);">
                        </div>
                    </div>
                  </div>
                </div>
            <!--pic close-->
            <div class="form-group col-md-6">
              <input type="email" class="form-control" name="email_id" placeholder="Email" value="<?php echo $emp->email_id; ?>" required>
            </div>
             <div class="form-group col-md-6">
              <input type="date" class="form-control" name="dob" placeholder="Date Of Birth" value="<?php echo $emp->dob; ?>" required>
            </div>
          <div class="form-group col-md-6">
            <input type="text" class="form-control" name="designation" placeholder="designation" value="<?php echo $emp->designation; ?>" required>
          </div>
          </div>
          <?php } } ?>
          <button type="submit" class="btn btn-green shadow-sm">Submit</button>
         <!--  <a href="#" class=" btn btn-green shadow-sm"> Submit</a> -->

        </form>
      </div>
    </div>
  </div>

</div>

<!-- /.container-fluid -->

</div>
      <!-- End of Main Content -->

<script type="text/javascript">

 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

</script>