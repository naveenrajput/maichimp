
<style>
    #myInput {
  background-image: url('assets/img/icons/searchicon.png');
  background-position: right ;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 20px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
  text-align:left;
}
.custom-file-uploader {
  position: relative;
  
  input[type='file'] {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 5;
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: default;
  }
}

</style>
<div class="container-fluid"> 
  
  <!-- Page Heading -->
  <div class="row">
   <div class="col-md-8">
	<h1 class="h3 mb-2 text-gray-800">Employee List</h1>
   <?php
   
   if(isset($breadcrumb)&&  !is_null($breadcrumb)){
   ?> 
   <div class="span10" >
      
         <ul class="breadcrumb">
            <?php
               echo $breadcrumb ;             
            ?>     
         </ul>
      
   </div>
   <?php 
      }
      ?>
    </div>
    
    <!-- CSV file upload form --> 
    <!--  <div class="col-md-12" id="importFrm" style="display: none;">
                <form action="<?php //echo base_url('import'); ?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="file"/>
                    <input type="submit" class="btn btn-green" name="importSubmit" value="IMPORT">
                </form>
            </div> --> 
  </div>
  
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h5 class="m-b-20 font-weight-bold text-primary"><?php if (isset($em)) { echo $em; } ?> Employees </h5>
	  <div class=" row ">
        <div class="col-lg-8 left-shift"> <a href="javascript:void(0);" class="btn btn-outline-secondary m-b-5 " data-toggle="modal" data-target="#Mymodal"><i class="plus"></i>Import Employee</a> <a href="<?php echo base_url('export'); ?>" class="btn btn-outline-secondary  m-b-5 "><i class="exp"></i> Export</a> </div>
        <div class="col-md-4 right-shift" style=""> <a href="<?php echo base_url('add-employee'); ?>" class="btn btn-green  m-b-5 "><i class="fa fa-plus" aria-hidden="true"></i> Employee</a> </div>
      </div>
    </div>
  
    <div class="card-body p-t-0 p-b-0 ">
      <div class="row ">
        <div class="col-lg-3 border-right-new">
          <div class="contacts_body  ">
               <div class="col-lg-12 m-b-20">
              <form class="d-none d-sm-inline-block form-inline  ">
            <div class="input-group">
              <input type="text" class="form-control border-0 small"  id="myInput" onkeyup="myFunction()"  placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
             
            </div>
          </form>
          </div>
            <!--<div class="form-group has-search">
              <input id="search" type="text" class="form-control" placeholder="Search">
              <span class="fa fa-search form-control-feedback"></span>
            </div>-->
  <ul class="contacts " id="myUL">
            <?php if (isset($data)) {
                foreach ($data as $emp) {
            ?>
          
          
              <li>
                 
                 <a href="#" class="showForm">
                      <?php if ($emp->access!="") { ?><img  title="sub-admin"  src="<?php echo base_url('uploads/flag.png'); ?> " style="width: 20px ;"><?php } ?>
                <div class="d-flex bd-highlight emp_form" id="<?php echo $emp->emp_id; ?>">
                  <div class="img_cont" ><?php if (!empty($emp->profile_pic)) { ?><img src="<?php echo $emp->profile_pic; ?>" class="rounded-circle user_img"><?php }elseif ($emp->gender=='Male') {?> <img src="<?php echo base_url('uploads/men.png'); ?>" class="rounded-circle user_img"><?php } else{ ?> <img src="<?php echo base_url('uploads/girl.png'); ?>" class="rounded-circle user_img"> <?php }?></div>
                  <div id="employeeN" class="user_info text-wrap"> <span><?php echo $emp->empname; ?></span> </div>
                </div>
                </a>
              </li>
           
           
            <?php  } } ?>
             </ul>
          </div>
        </div>
        <div class="col-lg-9">
          <div id=""   class="addForm p-t-20 p-b-20 p-l-10 p-r-10" style="display:none;">
            <div id="emp"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content --> 
<!-- ALL MODALS VIEW -->
<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content" id="Mymodal_view">
         <div class="modal-header">
            <h4>Import CSV File</h4>
            <button type="button" class="btn btn-default pull-left" onclick="javascript:window.location.reload()" data-dismiss="modal">X</button>
         </div>
         <div class="modal-body">
            
             
          <form action="" id="fupForm" method="post" enctype="multipart/form-data">
               <input type="file" name="file"/>
               <input type="submit" class="btn btn-green" name="importSubmit" value="IMPORT">
            </form>
            <div class="m-t-10 m-b-10">
                  <a href="<?php echo base_url('uploads/Employees_Samplefile.csv');?>" >Download Sample File</a>
                
            </div>
          
            <div id="sms"></div>
         </div>
         
         
      </div>
      <!-- /.modal-content --> 
   </div>
   <!-- /.modal-dialog --> 
</div>


<script>

//form form show kavita
$(function() {

  $(".showForm").on("click", function() {

    $(".addForm").show();

  });

});

$(function() {

  $(".emp_form").on("click", function() {
    var id = this.id;
    //$('#emp').remove();
     $.ajax({
           type:"POST",
           url:'<?php echo base_url("get_Employee");?>',
           data:{id:id},
           success: function(response){
              //$('#emp').replace();
               $('#emp').html(response); 
          
           }
       });

  });

});
</script> 
<script>
   $(document).ready(function() {
     $("#example").DataTable({
       aaSorting: [],
       responsive: true,
   
       columnDefs: [
         {
           responsivePriority: 1,
           targets: 0
         },
         {
           responsivePriority: 2,
           targets: -1
         }
       ]
     });
   
     $(".dataTables_filter input")
       .attr("placeholder", "Search here...")
       .css({
         width: "300px",
         display: "inline-block"
       });
   
     $('[data-toggle="tooltip"]').tooltip();
   });
   

   $(document).ready(function(e){
    // Submit form data via Ajax
    $("#fupForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url('import'); ?>',
            data: new FormData(this),
            dataType: 'html',
            contentType: false,
            cache: false,
            processData:false,
           
            success: function(successMsg){
            //alert(successMsg);
            $('#sms').html(successMsg);
            setTimeout(function() {$('#Mymodal').modal('hide');location.reload();},10000);
            }
        });
    });

});
// search

function myFunction() {
    var input, filter, ul, li, a;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

</script>
