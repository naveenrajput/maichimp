<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Greetlog - Register</title>
      <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">
      <!-- Custom fonts for this template-->
      <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/formValidation.min.css">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-float-label.min.css">
        
	  <!-- Bootstrap Form Helpers country code -->
	<link href="<?php echo base_url() ?>assets/css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.js"></script>
	  <script src="js/respond.min.js"></script>
	<![endif]-->
 
   </head>
   <body class="bg-gradient-primary">
      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-6">
               <div class="fadeIn first m-t-50 m-b-30 text-center">
                  <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="200" class="" alt="Greetlog">
               </div>

               <div class="card o-hidden border-0 shadow-lg m-t-10 m-b-30">
                  <div class="card-body p-0 ">
                     <!-- Nested Row within Card Body -->
                     <div class="row">
                        <!----<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>-->
                        <div class="col-lg-12">
                           <div class="p-4 ">
                              <div class="text-center">
                                 <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                              </div>

                              <?php if ( $this->session->flashdata('email_err')): ?>
                                 <div class="alert alert-danger alert-dismissible fade show" id="flash">
                                   <strong>Error! </strong><?php echo $this->session->flashdata('email_err');?>
                                 </div>
                             <?php endif; ?>

                              <form class="card-body" novalidate="" action="<?php echo base_url('registration');?>" method="post" id="myForm" autocomplete="off">
                                  <div class="form-row">
                           <div class="form-group col-lg-12">
                                 <label class="form-group has-float-label">
                                    <select class="form-control" id="ctype" name="companytype" required>
                                       <option value=""  class="small" disabled selected>Select</option>
                                       <option value="Company Name">Company</option>
                                       <option value="Corporation Name">Corporation</option>
                                       <option value="School/Institute Name">School/Institute</option>
                                       <option value="College/University Name">College/University</option>
                                       <option value="Title Name">Other</option>
                                    </select>
                                    <span>Whats You are ?</span>
                                    <div class="valid-feedback"> </div>
                                    <div class="invalid-feedback">Please select option. This field is required.</div>
                                  </label>
                                   
                                 </div>
                                
                                 <div class="form-group col-lg-12">
                                        <label class="has-float-label">
                                   <input id="cname " class="form-control input-text" type="text" name="companyname"  placeholder="-" required>
                                      <span class="" for="cname" id="tipeLabel">Company Name</span>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a company name. This field is required.</div>
                                    </label>
                                 </div>
                                        
                           <div class="form-group col-lg-3 m-t-20" >
                                 <label class="form-group has-float-label">
                                    <select class="form-control" id="" name="prefix" >
                                         <option value=""  class="small" disabled selected>Select</option>
                                        <option value="Ms"> Ms </option> 
                                        <option value="Miss"> Miss </option> 
                                        <option value="Mrs"> Mrs </option> 
                                        <option value="Mr"> Mr </option> 
                                        <option value="Master"> Master </option> 
                                        <option value="Rev"> Reverend (Rev) </option>
                                        <option value="Dr"> Doctor (Dr) </option> 
                                        <option value="Atty"> Attorney (Atty) </option>  
                                        <option value="Hon"> Honorable (Hon) </option>  
                                        <option value="Prof"> Professor (Prof) </option>  
                                
                                        <option value="Ofc"> Officer (Ofc) </option> 
                                    </select>
                                    <span>Prefix</span>
                                   
                                  </label>
                                   
                                 </div>
                                 
                                 <div class="form-group col-lg-9">
                                     <label class="has-float-label">
                                 
                                    <input id="aname" class="form-control input-text" type="text" name="ownername" placeholder="-" required>
                                    <span class="" for="aname">Admin First Name </span>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a admin name. This field is required.</div>
                                    </label>
                                 </div>
                                  
                                 <div class="form-group col-lg-6">
                                     <label class="has-float-label">
                                 
                                    <input id="aname" class="form-control input-text" type="text" name="middlename" placeholder="-" >
                                    <span class="" for="aname">Middle Name</span>
                                  
                                    </label>
                                 </div>
                                 <div class="form-group col-lg-6">
                                     <label class="has-float-label">
                                 
                                    <input id="aname" class="form-control input-text" type="text" name="lastname" placeholder="-" required>
                                    <span class="" for="aname">Last Name</span>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a admin name. This field is required.</div>
                                    </label>
                                 </div>
                               
                                 <div class="form-group col-lg-12">
                                       <label class="has-float-label">
                                   
                                    <input id="email" class=" form-control" type="email" name="email" required placeholder="-">
                                     <span class="" for="email">Email</span>
                                    <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                                     </label>  
                                 </div>
                                  <div class="form-group col-lg-12">
                                  <label class="form-group has-float-label">
                                    <select id="countries_phone1" name="country" class="form-control bfh-countries" data-country="IN"></select>
                                    <span>Country</span>
                                    <div class="valid-feedback"> </div>
                                    <div class="invalid-feedback">Please select option. This field is required.</div>
                                  </label>
                                   
                                 </div>
                                 <div class="form-group col-lg-12">
									<label class="has-float-label">
									<input type="text" class="form-control bfh-phone" name="contactno" data-country="countries_phone1" placeholder="-" required>
									 <span class="" for="cnumber">Contact Number</span>
									 <div class="valid-feedback"></div>
                                    <div class="invalid-feedback">Please enter a contact number. This field is required.</div>
                                    
									</label>
								 </div>
                             
                                 <button  type="submit" class="btn btn-green btn-user  btn-block m-t-25">
                                 Submit
                                 </button>
                                 </div>
                              </form>
                              <hr>
                              <div class="text-center">
                                 <a class=" text-basic" href="<?php echo base_url('login') ?>">Already have an account? Log in !</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>
      <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
      <!--  <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap-4-latest.min.js"></script>-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
      <!--for contact no country code-->
      <script src="<?php echo base_url() ?>assets/js/bootstrap-formhelpers.min.js"></script> 
      <script>
         // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
         $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
              // alert("You have registered successfully. Please check your email");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });



  $(function() {
    $('#flash').delay(300).fadeIn('normal', function() {
      $(this).delay(4000).fadeOut();
    });
  });
         
         
      </script>
      
<!--Jquery Validation By Badal-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
<script type="text/javascript">
$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    // --                                    or leave a space here ^^
});
  $("#myForm").validate({
    
  rules: {
    companytype: {
      required: true,
    },
    companyname: {
      required: true,
    },
    ownername: {
      required: true,
    },
    lastname: {
      required: true,
    },
    email: {
      required: true,
      email:true
    },
    contactno: {
      required: true,
      minlength: 10,
    },
    password: {
      required: true,
       minlength: 8,
       maxlength: 20,
    }
  },
  //For custom messages
  messages: {
    password:{
      required: "This field is required.",
      minlength: "Enter at least 8 characters"
    }
    
  },
  errorElement : 'div',
  errorPlacement: function(error, element) {
    var placement = $(element).data('error');
    if (placement) {
      $(placement).append(error)
    } else {
      error.insertAfter(element);
    }
  }
});
</script>
<!--Jquery Validation-->
   </body>
</html>