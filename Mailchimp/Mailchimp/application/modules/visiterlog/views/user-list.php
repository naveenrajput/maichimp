<div class="container-fluid">
  
   <!-- Page Heading -->
   <div class="row">
      <div class="col-md-10">
         <h1 class="h3 mb-2 text-gray-800">User List</h1>
          <?php
      if(isset($breadcrumb)&&  !is_null($breadcrumb)){
   ?> 
   <div class="span10" style="margin-left:5px;">
      <ul class="breadcrumb">
         <?php echo $breadcrumb; ?>     
      </ul>
   </div>
   <?php } ?>
      </div>
      <div class="col-md-2">
         <a href="<?php echo base_url('add-user'); ?>" class="btn btn-green">Add User</a>
      </div>
   </div>
   <!-- DataTales Example -->
   <div class="card shadow mb-4">
      <!--<div class="card-header py-3">-->
      <!--   <h6 class="m-0 font-weight-bold text-primary">User Datalist</h6>-->
        
      <!--</div>-->
      <div class="card-body">
         <div class="table-responsive">
            <table id="example" class="table table-hover responsive nowrap" style="width:100%">
               <thead>
                  <tr>
                     <th>User Name</th>
                     <th>Contact No.</th>
                     <th>Designation</th>
                     <th>Date of Birth</th>
                     <th>Actions</th>
                  </tr>
               </thead>
               <tbody>
                  <?php if (isset($data)) {
                     foreach ($data as $emp) {
                  ?>
                  <tr>
                     <td><?php if ($emp->access!="") { ?><img src="<?php echo base_url('uploads/flag.jpg'); ?> " style="width: 20px;"><?php } ?>
                        <a href="#">
                           <div class="d-flex align-items-center">
                              <div class="avatar avatar-blue mr-3"><img src="<?php echo $emp->profile_pic; ?>" class="avatar-img"></div>
                              <div class="">
                                 <p class="font-weight-bold mb-0"><?php echo $emp->empname; ?></p>
                                 <p class="text-muted mb-0"><?php echo $emp->email_id; ?></p>
                              </div>
                           </div>
                        </a>
                     </td>
                     <td><?php echo $emp->contact_num; ?></td>
                     <td><?php echo $emp->designation; ?></td>
                     <td><?php echo $emp->dob; ?></td>
                     <td>
                        <div class="dropdown">
                           <button class="btn btn-sm btn-icon" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <i class="fas fa-ellipsis-h" data-toggle="tooltip" data-placement="top"
                              title="Actions"></i>
                           </button>
                           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                              <a class="dropdown-item" href="#"><i class="far fa-eye mr-2"></i> Btn</a>
                              <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt mr-2"></i> Edit Profile</a>
                              <a class="dropdown-item text-danger" href="#"><i class="far fa-trash-alt mr-2"></i> Remove</a>
                           </div>
                        </div>
                     </td>
                  </tr>
                  <?php } } ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
   $(document).ready(function() {
  $("#example").DataTable({
    aaSorting: [],
    responsive: true,

    columnDefs: [
      {
        responsivePriority: 1,
        targets: 0
      },
      {
        responsivePriority: 2,
        targets: -1
      }
    ]
  });
  
  $(".dataTables_filter input")
    .attr("placeholder", "Search here...")
    .css({
      width: "300px",
      display: "inline-block"
    });

  $('[data-toggle="tooltip"]').tooltip();
});
</script>