<?php if (isset($data)) {
	//print_r($data);
?>
<style type="text/css">
	
</style>
<div class="text-left">
   <h1 class="h4 text-gray-900 mb-4"><?php echo $data["0"]->empname;?> </h1>
</div>
<form  method="post" id="bootstrapForm" action="<?php echo base_url('update-emp'); ?>" enctype="multipart/form-data" role="form"  autocomplete="off" novalidate>
   <div class="form-row">
      <div class="col-lg-9 col-md-12 col-sm-12 left-shift">
         <div class="form-row">
             <input type="hidden" name="u_id" value="<?php echo $data["0"]->emp_id; ?>">
            <div class="form-group col-md-12">
               <span class="has-float-label">
                  <input type="text" class="form-control" name="empname" placeholder="-" value="<?php echo $data["0"]->empname;?>"  id="ename" required>
                  <label class="" for="ename">Name*</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter name. This field is required.</div>
               </span>
            </div>
            <div class="form-group col-md-12">
               <span class="has-float-label">
                  <input type="email" class="form-control" name="email_id" placeholder="-" value="<?php echo $data["0"]->email_id; ?>" id="eid" required>
                  <label class="" for="eid">Email*</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
               </span>
            </div>
            <div class="form-group col-md-12">
               <span class="has-float-label">
               	  
	                <select name="gender" class="form-control"  id="gender" required>
	                  <option value="<?php if (!empty($data['0']->gender)) { echo $data['0']->gender; }else { echo ''; } ?>"><?php if (!empty($data["0"]->gender)) { echo $data["0"]->gender; }else { echo "Select Gender"; } ?></option>
	                  <option value="Male">Male</option>
	                  <option value="Female">Female</option>
	                  <option value="TransGender">TransGender</option>
	                </select>
            
                  <!-- <input type="text" class="form-control"  placeholder="-" value="<?php //echo $data["0"]->empname; ?>" id="gender" required> -->
                  <label class="" for="gender">Gender</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter gender. This field is required.</div>
               </span>
            </div>
        <!---bad me dekhna--<div class="form-group  col-md-6 m-t-10 ">
               <label class="form-group has-float-label">
                   
                  <select id="countries_phone1" class="form-control bfh-countries" name="country" data-country="<?php //echo $data["0"]->country ; ?>" required>
                  </select>
                  <span>Country</span>
                  <div class="valid-feedback"> </div>
                  <div class="invalid-feedback">Please select option. This field is required.</div>
               </label>
            </div>---->
            <div class="form-group col-md-6 ">
               <span class="has-float-label">
                  <input type="text" class="form-control bfh-phone" name="contact_num" data-country="countries_phone1" placeholder="Contact Number" value="<?php echo $data["0"]->contact_num ; ?>" required>
                  <label class="" for="cnumber">Contact Number</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter a contact number. This field is required.</div>
               </span>
            </div>
            <div class="form-group col-md-6">
               <span class="has-float-label">
                  <input type="date" class="form-control" name="dob" placeholder="DD-MM-YY" value="<?php echo $data["0"]->dob; ?>" id="dob" required>
                  <label class="" for="dob">Date Of Birth</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter date of birth. This field is required.</div>
               </span>
            </div>
            <div class="form-group col-md-12">
               <span class="has-float-label">
                  <input type="text" class="form-control" name="designation"  placeholder="-" value="<?php echo $data["0"]->designation; ?>" id="designation" required>
                  <label class="" for="designation">Designation</label>
                  <div class="valid-feedback"></div>
                  <div class="invalid-feedback">Please enter a designation. This field is required.</div>
               </span>
            </div>
            <?php if($this->session->userdata('name')=='admin'){ ?>
            <div class="form-group col-md-12">
              <label>Access Provide : </label>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline4" name="access" class="custom-control-input" value="full" class="sr-only">
                <label class="custom-control-label" for="customRadioInline4">Full Access</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadioInline5" name="access" class="custom-control-input" value="view" class="sr-only">
                <label class="custom-control-label" for="customRadioInline5">View Only</label>
              </div>
            </div>
            <?php } ?>
         </div>
      </div>
      <div class="col-lg-3 col-md-12 col-sm-12 right-shift">
         <!-- pic start -->
         <div class="form-group ">
            <div class="avatar-upload">
               <div class="avatar-edit">
                  <input type="file" id="imageUpload" name="profile_pic">
                  <label for="imageUpload"></label>
               </div>
               <div class="avatar-preview">
                  <div id="imagePreview" style="background-image: url(<?php echo $data["0"]->profile_pic; ?>);"> 
               </div>
            </div>
         </div>
         <span id="err_img"></span> 
      </div>
      <!-- pic close  -->
   </div>
   </div>
   <div class="row">
      <div class="col-md-6 col-xs-12 m-t-10 left-shift">
         <button type="submit" class="btn btn-green"> Save </button>
      </div>
      <!-- <div class="col-md-6 col-xs-12 m-t-10 right-shift">
         <button type="button" class="btn btn-outline-secondary text-right">Close </button>
      </div> -->
   </div>
</form>

<?php }  ?>

<script>
     function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});


//form validation kavita
     // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
     
     $("#bootstrapForm").submit(function(event) {
     
         // make selected form variable
         var vForm = $(this);
         
       /*
       If not valid prevent form submit
       https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
       */
         if (vForm[0].checkValidity() === false) {
           event.preventDefault()
           event.stopPropagation()
         } else {
          
           // Replace alert with ajax submit here...
           // alert("You have registered successfully. Please check your email");
           
         }
         
         // Add bootstrap 4 was-validated classes to trigger validation messages
         vForm.addClass('was-validated');
         
      
     });
</script>

