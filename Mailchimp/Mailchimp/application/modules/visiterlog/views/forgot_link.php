<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Greetlog - Dashboard</title>
      <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">
      <!-- Custom fonts for this template-->
      <link href="<?php echo base_url() ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
      <!-- Custom styles for this template-->
      <link href="<?php echo base_url() ?>assets/css/greetlog.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/login.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/formValidation.min.css">
      <style>
         <!--new--->
      </style>
   </head>
   <body class="bg-gradient-primary">
      <div class="container">
         <!-- Outer Row -->
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-6">
               <div class="fadeIn first m-t-50 m-b-30 text-center">
                  <img src="<?php echo base_url() ?>assets/img/logo-color.png" width="200" class="" alt="Greetlog">
               </div>
               <div class="card o-hidden border-0 shadow-lg m-t-10 m-b-10">
                  <div class="card-body p-0">
                     <!-- Nested Row within Card Body -->
                     <div class="row">
                        <!----<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>-->
                        <div class="col-lg-12">
                           <div class="p-5">
                              <div class="text-center">
                                 <h1 class="h4 text-gray-900 mb-4">Enter Your Email for Generate Password!</h1>
                              </div>
                              <form class="card-body" novalidate="" action="<?php echo base_url('submit_email');?>" method="" id="bootstrapForm">
                                 <div class="form-group">
                                    <label class="form-label" for="email" autocomplete="off">Email</label>
                                    <input id="email" class="form-input form-control" type="email"  required>
                                    <!-- <div class="valid-feedback">Email is Valid</div> -->
                                    <div class="invalid-feedback">The email is required in valid format!</div>
                                    <div style="color: red; text-align: left;"><?php if (isset($error)) {echo $error; } ?></div>
                                 </div>
                                 <button  type="submit"  class="btn btn-green btn-user  btn-block m-t-25">
                                 Submit
                                 </button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!--  Core plugin JavaScript-->
      <script src="<?php echo base_url() ?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="<?php echo base_url() ?>assets/js/greetlog.min.js"></script>
      <script src="<?php echo base_url() ?>assets/js/custom.js"></script>
      <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.slim.min.js"></script>
      <script src="<?php echo base_url() ?>assets/js/bootstrap-4-latest.min.js"></script>
      <script>
         // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example
         
         $("#bootstrapForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
           
           /*
           If not valid prevent form submit
           https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
           */
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
             alert("Please check your mail and create new password"); 
             /*  JSalert(); */
              
           
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });
         
         /* function JSalert(){
            swal({
           title: 'Are you sure?',
           text: "It will permanently deleted !",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!'
         }).then(function() {
           swal(
             'Deleted!',
             'Your file has been deleted.',
             'success'
           );
         })
          
         } */
      </script>
   </body>
</html>