<?php  
   class Dash_model extends CI_Model  
   {  
      function __construct()  
      {   
         parent::__construct();  

      }

      public function Check_in($companyid, $branchid, $day, $end_date)
      {
            if (empty($branchid)) {
                  $date = date("Y-m-d");
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  if ((!empty($day))&&(!empty($end_date))) {
                        $this->db->where('createdate >=', $day);
                        $this->db->where('createdate <', $end_date);
                  }else{
                        if ($day==$date) {

                              $this->db->where('createdate', $date);
                   
                        }elseif ($day==date('Y-m-d', strtotime('-1 day'))) {
                              $this->db->where('createdate', $day);

                        }elseif ($day==date('Y-m-d', strtotime('-7 day'))) {
                              //print_r($day);die;
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                              
                        }elseif ($day==date('Y-m-d', strtotime('-1 month'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);

                        }elseif ($day==date('Y-m-d', strtotime('-1 year'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                        }
                  }

                  
                        return $this->db->count_all_results();
            }else{
                  $date = date("Y-m-d");
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('branchid',$branchid);
                  if ((!empty($day))&&(!empty($end_date))) {
                        $this->db->where('createdate >=', $day);
                        $this->db->where('createdate <', $end_date);
                  }else{
                        if ($day==$date) {

                              $this->db->where('createdate', $date);
                   
                        }elseif ($day==date('Y-m-d', strtotime('-1 day'))) {
                              $this->db->where('createdate', $day);

                        }elseif ($day==date('Y-m-d', strtotime('-7 day'))) {
                              //print_r($day);die;
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                              
                        }elseif ($day==date('Y-m-d', strtotime('-1 month'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);

                        }elseif ($day==date('Y-m-d', strtotime('-1 year'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                        }
                  }

            
                  return $this->db->count_all_results();
            }
      	
   
 
      }


      public function Repeat_visit($companyid, $branchid, $day,$end_date)
      {
      	
// $query1=$this->db->query('SELECT *
// FROM visiters
// GROUP BY face_id
// HAVING COUNT(face_id) >1')->result();
// print_r($this->db->last_query($query1)); 

            if (empty($branchid)) {
            	$date = date("Y-m-d");
            	$this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
            	if ((!empty($day))&&(!empty($end_date))) {
            		$this->db->where('createdate >=', $day);
      			$this->db->where('createdate <', $end_date);
            	}else{

      	      	if ($day==$date) {

      		      	$this->db->where('createdate', $date);
      		 
      	      	}elseif ($day==date('Y-m-d', strtotime('-1 day'))) {
      	      		$this->db->where('createdate', $day);

      	      	}elseif ($day==date('Y-m-d', strtotime('-7 day'))) {
      	      		//print_r($day);die;
      	      		$this->db->where('createdate >=', $day);
      				$this->db->where('createdate <', $date);
      	      		
      	      	}elseif ($day==date('Y-m-d', strtotime('-1 month'))) {
      	      		$this->db->where('createdate >=', $day);
      				$this->db->where('createdate <', $date);

      	      	}elseif ($day==date('Y-m-d', strtotime('-1 year'))) {
      	      		$this->db->where('createdate >=', $day);
      				$this->db->where('createdate <', $date);
      	      	}
      	    }

            	// $this->db->where('createdate', $date);

      	   
               $this->db->having('count(face_id)>',1) ;
            	//$this->db->where('face_id >', '1');
            	return  $this->db->count_all_results() ;
            }else{

                  $date = date("Y-m-d");
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('branchid',$branchid);
                  if ((!empty($day))&&(!empty($end_date))) {
                        $this->db->where('createdate >=', $day);
                        $this->db->where('createdate <', $end_date);
                  }else{

                        if ($day==$date) {

                              $this->db->where('createdate', $date);
                   
                        }elseif ($day==date('Y-m-d', strtotime('-1 day'))) {
                              $this->db->where('createdate', $day);

                        }elseif ($day==date('Y-m-d', strtotime('-7 day'))) {
                              //print_r($day);die;
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                              
                        }elseif ($day==date('Y-m-d', strtotime('-1 month'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);

                        }elseif ($day==date('Y-m-d', strtotime('-1 year'))) {
                              $this->db->where('createdate >=', $day);
                              $this->db->where('createdate <', $date);
                        }
                }

                  // $this->db->where('createdate', $date);

                // $this->db->group_by('face_id');
                // $this->db->having('count(face_id)>',1) ;
                  //$this->db->where('face_id >', '1');
                  return  $this->db->count_all_results() ;
            }
      	 
      }


      // today visitor of company
      public function Today_visit($companyid, $branchid)
      {
            if (empty($branchid)) {
                  $date = date("Y-m-d");
                  //print_r($date);die;
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('createdate', $date);

                  return $this->db->count_all_results();
            }else{
                  $date = date("Y-m-d");
                  //print_r($date);die;
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('branchid',$branchid);
                  $this->db->where('createdate', $date);

                  return $this->db->count_all_results();
            }
      	

      }

      
      // Today Visitor of branch
      public function Visitor_list($companyid, $branchid)
      {
            if (empty($branchid)) {
                  $date = date("Y-m-d");
                  //print_r($date);die;
                  $this->db->select('*');
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('createdate', $date);
                  return $this->db->get()->result();
                  
                  
            }else{
                  $date = date("Y-m-d");
                  //print_r($date);die;
                  $this->db->select('*');
                  $this->db->from('visiters');
                  $this->db->where('companyid',$companyid);
                  $this->db->where('branchid',$branchid);
                  $this->db->where('createdate', $date);
                  
                  return  $this->db->get()->result();
            }
            

      }

    



      public function Today_data()
      {
            $date = date("Y-m-d ");
            $this->db->select('*');
         
            return $this->db->from('visiters')->where('createdate', $date)->get()->result();
            
      }

// Graph Data
      // Last 7 Days data total visitor
      public function Day7_data()
      {
            $str_date = date('Y-m-d', strtotime('-7 day'));
            $end_date = date('Y-m-d', strtotime('-6 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
            
      }
      public function Day6_data(){
            $str_date = date('Y-m-d', strtotime('-6 day'));
            $end_date = date('Y-m-d', strtotime('-5 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day5_data(){
            $str_date = date('Y-m-d', strtotime('-5 day'));
            $end_date = date('Y-m-d', strtotime('-4 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day4_data(){
            $str_date = date('Y-m-d', strtotime('-4 day'));
            $end_date = date('Y-m-d', strtotime('-3 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day3_data(){
            $str_date = date('Y-m-d', strtotime('-3 day'));
            $end_date = date('Y-m-d', strtotime('-2 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day2_data(){
            $str_date = date('Y-m-d', strtotime('-2 day'));
            $end_date = date('Y-m-d', strtotime('-1 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day1_data(){
            $str_date = date('Y-m-d', strtotime('-1 day'));
            $end_date = date('Y-m-d');
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }


      //last 7 days repeat visitor  data
      public function Day7_rdata()
      {
            $str_date = date('Y-m-d', strtotime('-7 day'));
            // $end_date = date('Y-m-d', strtotime('-6 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
            
      }
      public function Day6_rdata(){
            $str_date = date('Y-m-d', strtotime('-6 day'));
            // $end_date = date('Y-m-d', strtotime('-5 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day5_rdata(){
            $str_date = date('Y-m-d', strtotime('-5 day'));
            // $end_date = date('Y-m-d', strtotime('-4 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day4_rdata(){
            $str_date = date('Y-m-d', strtotime('-4 day'));
            // $end_date = date('Y-m-d', strtotime('-3 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day3_rdata(){
            $str_date = date('Y-m-d', strtotime('-3 day'));
            // $end_date = date('Y-m-d', strtotime('-2 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day2_rdata(){
            $str_date = date('Y-m-d', strtotime('-2 day'));
            // $end_date = date('Y-m-d', strtotime('-1 day'));
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }
      public function Day1_rdata(){
            $str_date = date('Y-m-d', strtotime('-1 day'));
            // $end_date = date('Y-m-d');
            $this->db->select('*');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            // $this->db->where('createdate <', $end_date);
            $this->db->group_by('face_id');
            $this->db->having('count(face_id)>',1) ;
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $str_date,
                               'data' => $data);
            return  $arrayName;
      }

      


      //LAST MONTH DATA in 5-5 DAYS INTERVAL
      public function M5_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-30 day',strtotime($month))); 
            $end_date = date('Y-m-d', strtotime('-25 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName;
      }

      public function M10_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-25 day',strtotime($month)));
            $end_date = date('Y-m-d', strtotime('-20 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName; 
      }

      public function M15_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-20 day',strtotime($month)));
            $end_date = date('Y-m-d', strtotime('-15 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName; 
      }

      public function M20_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-15 day',strtotime($month)));
            $end_date = date('Y-m-d', strtotime('-10 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
             $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function M25_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-10 day',strtotime($month)));
            $end_date = date('Y-m-d', strtotime('-5 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName;
      }

      public function M30_data(){
            $month = date('Y-m-00');
            $str_date = date('Y-m-d', strtotime('-5 day',strtotime($month)));
            $end_date = date('Y-m-d', strtotime('-0 day',strtotime($month)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }


      //Yearly data
      // public function Jan_data(){
      //       $year = date('Y-01-00');
      //       $str_date = date('Y-m-d', strtotime('+1 day ',strtotime($year)));
      //       $end_date = date('Y-m-d', strtotime('+1 day +1 month',strtotime($year)));
      //       $this->db->select('createdate');
      //       $this->db->from('visiters');
      //       $this->db->where('createdate >=', $str_date);
      //       $this->db->where('createdate <', $end_date);
      //       $data = $this->db->count_all_results() ;
      //       $arrayName = array('date' => 'Jan',
      //                         'dateM' => $end_date,
      //                          'data' => $data);
      //       return  $arrayName;  
      // }

      public function Feb_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day ',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +1 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Feb','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function March_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +1 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('-2 day +2 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Mar','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Apr_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +2 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +3 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Apr','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function May_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +3 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime(' +4 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'May','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function June_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +4 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +5 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Jun','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function July_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +5 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime(' +6 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Jul','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Aug_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +6 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +7 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Aug','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Sep_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +7 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +8 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Sept','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Oct_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +8 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime(' +9 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Oct','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Nov_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +9 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime('+1 day +10 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Nov','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      public function Dec_data(){
            $year = date('Y-01-00');
            $str_date = date('Y-m-d', strtotime('+1 day +10 month',strtotime($year)));
            $end_date = date('Y-m-d', strtotime(' +11 month',strtotime($year)));
            $this->db->select('createdate');
            $this->db->from('visiters');
            $this->db->where('createdate >=', $str_date);
            $this->db->where('createdate <', $end_date);
            $data = $this->db->count_all_results() ;
            $arrayName = array('date' => 'Dec','dateM' => $end_date,
                               'data' => $data);
            return  $arrayName;  
      }

      //Present month data
      
      public function PresentMonth_data(){
            $this->db->from('visiters');
            $this->db->like('createdate' ,date('Y-m'));
            $data = $this->db->count_all_results() ;
            
            return  $data;       
      }

      //Present year data
      
      public function PresentYear_data(){
            $this->db->from('visiters');
            $this->db->like('createdate' ,date('Y'));
            $data = $this->db->count_all_results() ;
            
            return  $data;       
      }


  }